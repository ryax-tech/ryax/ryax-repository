# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import time
from typing import Optional

from sqlalchemy import create_engine, event
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.orm import sessionmaker

from ryax.repository.infrastructure.database.mapper import start_mapping
from ryax.repository.infrastructure.database.metadata import metadata

Session = sessionmaker()

logger = logging.getLogger(__name__)
logger_sql_time = logging.getLogger("ryax.performance.sql_query_time")


def _remove_password_from_URI(uri: str) -> str:
    passw = uri.split("://")[1].split("@")[0].split(":")[1]
    return uri.replace(passw, "******")


class DatabaseEngine:
    def __init__(self, connection_url: str):
        self.connection_url: str = connection_url
        self.connection: Optional[Engine] = None

    def connect(self) -> None:
        logger.info("Connecting to database")
        logger.debug(
            "Database connection url: %s",
            _remove_password_from_URI(self.connection_url),
        )
        self.connection = create_engine(self.connection_url)
        metadata.create_all(bind=self.connection)
        start_mapping()

    def disconnect(self) -> None:
        logger.info("Disconnecting from database")
        if self.connection is not None:
            self.connection.dispose()

    def get_session(self) -> sessionmaker:
        return Session(bind=self.connection)


@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(  # type: ignore
    conn: Connection, _cursor, statement: str, _parameters, _context, _executemany
) -> None:
    conn.info.setdefault("query_start_time", []).append(time.time())
    logger_sql_time.debug("Start Query: %s", statement)


@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(  # type: ignore
    conn: Connection, _cursor, statement: str, _parameters, _context, _executemany
) -> None:
    total = time.time() - conn.info["query_start_time"].pop(-1)
    if total > 0.1:
        logger_sql_time.warning(
            "This query took too much time: %s. Query: %s", total, statement
        )
    else:
        logger_sql_time.debug("Query Complete! Total Time: %f", total)
