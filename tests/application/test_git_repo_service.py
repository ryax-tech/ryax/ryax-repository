import datetime
from unittest import mock

from ryax.repository.application.git_repo_service import RepositoryService
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.domain.git_repo.git_repo import GitRepo
from ryax.repository.domain.git_repo.git_repo_repository import IGitRepoRepository
from ryax.repository.domain.git_repo.git_repo_values import GitRepoInfo
from ryax.repository.domain.scan_result.scan_result import ScanResult


def test_list_sources(app_container: ApplicationContainer) -> None:
    current_project = "current_project"
    source_1 = GitRepo(
        id="source_1",
        project=current_project,
        scan_result=ScanResult(
            id="id",
            scan_date=datetime.datetime.now(),
            project=current_project,
            sha="123",
        ),
        name="name",
        url="www.test.com",
    )
    source_2 = GitRepo(id="source_2", project=current_project, name="name", url="test")
    source_1.scan_result.refresh_status = mock.MagicMock()
    git_repo_repository: mock.MagicMock[
        IGitRepoRepository
    ] = app_container.git_repo_repository()
    git_repo_repository.list_sources.return_value = [source_1, source_2]
    source_service: RepositoryService = app_container.repository_service()
    assert source_service.list_with_v1_result_bar(current_project) == [
        source_1,
        source_2,
    ]
    git_repo_repository.list_sources.assert_called_once()
    source_1.scan_result.refresh_status.assert_called_once()


def test_update_source(app_container: ApplicationContainer) -> None:
    current_project = "current_project"
    source_1 = GitRepo(
        name="name",
        url="test",
        id="source_1",
        project=current_project,
        scan_result=None,
        username="user1",
        password=b"passNotNull",
    )
    git_repo_repository: mock.MagicMock[
        IGitRepoRepository
    ] = app_container.git_repo_repository()
    git_repo_repository.get_source.return_value = source_1
    source_service: RepositoryService = app_container.repository_service()
    source_service.encryption_service.encrypt = mock.MagicMock(return_value=b"")
    update_infos = GitRepoInfo(password="", url="foo", name="bar")
    assert source_service.update(source_1.id, update_infos, current_project) == GitRepo(
        id="source_1",
        project=current_project,
        scan_result=None,
        username="user1",
        password=None,
        password_is_set=False,
        name="bar",
        url="foo",
    )
    git_repo_repository.get_source.assert_called_once()


@mock.patch.object(GitRepo, "create_from_infos")
def test_add_source(
    create_from_infos_mock, app_container: ApplicationContainer
) -> None:
    current_project = "current_project"
    source = GitRepo(
        id="source_id",
        project=current_project,
        username="user1",
        password="password1",
        name="name",
        url="url",
    )
    data = GitRepoInfo(
        name="git_repo", username="user1", password="password1", url="url"
    )
    git_repo_repository: mock.MagicMock[
        IGitRepoRepository
    ] = app_container.git_repo_repository()
    create_from_infos_mock.return_value = source
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    source_service: RepositoryService = app_container.repository_service()
    source_service.encryption_service.encrypt = mock.MagicMock(side_effect=lambda x: x)
    assert source_service.add(data, current_project) == source
    source_service.encryption_service.encrypt.assert_called_once_with("password1")
    create_from_infos_mock.assert_called_once_with(
        project=current_project, data=data, encrypted_password="password1"
    )
    git_repo_repository.add_source.assert_called_once_with(source)
    unit_of_work_mock.commit.assert_called_once()
    git_repo_repository.refresh_source.assert_called_once_with(source)


@mock.patch.object(GitRepo, "create_from_infos")
def test_add_source_when_no_user_pass(
    create_from_infos_mock, app_container: ApplicationContainer
) -> None:
    current_project = "current_project"
    source = GitRepo(id="source_id", project=current_project, name="name", url="url")
    data = GitRepoInfo(name="git_repo", url="url")
    git_repo_repository: mock.MagicMock[
        IGitRepoRepository
    ] = app_container.git_repo_repository()
    create_from_infos_mock.return_value = source
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    source_service: RepositoryService = app_container.repository_service()
    created_source = source_service.add(data, current_project)
    assert created_source == source
    create_from_infos_mock.assert_called_once_with(
        project="current_project", data=data, encrypted_password=None
    )
    git_repo_repository.add_source.assert_called_once_with(source)
    unit_of_work_mock.commit.assert_called_once()
    git_repo_repository.refresh_source.assert_called_once_with(source)


def test_get_source(app_container: ApplicationContainer) -> None:
    current_project = "current_project"
    source_id = "source_1"
    action_sorting = "name"
    action_sorting_type = "asc"
    source = GitRepo(
        id=source_id,
        project=current_project,
        scan_result=ScanResult(
            id="id",
            scan_date=datetime.datetime.now(),
            project=current_project,
            sha="123",
        ),
        name="name",
        url="url",
    )
    source.scan_result.sort_actions = mock.MagicMock()
    source.scan_result.refresh_status = mock.MagicMock()
    git_repo_repository: mock.MagicMock[
        IGitRepoRepository
    ] = app_container.git_repo_repository()
    git_repo_repository.get_source.return_value = source
    source_service: RepositoryService = app_container.repository_service()
    assert (
        source_service.get(
            source_id, current_project, action_sorting, action_sorting_type
        )
        == source
    )
    git_repo_repository.get_source.assert_called_once_with(source_id, current_project)
    source.scan_result.sort_actions.assert_called_once_with(
        action_sorting, sort_descending=action_sorting_type == "desc"
    )
    source.scan_result.refresh_status.assert_called_once()
