# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass
from typing import Optional

from ryax.repository.domain.git_repo.git_repo_values import (
    GitRepoInfo,
    GitRepoLastScanSummary,
)
from ryax.repository.domain.scan_result.scan_result import ScanResult


@dataclass
class GitRepo:
    """A Ryax user's source code, contained in a git git_repo"""

    id: str
    name: str
    url: str
    project: str
    scan_result: Optional[ScanResult] = None
    archived: bool = False
    password_is_set: bool = False
    username: Optional[str] = None
    password: Optional[bytes] = None
    last_scan_summary: Optional[GitRepoLastScanSummary] = None
    last_scan: Optional[ScanResult] = None

    @classmethod
    def create_from_info(
        cls, project: str, data: GitRepoInfo, encrypted_password: Optional[bytes]
    ) -> "GitRepo":
        return GitRepo(
            id=str(uuid.uuid4()),
            name=data.name,
            url=data.url,
            archived=False,
            project=project,
            username=data.username,
            password=encrypted_password,
            password_is_set=bool(encrypted_password),
        )

    def update_information(
        self, source_data: GitRepoInfo, encrypted_password: Optional[bytes]
    ) -> None:
        """Method to update git_repo information"""
        self.name = source_data.name if source_data.name else self.name
        self.url = source_data.url if source_data.url else self.url
        self.username = (
            source_data.username if source_data.username is not None else self.username
        )
        if encrypted_password is not None:
            self.password = None if encrypted_password == b"" else encrypted_password
        self.password_is_set = bool(self.password)

    def archive(self) -> None:
        """Method to mark git_repo as archived"""
        self.archived = True

    def set_last_scan_summary(self) -> None:
        assert self.scan_result is not None
        self.last_scan_summary = GitRepoLastScanSummary(
            scan_date=self.scan_result.scan_date,
            actions_built=len(self.scan_result.get_built_actions()),
            actions_building=len(self.scan_result.get_building_actions()),
            actions_scanned=len(self.scan_result.get_scanned_actions()),
            actions_error=len(self.scan_result.get_action_error()),
        )
