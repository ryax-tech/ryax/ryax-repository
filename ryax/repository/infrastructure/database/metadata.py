# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy import (
    JSON,
    BigInteger,
    Boolean,
    Column,
    DateTime,
    Enum,
    Float,
    ForeignKey,
    Integer,
    LargeBinary,
    MetaData,
    String,
    Table,
)
from sqlalchemy.dialects.postgresql import ARRAY

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionIOKind,
    RepositoryActionIOType,
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
)

metadata = MetaData()

# Source table declaration
git_repo_table = Table(
    "git_repo",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("url", String, nullable=False),
    Column("archived", Boolean, nullable=False),
    Column("project", String, nullable=False),
    Column("username", String, nullable=True),
    Column("password", LargeBinary, nullable=True),
    Column("password_is_set", Boolean, nullable=False),
)

# Action status type
repository_action_status = Enum(
    RepositoryActionStatus,
    name="action_status",
    values_callable=lambda x: [e.value for e in x],
)

repository_action_type = Enum(
    RepositoryActionType,
    name="repository_action_type",
    values_callable=lambda x: [e.value for e in x],
)

repository_action_kind = Enum(
    RepositoryActionKind,
    name="repository_action_kind",
    values_callable=lambda x: [e.value for e in x],
)

# Action table declaration
repository_action_table = Table(
    "repository_action",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("technical_name", String, nullable=True),
    Column("name", String, nullable=True),
    Column("description", String, nullable=True),
    Column("version", String, nullable=False),
    Column("type", repository_action_type, nullable=False),
    Column("kind", repository_action_kind, nullable=False),
    Column("status", repository_action_status, nullable=False),
    Column("archive", LargeBinary, nullable=True),
    Column("lockfile", LargeBinary, nullable=True),
    Column("dynamic_outputs", Boolean, nullable=False),
    Column("creation_date", DateTime, nullable=False),
    Column("build_date", DateTime, nullable=True),
    Column("submit_date", DateTime, nullable=True),
    Column("source_id", String, ForeignKey("git_repo.id")),
    Column("owner_id", String, nullable=False),
    Column("logo_id", ForeignKey("logo.id", ondelete="CASCADE"), nullable=True),
    Column(
        "repository_action_resources_id",
        ForeignKey("repository_action_resources.id", ondelete="CASCADE"),
        nullable=True,
    ),
    Column("build_error", String, nullable=True),
    Column("categories", ARRAY(String), nullable=True),
    Column("project", String, nullable=False),
    Column("sha", String, nullable=False),
    Column("metadata_path", String, nullable=False),
    Column(
        "scan_errors", ARRAY(String), nullable=False, server_default="{}"
    ),  # Default to an empty array
    Column("addons", JSON, nullable=False, server_default="{}"),
)

repository_action_resources_table = Table(
    "repository_action_resources",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("cpu", Float, nullable=True),
    Column("memory", BigInteger, nullable=True),
    Column("time", Float, nullable=True),
    Column("gpu", Integer, nullable=True),
)

logo_table = Table(
    "logo",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("extension", String, nullable=False),
    Column("content", LargeBinary, nullable=False),
)

# Action IO kind
repository_action_io_type = Enum(
    RepositoryActionIOType,
    name="action_io_type",
    values_callable=lambda x: [e.value for e in x],
)
repository_action_io_kind = Enum(
    RepositoryActionIOKind,
    name="action_io_kind",
    values_callable=lambda x: [e.value for e in x],
)

# Action IO table definition
repository_action_io_table = Table(
    "action_io",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("technical_name", String, nullable=False),
    Column("display_name", String, nullable=False),
    Column("help", String, nullable=True),
    Column("type", repository_action_io_type, nullable=False),
    Column("kind", repository_action_io_kind, nullable=False),
    Column("enum_values", ARRAY(String)),
    Column("action_id", String, ForeignKey("repository_action.id")),
    Column("default_value", String, nullable=True),
    Column("optional", Boolean, nullable=False),
)

scan_result_table = Table(
    "scan_result",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("scan_date", DateTime, nullable=False),
    Column("project", String, nullable=False),
    Column(
        "source_id",
        String,
        ForeignKey("git_repo.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column("sha", String, nullable=True),
)

scan_result_action_association = Table(
    "scan_result_action_association",
    metadata,
    Column("scan_result_id", String, ForeignKey("scan_result.id")),
    Column("action_id", String, ForeignKey("repository_action.id", ondelete="CASCADE")),
)
