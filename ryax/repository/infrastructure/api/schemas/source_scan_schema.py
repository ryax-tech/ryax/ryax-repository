# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import List

from marshmallow import Schema, fields

from ryax.repository.domain.scan.scan import Scan
from ryax.repository.infrastructure.api.schemas.scanned_repository_action_schema import (
    ScannedRepositoryActionSchema,
)

logger = getLogger(__name__)


class RepositoryScanErrorSchema(Schema):
    error = fields.String(
        metadata={
            "description": "Error encountered when processing git git_repo",
            "example": "Authentication failed",
        },
    )


class RepositoryScanSchema(Schema):
    branch = fields.String(
        metadata={"description": "Name of scanned branch", "example": "master"},
    )

    error = fields.String(
        metadata={
            "description": "Error encountered when processing git git_repo",
            "example": "Authentication failed",
        },
    )

    modules = fields.Method("get_scanned_user_code_actions")

    @staticmethod
    def get_scanned_user_code_actions(
        source_scan: Scan,
    ) -> List[ScannedRepositoryActionSchema]:
        schema = ScannedRepositoryActionSchema()
        schema.context = {"source_path": source_scan.path}
        return [
            schema.dump(action)
            for action in source_scan.actions
            if action.duplicated is not True
        ]

    sha = fields.String(
        metadata={
            "description": "The commit sha of git_repo",
            "example": "6db54a2b96da72a0ffe8e0247e39815117dc2e92",
        },
    )
