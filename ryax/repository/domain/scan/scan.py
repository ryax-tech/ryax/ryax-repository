# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import List, Optional

from ryax.repository.domain.repository_action.repository_action import RepositoryAction


@dataclass
class Scan:
    """Represents the analysis (scan) of a GitRepo and reports its contents"""

    path: str
    project: str
    sha: str = ""
    error: Optional[str] = None
    actions: List[RepositoryAction] = field(init=True, default_factory=list)

    @property
    def has_error(self) -> bool:
        return bool(self.error)

    def set_error(self, error: str) -> None:
        self.error = error

    def add_action(self, action: RepositoryAction) -> None:
        self.actions.append(action)
