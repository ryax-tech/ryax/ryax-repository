# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import Optional

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionKind,
    RepositoryActionType,
    RepositoryActionWrapperType,
    BuildErrorCode,
)


@dataclass
class ActionBuildEvent:
    """Event sent to builder to request repository_action build"""

    action_id: str
    action_version: str
    action_archive: bytes
    action_project: str
    action_owner_id: str
    action_technical_name: str
    action_type: RepositoryActionType
    action_kind: RepositoryActionKind
    event_type: str = "ActionBuild"
    action_wrapper_type_list: list[RepositoryActionWrapperType] = field(
        init=True, default_factory=list
    )


@dataclass
class ActionCancelBuildEvent:
    """Event sent to builder to request cancel a build"""

    action_id: str
    event_type: str = "ActionCancelBuild"


@dataclass
class ActionBuildStartEvent:
    """Event received from builder to notify repository_action build start"""

    action_id: str
    event_type: str = "ActionBuildStart"


@dataclass
class ActionBuildSuccessEvent:
    """Event received from builder to notify repository_action build success"""

    action_id: str
    event_type: str = "ActionBuildSuccess"


@dataclass
class ActionBuildCanceledEvent:
    """Event received from builder to notify repository_action build canceled"""

    action_id: str
    event_type: str = "ActionBuildCanceled"


@dataclass
class ActionBuildErrorEvent:
    """Event received from builder to notify repository_action build error"""

    action_id: str
    code: BuildErrorCode
    event_type: str = "ActionBuildError"
    logs: Optional[str] = None


@dataclass
class ActionDeletedEvent:
    """Event received from studio to delete repository_action"""

    action_id: str
    event_type: str = "ActionDelete"
