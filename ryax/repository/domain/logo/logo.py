# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass


@dataclass
class Logo:
    id: str
    name: str
    extension: str
    content: bytes

    @staticmethod
    def from_metadata(name: str, extension: str, content: bytes) -> "Logo":
        return Logo(
            id=str(uuid.uuid4()),
            name=name,
            extension=extension,
            content=content,
        )
