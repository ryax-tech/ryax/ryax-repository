# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class GitRepoInfo:
    """Values used to create or update git_repo"""

    url: str
    name: str
    username: Optional[str] = None
    password: Optional[str] = None


@dataclass
class GitRepoLastScanSummary:
    scan_date: datetime
    actions_error: int
    actions_scanned: int
    actions_built: int
    actions_building: int
