import datetime

from aiohttp.test_utils import TestClient

from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.domain.repository_action.repository_action_views import (
    ActionView,
    SourceView,
)


async def test_list_actions(
    action_service_mock: RepositoryActionService,
    api_client: TestClient,
) -> None:
    """When fetching actions endpoint should return list of actions"""
    action_1 = ActionView(
        id="action_1_id",
        name="Action 1 name",
        description="Mock repository_action 1 description",
        version="1.0",
        technical_name="action_1_technical_name",
        creation_date=datetime.datetime(2000, 1, 1),
        status=RepositoryActionStatus.BUILT,
        source=SourceView(id="id", name="name", project="project"),
        project="project",
        lockfile=b"",
    )
    action_2 = ActionView(
        id="action_2_id",
        name="Action 2 name",
        description="Mock repository_action 2 description",
        version="2.0",
        technical_name="action_2_technical_name",
        creation_date=datetime.datetime(2000, 1, 2),
        status=RepositoryActionStatus.BUILT,
        source=SourceView(id="id", name="name", project="project"),
        project="project",
        lockfile=b"",
    )
    action_service_mock.list_actions.return_value = [action_1, action_2]
    response = await api_client.get("/modules")
    assert response.status == 200
    assert await response.json() == [
        {
            "id": action_1.id,
            "name": action_1.name,
            "description": action_1.description,
            "version": action_1.version,
            "technical_name": action_1.technical_name,
            "creation_date": action_1.creation_date.strftime("%Y-%m-%dT%H:%M:%S"),
            "build_error": None,
            "status": "Built",
            "lockfile": b"",
            "git_repo": {"id": "id", "name": "name"},
        },
        {
            "id": action_2.id,
            "name": action_2.name,
            "description": action_2.description,
            "version": action_2.version,
            "technical_name": action_2.technical_name,
            "creation_date": action_2.creation_date.strftime(
                "%Y-%m-%dT%H:%M:%S"
            ),  # '2000-01-02T00:00:00'
            "build_error": None,
            "status": "Built",
            "lockfile": b"",
            "git_repo": {"id": "id", "name": "name"},
        },
    ]
