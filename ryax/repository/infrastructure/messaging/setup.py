# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.repository.container import ApplicationContainer
from ryax.repository.infrastructure.messaging.handlers import repository_action_handler
from ryax.repository.infrastructure.messaging.utils.consumer import MessagingConsumer


def setup(consumer: MessagingConsumer, container: ApplicationContainer) -> None:
    """Method to setup messaging mapper (event type/messages mapping)"""
    container.wire(modules=[repository_action_handler])

    # Register message controller
    consumer.register_handler(
        "ActionBuildStart", repository_action_handler.on_action_build_start
    )
    consumer.register_handler(
        "ActionBuildError", repository_action_handler.on_action_build_error
    )
    consumer.register_handler(
        "ActionBuildSuccess", repository_action_handler.on_action_build_success
    )

    consumer.register_handler(
        "ActionDeleted", repository_action_handler.on_action_deleted
    )

    consumer.register_handler(
        "ActionBuildCanceled", repository_action_handler.on_action_build_canceled
    )
    consumer.register_handler(
        "NoActionBuild", repository_action_handler.on_no_action_build
    )
