import asyncio
import datetime
import os
import tempfile
import time
from asyncio import FIRST_COMPLETED
from pathlib import Path
from unittest import mock
from unittest.mock import MagicMock
from urllib.parse import quote_plus

import git
import pytest

from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionHandleFunctionNotFoundException,
    ActionInvalidSchemaException,
    ActionMetadataIsV1Exception,
    ActionRunnerFileNotFoundException,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionKind,
    RepositoryActionType,
)
from ryax.repository.domain.scan.scan import Scan
from ryax.repository.domain.scan.scan_values import ScanGitRepoRequest
from ryax.repository.infrastructure.git.git_service import GitService


@mock.patch.object(git.Repo, "clone_from")
async def test_load_source_code_master(
    repo_clone_mock, uuid_mock, app_container: ApplicationContainer
):
    source_scan = Scan()
    source_scan_request = ScanGitRepoRequest()
    source_url = "https://github.com/project"
    source_user = "user1"
    source_pass = "pass1"
    uuid_hex = mock.MagicMock()
    uuid_hex.hex = "new_id"
    uuid_mock.return_value = uuid_hex
    git_service = GitService("tmp_dir_path")
    await git_service.scan_source_code(
        source_scan, source_scan_request, source_url, source_user
    )
    repo_clone_mock.assert_called_once_with(
        f"https://{source_user}:{source_pass}@github.com/project",
        "tmp_dir_path/new_id",
        branch="master",
    )


@mock.patch("uuid.uuid4")
@mock.patch.object(git.Repo, "clone_from")
async def test_load_source_code_with_special_chars_in_password(
    repo_clone_mock, uuid_mock, app_container: ApplicationContainer
):
    password_special_chars = "!#$&'()*+,/:;=?@[]"
    password_special_chars_quote_plus = quote_plus(password_special_chars)

    source_scan = Scan()
    source_scan_request = ScanGitRepoRequest()
    source_url = "https://github.com/project"
    uuid_hex = mock.MagicMock()
    uuid_hex.hex = "new_id"
    uuid_mock.return_value = uuid_hex
    git_service = GitService("tmp_dir_path")
    await git_service.scan_source_code(source_scan, source_scan_request, source_url)
    repo_clone_mock.assert_called_once_with(
        f"https://user:{password_special_chars_quote_plus}@github.com/project",
        "tmp_dir_path/new_id",
        branch="master",
    )


@mock.patch("uuid.uuid4")
@mock.patch.object(git.Repo, "clone_from")
async def test_load_source_code_with_no_credentials(
    repo_clone_mock, uuid_mock, app_container: ApplicationContainer
) -> None:
    source_scan = Scan()
    source_scan_request = ScanGitRepoRequest()
    source_url = "https://github.com/project"
    uuid_hex = mock.MagicMock()
    uuid_hex.hex = "new_id"
    uuid_mock.return_value = uuid_hex
    mock_user = "user"
    # mock_password = "pass"
    git_service = GitService("tmp_dir_path")
    git_service._build_git_url = mock.MagicMock(return_value=source_url)
    await git_service.scan_source_code(
        source_scan, source_scan_request, source_url, mock_user
    )
    repo_clone_mock.assert_called_once_with(
        source_url, "tmp_dir_path/new_id", branch="master"
    )


@pytest.mark.skip(reason="This test is not correct. Please split for each function")
def test_analyze_source_technical_name_takes_user_given_id() -> None:
    git_service = GitService("path")
    data_path = str(os.getcwd()) + "/tests/metadata/print_env"
    source_scan = Scan(path=data_path)
    source_scan.add_action = mock.MagicMock()
    git_service.set_git_sha()
    source_scan.add_action.assert_called_with(
        name="Print Environment",
        technical_name="print-env",
        version="1.0",
        description="TEST MODULE. Print in the logs of this repository_action all the environment of the repository_action: all the environements variables and all the files.",
        type="python3",
        kind="Functions",
        dynamic_outputs=False,
        metadata_path=str(os.getcwd()) + "/tests/metadata/print_env/ryax_metadata.yaml",
    )


@mock.patch("os.path.abspath", mock.MagicMock(return_value="/tests/logo.png"))
def test_get_logo_path(app_container: ApplicationContainer) -> None:
    git_service: GitService = app_container.git_service()
    assert (
        git_service.get_logo_path(Path("/tests/metadata"), "logo.png")
        == "/tests/logo.png"
    )


@mock.patch("os.path.abspath", mock.MagicMock(return_value="/tests/metadata"))
@mock.patch.object(Path, "open")
@mock.patch("ryax.git_repo.infrastructure.git.git_service.Git")
def test_analyze_action_metadata(
    git_mock, path_open_mock, app_container: ApplicationContainer
) -> None:
    source_scan = Scan(path="path", project="project_id")
    action = RepositoryAction(
        id="id",
        project="project_id",
        metadata_path="path",
        creation_date=datetime.datetime.now(),
    )

    source_scan.add_action = mock.MagicMock(return_value=action)
    git_path_mock = mock.MagicMock()
    git_path_mock.log = mock.Mock(
        return_value="99d50838417b8e310d229ccb252b5eaa5d142861"
    )
    git_mock.return_value = git_path_mock
    path = Path("/tests")
    git_service: GitService = app_container.git_service()
    git_service._parse_scan_action_data = mock.MagicMock()
    git_service._analyze_action_files(source_scan, path)
    source_scan.add_action.assert_called_once_with(
        metadata_path="/tests/metadata",
        sha="99d50838417b8e310d229ccb252b5eaa5d142861",
    )
    git_path_mock.log.assert_called_once_with("--pretty=%H", "-n 1", "--", ".")


def test_check_runner_file_validity_when_no_runner_file(
    app_container: ApplicationContainer,
) -> None:
    with tempfile.TemporaryDirectory() as mock_path:
        git_service: GitService = app_container.git_service()
        with pytest.raises(ActionRunnerFileNotFoundException):
            git_service.check_runner_file_validity(
                Path(mock_path),
                RepositoryActionKind.PROCESSOR,
                RepositoryActionType.PYTHON3,
            )


@pytest.mark.parametrize(
    "handler_path, handler_file_contents, kind, type",
    (
        ["ryax_handler.py", "THIS IS NOT PYTHON!!", "Processor", "python3"],
        ["ryax_run.py", "THIS ALSO...", "Source", "python3"],
    ),
)
def test_check_runner_file_validity_when_syntax_error(
    handler_path,
    handler_file_contents,
    kind,
    type,
    app_container: ApplicationContainer,
) -> None:
    with tempfile.TemporaryDirectory() as mock_path:
        with open(Path(mock_path) / handler_path, "w") as handler:
            handler.write(handler_file_contents)
        git_service: GitService = app_container.git_service()
        with pytest.raises(SyntaxError):
            git_service.check_runner_file_validity(
                Path(mock_path) / handler_path, kind, type
            )


@pytest.mark.parametrize(
    "handler_path, handler_file_contents, kind",
    (
        ["ryax_handler.py", "def toto(foo):\n    pass\n", "Processor"],
        ["ryax_run.py", "async def tata(service, input_values):\n    pass\n", "Source"],
    ),
)
def test_check_runner_file_validity_when_handler_func_nonexistant(
    handler_path, handler_file_contents, kind, app_container: ApplicationContainer
) -> None:
    with tempfile.TemporaryDirectory() as mock_path:
        with open(Path(mock_path) / handler_path, "w") as handler:
            handler.write(handler_file_contents)
        git_service: GitService = app_container.git_service()
        with pytest.raises(ActionHandleFunctionNotFoundException):
            git_service.check_runner_file_validity(
                Path(mock_path) / handler_path, kind, "python3"
            )


@pytest.mark.parametrize(
    "handler_path, handler_file_contents, kind",
    (
        ["ryax_handler.py", "def handle(foo):\n    pass\n", "Processor"],
        ["ryax_run.py", "async def run(service, input_values):\n    pass\n", "Source"],
    ),
)
def test_check_handler_validity_when_valid(
    handler_path, handler_file_contents, kind, app_container: ApplicationContainer
) -> None:
    with tempfile.TemporaryDirectory() as mock_path:
        with open(Path(mock_path) / handler_path, "w") as handler:
            handler.write(handler_file_contents)
        git_service: GitService = app_container.git_service()
        git_service.check_runner_file_validity(
            Path(mock_path) / handler_path, kind, "python3"
        )


def test_check_schema_validity(app_container: ApplicationContainer) -> None:
    yaml_data = {
        "apiVersion": "ryax.tech/v2.0",
        "kind": "Processor",
        "spec": {
            "human_name": "name",
            "id": "technical_name",
            "logo": "logo.png",
            "version": "1.0",
            "description": "foo",
            "type": "python3",
            "inputs": [
                {
                    "name": "input_name",
                    "human_name": "input_human_name",
                    "help": "input_help",
                    "type": "string",
                    "enum_values": [],
                }
            ],
            "outputs": [
                {
                    "name": "output_name",
                    "human_name": "output_human_name",
                    "help": "output_help",
                    "type": "string",
                    "enum_values": [],
                },
            ],
        },
    }
    git_service: GitService = app_container.git_service()
    git_service._check_schema_validity(yaml_data)


def test_check_for_v1_metadata(app_container: ApplicationContainer) -> None:
    metadata = {"not api version": "bar"}
    git_service: GitService = app_container.git_service()
    assert git_service._check_for_v1_metadata(metadata) is None


def test_check_for_v1_metadata_when_v1(app_container: ApplicationContainer) -> None:
    metadata = {"apiVersion": "ryax.tech/v1"}
    git_service: GitService = app_container.git_service()
    with pytest.raises(ActionMetadataIsV1Exception):
        git_service._check_for_v1_metadata(metadata)


@pytest.mark.parametrize(
    "yaml_data",
    (
        {
            "kind": "Functions",
            "spec": {
                "human_name": "name",
                "id": "technical_name",
                "logo": "logo.png",
                "version": "1.0",
                "detail": "description",
                "type": "python3",
                "kind": "Functions",
            },
        },
        {
            "apiVersion": "ryax.tech/v1.0",
            "kind": "Processor",
            "spec": {
                "human_name": "name",
                "id": "technical_name",
                "logo": "logo.png",
                "version": "1.0",
                "description": "foo",
                "type": "python3",
                "inputs": [
                    {
                        "name": "input_name",
                        "human_name": "input_human_name",
                        "help": "input_help",
                        "type": "string",
                        "enum_values": [],
                    }
                ],
                "outputs": [
                    {
                        "name": "output_name",
                        "human_name": "output_human_name",
                        "help": "output_help",
                        "type": "string",
                        "enum_values": [],
                    },
                ],
            },
        },
        {
            "apiVersion": "ryax.tech/v2.0",
            "kind": "Functions",
            "spec": {
                "human_name": "name",
                "id": "technical_name",
                "logo": "logo.png",
                "version": "1.0",
                "description": "foo",
                "type": "python3",
                "inputs": [
                    {
                        "name": "input_name",
                        "human_name": "input_human_name",
                        "help": "input_help",
                        "type": "string",
                        "enum_values": [],
                    }
                ],
                "outputs": [
                    {
                        "name": "output_name",
                        "human_name": "output_human_name",
                        "help": "output_help",
                        "type": "string",
                        "enum_values": [],
                    },
                ],
            },
        },
        {
            "apiVersion": "ryax.tech/v2.0",
            "kind": "Processor",
            "spec": {
                "human_name": "name",
                "id": "technical_name",
                "logo": "logo.png",
                "version": "1.0",
                "description": "foo",
                "type": "python3",
                "inputs": [
                    {
                        "name": "input_name",
                        "invalid_key": "INVALID HERE",
                        "human_name": "input_human_name",
                        "help": "input_help",
                        "type": "string",
                        "enum_values": [],
                    }
                ],
                "outputs": [
                    {
                        "name": "output_name",
                        "human_name": "output_human_name",
                        "help": "output_help",
                        "type": "string",
                        "enum_values": [],
                    },
                ],
            },
        },
    ),
)
def test_check_schema_validity_when_invalid(
    yaml_data, app_container: ApplicationContainer
) -> None:
    git_service: GitService = app_container.git_service()
    with pytest.raises(ActionInvalidSchemaException):
        git_service._check_schema_validity(yaml_data)


@pytest.mark.parametrize(
    "categories",
    [
        (["A" * 33]),
        (["AA"]),
        (["valid_category"] * 33),
        (["1", 2, {"invalid": "object"}]),
        (["1", ["invalid"]]),
    ],
)
def test_check_schema_validity_invalid_categories(
    app_container: ApplicationContainer, categories: list
) -> None:
    yaml_data = {
        "apiVersion": "ryax.tech/v1",
        "kind": "Functions",
        "spec": {
            "human_name": "name",
            "id": "technical_name",
            "logo": "logo.png",
            "version": "1.0",
            "detail": "description",
            "type": "python3",
            "kind": "Functions",
            "categories": categories,
        },
    }
    git_service: GitService = app_container.git_service()
    with pytest.raises(ActionInvalidSchemaException):
        git_service._check_schema_validity(yaml_data)


async def test_git_clone_is_not_blocking_other_services() -> None:
    """
    Run   git clone in parallel with task that writes every seconds an index and a time to a file.
    If the task are really executed in parallel the index and time of write should be equals
    """

    async def test(testfile):
        index = 0
        started = int(time.time())
        try:
            while True:
                await asyncio.sleep(1)
                index = index + 1
                now = int(time.time())
                row = f"{now - started};{index}\n"
                testfile.write(row)
        except asyncio.CancelledError():
            return

    source_scan = Scan()
    source_scan_request = ScanGitRepoRequest()
    source_url = "https://github.com/tomplus/kubernetes_asyncio.git"
    with tempfile.TemporaryDirectory() as tmp_dir:
        with tempfile.NamedTemporaryFile(mode="w+") as tmp_file:
            task = asyncio.create_task(test(tmp_file))
            git_service = GitService(tmp_dir)
            clone_task = git_service.scan_source_code(
                source_scan, source_scan_request, source_url, ""
            )
            await asyncio.wait({task, clone_task}, return_when=FIRST_COMPLETED)
            await asyncio.sleep(1)
            task.cancel()
            tmp_file.flush()
            tmp_file.seek(0)
            for line in tmp_file.readlines():
                time_index = line.split(";")[0].strip()
                index = line.split(";")[1].strip()
                assert time_index == index


def test_resources_scanned_invalid(app_container: ApplicationContainer) -> None:
    yaml_data = {
        "apiVersion": "ryax.tech/v2.0",
        "kind": "Processor",
        "spec": {
            "human_name": "name",
            "id": "technical_name",
            "version": "1.0",
            "description": "foo",
            "type": "python3",
            "resources": {"not_valid"},
            "inputs": [],
            "outputs": [],
        },
    }
    git_service: GitService = app_container.git_service()
    with pytest.raises(ActionInvalidSchemaException):
        git_service._check_schema_validity(yaml_data)


def test_resources_properly_scanned(app_container: ApplicationContainer) -> None:
    yaml_data = {
        "apiVersion": "ryax.tech/v2.0",
        "kind": "Processor",
        "spec": {
            "human_name": "name",
            "id": "technical-name",
            "version": "1.0",
            "description": "foo",
            "type": "python3",
            "resources": {"cpu": 1, "memory": 1024, "time": "200s", "gpu": 2},
            "inputs": [],
            "outputs": [],
        },
    }
    git_service: GitService = app_container.git_service()
    action = MagicMock(RepositoryAction, id="test", inputs=[], outputs=[])
    git_service._inject_action_with_metadata(
        action=action, action_metadata=yaml_data, action_metadata_path=Path()
    )
    assert action.resources.cpu == 1
    assert action.resources.memory == 1024
    assert action.resources.time == 200
    assert action.resources.gpu == 2


def test_resources_empty_values_scanned(app_container: ApplicationContainer) -> None:
    yaml_data = {
        "apiVersion": "ryax.tech/v2.0",
        "kind": "Processor",
        "spec": {
            "human_name": "name",
            "id": "technical-name",
            "version": "1.0",
            "description": "foo",
            "type": "python3",
            "resources": {
                "memory": 1024,
                "time": "200s",
            },
            "inputs": [],
            "outputs": [],
        },
    }
    git_service: GitService = app_container.git_service()
    action = MagicMock(RepositoryAction, id="test", inputs=[], outputs=[])
    git_service._inject_action_with_metadata(
        action=action, action_metadata=yaml_data, action_metadata_path=Path()
    )
    assert action.resources.cpu is None
    assert action.resources.memory == 1024
    assert action.resources.time == 200
    assert action.resources.gpu is None


def test_no_resources_scanned(app_container: ApplicationContainer) -> None:
    yaml_data = {
        "apiVersion": "ryax.tech/v2.0",
        "kind": "Processor",
        "spec": {
            "human_name": "name",
            "id": "technical-name",
            "version": "1.0",
            "description": "foo",
            "type": "python3",
            "inputs": [],
            "outputs": [],
        },
    }
    git_service: GitService = app_container.git_service()
    action = MagicMock(RepositoryAction, id="test", inputs=[], outputs=[])
    git_service._inject_action_with_metadata(
        action=action, action_metadata=yaml_data, action_metadata_path=Path()
    )
    assert action.resources.cpu is None
    assert action.resources.memory is None
    assert action.resources.time is None
    assert action.resources.gpu is None
