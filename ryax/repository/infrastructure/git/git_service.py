# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import ast
import asyncio
import concurrent.futures
import itertools
import logging
import os
import re
import shutil
import uuid
from pathlib import Path
from typing import Optional, Union
from urllib.parse import quote_plus

import yaml
from git import Git, GitCommandError, Repo
from jsonschema import ValidationError, validate
from PIL import Image

from ryax.repository.domain.action_metadata.ryax_metadata_schemas import (
    RyaxMetadataSchemaV2,
)
from ryax.repository.domain.git_repo.git_repo_exceptions import (
    ErrorDuringGitRepoFetchException,
)
from ryax.repository.domain.logo.logo import Logo
from ryax.repository.domain.repository_action.repository_action import (
    RepositoryAction,
    RepositoryActionIO,
    RepositoryActionResources,
)
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionDynamicOutputsNotPermittedException,
    ActionHandleFunctionInvalidArgumentCountException,
    ActionHandleFunctionNotFoundException,
    ActionInvalidSchemaException,
    ActionIOInvalidTypeException,
    ActionMetadataIsV1Exception,
    ActionRunnerFileNotFoundException,
    ActionSurpassesDataLimitException,
    LogoFileDoesNotExistException,
    LogoFileInvalidFileTypeException,
    LogoFileTooLargeException,
    RepositoryActionDuplicatedInputTechnicalNameException,
    RepositoryActionDuplicatedOutputTechnicalNameException,
    RepositoryActionInvalidKindException,
    RepositoryActionInvalidTypeException,
    RepositoryActionResourceRequestInvalidException,
    RepositoryActionTechnicalNameInvalidException,
    UnableToLoadActionFilesException,
    UnableToOpenLogoFileException,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionIOKind,
    RepositoryActionIOType,
    RepositoryActionKind,
    RepositoryActionType,
)
from ryax.repository.domain.scan.scan import Scan
from ryax.repository.domain.scan.scan_service import IRepositoryScanService
from ryax.repository.domain.scan.scan_values import ActionScanErrors, ScanGitRepoRequest

logger = logging.getLogger(__name__)


class GitService(IRepositoryScanService):
    def __init__(self, tmp_directory_path: str):
        self.tmp_directory_path: Path = Path(tmp_directory_path)

    @staticmethod
    def _build_git_url(
        source_url: str,
        source_username: Optional[str],
        source_password: Optional[str],
    ) -> str:
        """Build authenticated url"""
        credentials = (
            f"{source_username}:{quote_plus(source_password)}@"
            if source_username and source_password
            else ""
        )
        if "://" in source_url:
            protocol, url = source_url.split("://")
            return f"{protocol}://{credentials}{url}"
        else:
            return f"https://{credentials}{source_url}"

    def _build_git_path(self) -> str:
        """Build authenticated url"""
        return str(self.tmp_directory_path / uuid.uuid4().hex)

    async def scan_source_code(
        self,
        scan_git_repo_request: ScanGitRepoRequest,
        project: str,
        git_repo_url: str,
        git_repo_username: Optional[str] = None,
        git_repo_password: Optional[str] = None,
    ) -> Scan:
        """Clone git git_repo on local disk"""
        url = self._build_git_url(git_repo_url, git_repo_username, git_repo_password)
        path = self._build_git_path()
        scan = Scan(path=path, project=project)
        try:
            loop = asyncio.get_running_loop()

            if scan_git_repo_request.branch == "master":

                def clone() -> None:
                    Repo.clone_from(url, path, branch="master")

            else:

                def clone() -> None:
                    repo = Repo.clone_from(url, path)
                    repo.git.checkout(scan_git_repo_request.branch.strip())

            with concurrent.futures.ThreadPoolExecutor() as pool:
                await loop.run_in_executor(pool, clone)

            return scan
        except GitCommandError as error:
            logger.warning(
                "Error happened during Git repository fetch %s", error.stderr
            )
            scan.set_error(
                error=ErrorDuringGitRepoFetchException.message + ": " + str(error)
            )
            raise ErrorDuringGitRepoFetchException

    @staticmethod
    def _check_for_v1_metadata(metadata: dict) -> None:
        """Function to display error when v1 apiVersion is detected. This method may be removed later when support is fully dropped"""
        if metadata.get("apiVersion") == "ryax.tech/v1":
            raise ActionMetadataIsV1Exception

    @staticmethod
    def _check_schema_validity(yaml_data: dict) -> None:
        """Determine if the ryax_metadata file given is a valid repository_action of any version"""
        try:
            validate(instance=yaml_data, schema=RyaxMetadataSchemaV2.schema)
        except ValidationError as err:
            message = "Invalid ryax_metadata.yaml file"
            if (
                len(err.message) <= 255
            ):  # err.message copies the data set in input, and it can be very long
                message += f": {err.message}"
            if len(err.path) > 0:
                path = "[%s]" % "][".join(repr(index) for index in err.path)
                message += f" in {path}."
            else:
                message += "."
            raise ActionInvalidSchemaException(message)

    @staticmethod
    def _load_runner_file_contents(runner_file_path: Path) -> str:
        """Loads the runner file as a raw string"""
        with open(runner_file_path, "r") as f:
            return f.read()

    @staticmethod
    def _confirm_has_handler_function(
        parsed_runner_file_contents: ast.AST, kind: RepositoryActionKind
    ) -> Union[ast.FunctionDef, ast.AsyncFunctionDef]:
        """Checks that the user has defined a handler/run function"""
        generator = ast.walk(parsed_runner_file_contents)
        if kind == RepositoryActionKind.SOURCE:
            for node in generator:
                if isinstance(node, ast.AsyncFunctionDef) and (node.name == "run"):
                    return node
        else:
            for node in generator:
                if isinstance(node, ast.FunctionDef) and node.name == "handle":
                    return node
        raise ActionHandleFunctionNotFoundException()

    @staticmethod
    def _check_handler_args(
        handler_node: Union[ast.FunctionDef, ast.AsyncFunctionDef],
        kind: RepositoryActionKind,
        has_addons: bool,
    ) -> None:
        """Checks that the handler method in the user code takes the correct amount of arguments"""
        action_args = 2 if has_addons else 1
        trigger_args = 3 if has_addons else 2

        handler_args = next(
            len(node.args)
            for node in ast.walk(handler_node)
            if isinstance(node, ast.arguments)
        )
        if not (
            (handler_args == action_args and kind != RepositoryActionKind.SOURCE)
            or (handler_args == trigger_args and kind == RepositoryActionKind.SOURCE)
        ):
            raise ActionHandleFunctionInvalidArgumentCountException()

    def check_runner_file_validity(
        self,
        metadata_path: Path,
        kind: RepositoryActionKind,
        type: RepositoryActionType,
    ) -> None:
        """
        Checking the python code of the runner file:
        1. Locate the existing file and load its contents
        2. Parse the contents and check for a handle(r) function that takes 1 argument
        """
        if type == RepositoryActionType.PYTHON3:
            trigger_runner = "ryax_run.py"
            action_runner = "ryax_handler.py"

            action_dir = metadata_path.absolute().parent

            runner_path = (
                action_dir / trigger_runner
                if kind == RepositoryActionKind.SOURCE
                else action_dir / action_runner
            )
            if not runner_path.is_file():
                raise ActionRunnerFileNotFoundException()

            runner_file_contents = self._load_runner_file_contents(runner_path)
            parsed_contents = ast.parse(runner_file_contents)
            _ = self._confirm_has_handler_function(parsed_contents, kind)
            # TODO decide if this check is needed after addons are supported
            # self._check_handler_args(handler_node, kind, has_addons)
        if type == RepositoryActionType.CSHARP_DOTNET6:
            # TODO Do some validation using something like https://github.com/pan-unit42/dotnetfile
            pass

    @staticmethod
    def _set_action_sha(action: RepositoryAction, action_metadata_path: Path) -> None:
        """Sets the sha for this RepositoryAction"""
        git_path = Git(action_metadata_path.parent)
        action_sha = git_path.log("--pretty=%H", "-n 1", "--", ".")
        action.sha = action_sha

    def _inject_action_with_metadata(
        self,
        action: RepositoryAction,
        action_metadata: dict,
        action_metadata_path: Path,
    ) -> None:
        """Transfers data from ryax_metadata.yaml to the RepositoryAction"""
        # Required metadata fields
        action_kind = RepositoryActionKind.string_to_enum(action_metadata["kind"])
        action.kind = action_kind
        action_technical_name = action_metadata["spec"]["id"]
        if re.match("^[a-z0-9-.]+$", action_technical_name) is None:
            raise RepositoryActionTechnicalNameInvalidException
        action.technical_name = action_technical_name

        action.name = action_metadata["spec"]["human_name"]

        action.type = RepositoryActionType.string_to_enum(
            action_metadata["spec"]["type"]
        )

        # Optional metadata fields
        action.version = action_metadata["spec"].get("version", "")
        action.description = action_metadata["spec"].get("description", "")
        action.categories = list(set(action_metadata["spec"].get("categories", [])))
        action.addons = action_metadata["spec"].get("addons", {})
        action.dynamic_outputs = action_metadata["spec"].get("dynamic_outputs", False)
        if (
            action.kind != RepositoryActionKind.SOURCE
            and action.dynamic_outputs is True
        ):
            raise ActionDynamicOutputsNotPermittedException()

        action.resources = (
            RepositoryActionResources.from_metadata(
                cpu=action_metadata["spec"]["resources"].get("cpu"),
                memory=action_metadata["spec"]["resources"].get("memory"),
                time=action_metadata["spec"]["resources"].get("time"),
                gpu=action_metadata["spec"]["resources"].get("gpu"),
            )
            if action_metadata["spec"].get("resources") is not None
            else None
        )

        for metadata_input in action_metadata["spec"].get("inputs", []):
            if not RepositoryActionIOType.has_value(metadata_input["type"]):
                raise ActionIOInvalidTypeException
            action_input_default_value = metadata_input.get("default_value", None)
            action_input = RepositoryActionIO.from_metadata(
                display_name=metadata_input["human_name"],
                technical_name=metadata_input["name"],
                help=metadata_input["help"],
                type=metadata_input["type"],
                kind=RepositoryActionIOKind.INPUT,
                enum_values=metadata_input.get("enum_values", []),
                default_value=str(action_input_default_value)
                if action_input_default_value is not None
                else None,
                optional=metadata_input.get("optional", False),
            )
            action.add_action_input(action_input)
        for input_a, input_b in itertools.combinations(action.inputs, 2):
            if input_a.technical_name == input_b.technical_name:
                raise RepositoryActionDuplicatedInputTechnicalNameException

        for metadata_output in action_metadata["spec"].get("outputs", []):
            if not RepositoryActionIOType.has_value(metadata_output["type"]):
                raise ActionIOInvalidTypeException
            action_output = RepositoryActionIO.from_metadata(
                display_name=metadata_output["human_name"],
                technical_name=metadata_output["name"],
                help=metadata_output["help"],
                type=metadata_output["type"],
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=metadata_output.get("enum_values", []),
                optional=metadata_output.get("optional", False),
            )
            action.add_action_output(action_output)
        for output_a, output_b in itertools.combinations(action.outputs, 2):
            if output_a.technical_name == output_b.technical_name:
                raise RepositoryActionDuplicatedOutputTechnicalNameException

        metadata_logo: str = action_metadata["spec"].get("logo", None)
        if metadata_logo is not None:
            logo_path = Path(metadata_logo)
            logo_complete_path = str(action_metadata_path.parent / logo_path)
            self.check_logo_file(logo_complete_path)
            logo_content = self.load_logo_file(logo_complete_path)
            action_logo = Logo.from_metadata(
                name=logo_path.stem,
                extension=logo_path.suffix,
                content=logo_content,
            )
            action.set_logo(action_logo)

        # FIXME: Avoid hardcoded path?
        lockfile_filename = "ryax_lock.json"
        lockfile_path = action_metadata_path.parent / lockfile_filename
        if lockfile_path.exists():
            lockfile = self.load_lockfile(lockfile_path)
            logger.debug("Action contains lockfile %s" % lockfile.decode())

            if lockfile:
                logger.debug("Save lockfile in db for front")
                action.set_lockfile(lockfile)

    @staticmethod
    def _load_file_content_as_bytes(file_path: str) -> bytes:
        """Reads a file in binary mode and returns the content"""
        with open(file_path, "rb") as file:
            content = file.read()
        return content

    @staticmethod
    def check_logo_file(logo_path: str) -> None:
        logo = Path(logo_path)
        if not logo.is_file():
            raise LogoFileDoesNotExistException
        elif logo.suffix not in [".png", ".jpg", ".jpeg"]:
            raise LogoFileInvalidFileTypeException
        else:
            with Image.open(logo_path) as img:
                width, height = img.size
            if width > 250 or height > 250:
                raise LogoFileTooLargeException

    def load_logo_file(self, logo_path: str) -> bytes:
        """Loads the repository_action logo"""
        try:
            logo_content = self._load_file_content_as_bytes(logo_path)
            return logo_content
        except Exception as err:
            logger.warning(f"Error opening logo file: {err}")
            raise UnableToOpenLogoFileException

    def load_lockfile(self, lockfile_path: Path) -> bytes:
        """Loads the lockfile if exists"""
        try:
            lockfile_content = self._load_file_content_as_bytes(str(lockfile_path))
            return lockfile_content
        except Exception as err:
            logger.warning(f"Error opening logo file: {err}")
            raise UnableToOpenLogoFileException

    def parse_action_data(
        self, action: RepositoryAction, action_metadata_path: Path
    ) -> None:
        """
        Principal method used for creating a RepositoryAction from user code.
        Checks all the user files and catches any possible exceptions
        """
        try:
            self._set_action_sha(action, action_metadata_path=action_metadata_path)

            with action_metadata_path.open() as opened_metadata_file:
                action_metadata = yaml.safe_load(opened_metadata_file)

            self._check_for_v1_metadata(action_metadata)
            self._check_schema_validity(action_metadata)
            self._inject_action_with_metadata(
                action, action_metadata, action_metadata_path=action_metadata_path
            )
            self.check_runner_file_validity(
                action_metadata_path, action.kind, action.type
            )
            self.load_code_archive(action, action_metadata_path)

        except (IOError, FileNotFoundError):
            logger.warning("Error when opening metadata file")
            action.add_scan_error(ActionScanErrors.read_metadata_fail)
        except ActionIOInvalidTypeException:
            logger.warning(
                "An IO value defined in the metadata has an unsupported type"
            )
            action.add_scan_error(ActionScanErrors.action_io_invalid_type)
        except ActionMetadataIsV1Exception:
            logger.warning("apiVersion v1 metadata no longer supported")
            action.add_scan_error(ActionScanErrors.metadata_apiversion_is_v1)
        except ActionInvalidSchemaException as err:
            logger.warning(f"Invalid schema in ryax_metadata file: {err}")
            action.add_scan_error(str(err))
        except yaml.YAMLError as err:
            logger.warning(f"Unable to parse metadata file: {err}")
            action.add_scan_error(ActionScanErrors.parse_metadata_fail)
        except ActionRunnerFileNotFoundException:
            logger.warning("Action runner file not found")
            action.add_scan_error(ActionScanErrors.handler_not_found)
        except SyntaxError:
            logger.warning("Action python code has syntax errors")
            action.add_scan_error(ActionScanErrors.runner_file_syntax_error)
        except ActionHandleFunctionNotFoundException:
            logger.warning("Handle function was not found")
            action.add_scan_error(ActionScanErrors.runner_file_handle_function_error)
        except ActionHandleFunctionInvalidArgumentCountException:
            logger.warning("Handle function must take a single argument")
            action.add_scan_error(
                ActionScanErrors.runner_file_handle_function_invalid_argument_count
            )
        except ActionDynamicOutputsNotPermittedException:
            logger.warning(
                "Action was a non trigger but had dynamic outputs=True in metadata."
            )
            action.add_scan_error(
                ActionScanErrors.non_trigger_action_has_dynamic_outputs
            )
        except UnableToOpenLogoFileException:
            logger.warning("Action has logo defined but the file could not be opened.")
            action.add_scan_error(ActionScanErrors.action_logo_file_could_not_be_opened)
        except ActionSurpassesDataLimitException:
            logger.warning("Action files exceeds data limit")
            action.add_scan_error(ActionScanErrors.action_files_exceed_data_limit)
        except UnableToLoadActionFilesException:
            logger.warning("Unable to open repository_action files.")
            action.add_scan_error(ActionScanErrors.unable_to_open_action_files)
        except RepositoryActionInvalidKindException:
            logger.warning("Action has invalid kind.")
            action.add_scan_error(ActionScanErrors.kind_is_invalid)
        except RepositoryActionInvalidTypeException:
            logger.warning("Action has invalid type.")
            action.add_scan_error(ActionScanErrors.type_is_invalid)
        except RepositoryActionTechnicalNameInvalidException:
            logger.warning("Action has invalid technical name.")
            action.add_scan_error(ActionScanErrors.technical_name_is_invalid)
        except LogoFileDoesNotExistException:
            logger.warning("Logo file does not exist.")
            action.add_scan_error(ActionScanErrors.logo_file_not_found)
        except LogoFileInvalidFileTypeException:
            logger.warning("Logo file is of the wrong file type.")
            action.add_scan_error(ActionScanErrors.logo_file_invalid_format)
        except LogoFileTooLargeException:
            logger.warning("Logo file is too large.")
            action.add_scan_error(ActionScanErrors.logo_too_large)
        except RepositoryActionDuplicatedInputTechnicalNameException:
            logger.warning("Action input technical name duplicated.")
            action.add_scan_error(ActionScanErrors.duplicated_input_technical_name)

        except RepositoryActionDuplicatedOutputTechnicalNameException:
            logger.warning("Action output technical name duplicated.")
            action.add_scan_error(ActionScanErrors.duplicated_output_technical_name)
        except RepositoryActionResourceRequestInvalidException as err:
            logger.warning("Action resource time request is invalid.")
            action.add_scan_error(str(err))
        finally:
            if not action.technical_name:
                logger.warning(
                    "An error occurred when scanning the action and no 'id' field was read from the ryax_metadata.yaml file. Assigning a generated `id` for the `technical_name` of this action"
                )
                action.technical_name = f"unknown_technical_name-{str(uuid.uuid4())}"

    def set_git_sha(self, scan: Scan) -> None:
        """Browse code and find ryax_metadata.yaml files to parse each repository_action. Parse repository_action data and report errors sequentially"""
        git_path = Git(scan.path)
        scan_sha = git_path.log("--pretty=%H", "-n 1", "--", ".")
        scan.sha = scan_sha

    def clear_source_code(self, scan: Scan) -> None:
        """Remove temporary directory used for analysis"""
        if os.path.exists(scan.path):
            shutil.rmtree(scan.path)

    def _create_action_archive_zip(self, metadata_path: Path) -> str:
        """
        Creates a compressed archive for the repository_action data
        """
        action_absolute_path = metadata_path.absolute().parent

        action_archive_path = self.tmp_directory_path / uuid.uuid4().hex

        action_archive_tmp = shutil.make_archive(
            str(action_archive_path), "zip", str(action_absolute_path)
        )
        return action_archive_tmp

    def _check_size(self, action_metadata_path: Path) -> None:
        """Checks if the files for this repository_action exceed 1 GB"""
        total_size = 0
        action_data_path = os.path.abspath(
            os.path.join(self.tmp_directory_path, str(action_metadata_path.parent))
        )
        for dir_path, dir_names, filenames in os.walk(action_data_path):
            for f in filenames:
                fp = os.path.join(dir_path, f)
                if not os.path.islink(fp):
                    file_size = os.path.getsize(fp)
                    total_size += file_size
        if total_size > 1_000_000_000:
            raise ActionSurpassesDataLimitException

    def _clear_zip_file(self, action_archive_path: str) -> None:
        """Removes an actions archive"""
        if os.path.exists(action_archive_path):
            os.remove(action_archive_path)

    def load_code_archive(
        self, action: RepositoryAction, action_metadata_path: Path
    ) -> None:
        """
        Loads the data as bytes for this repository_action
        """
        try:
            self._check_size(action_metadata_path)
            action_archive_path = self._create_action_archive_zip(action_metadata_path)
            action.archive = self._load_file_content_as_bytes(action_archive_path)
            self._clear_zip_file(action_archive_path)
        except Exception as error:
            logger.warning(f"Unable to load archive: {error}")
            raise UnableToLoadActionFilesException
