import datetime
from unittest import mock

import pytest

from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.domain.events.event_publisher import IEventPublisher
from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
)
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionNotFoundException,
    ActionUnauthorizedDelete,
)
from ryax.repository.domain.repository_action.repository_action_repository import (
    IActionRepository,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
    BuildErrorCode,
)
from ryax.repository.domain.repository_action.repository_action_views import (
    ActionView,
    SourceView,
)


def test_list_action(app_container: ApplicationContainer) -> None:
    current_project = "current_project"
    action_1 = ActionView(
        id="action_1",
        project=current_project,
        name="name",
        technical_name="techname",
        version="version",
        description="desc",
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source=SourceView(id="123", name="name", project=current_project),
        build_error_logs="",
        build_error_code=1,
    )

    action_2 = ActionView(
        id="action_2",
        project=current_project,
        name="name",
        technical_name="techname",
        version="version",
        description="desc",
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source=SourceView(id="123", name="name", project=current_project),
        build_error_logs="",
        build_error_code=1,
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.list_actions_views.return_value = [action_1, action_2]
    action_service: RepositoryActionService = app_container.action_service()
    assert action_service.list_actions(current_project) == [action_1, action_2]
    action_repository.list_actions_views.assert_called_once()


async def test_build_action(app_container: ApplicationContainer) -> None:
    """Test build repository_action"""
    action_id = "action_id"
    current_project = "current_project"
    action = RepositoryAction(
        id=action_id,
        project=current_project,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action_in_project.return_value = action
    action_repository.get_action_view.return_value = ActionView(
        id=action_id,
        project="current_project",
        name="name",
        technical_name="techname",
        version="version",
        description="desc",
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source=SourceView(id="123", name="name", project=current_project),
        build_error_logs="",
        build_error_code=1,
    )
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.build = mock.MagicMock()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action_service: RepositoryActionService = app_container.action_service()
    assert await action_service.build_action(action_id, current_project)
    action_repository.get_action_in_project.assert_called_once_with(
        action_id, current_project
    )
    action.build.assert_called_once()
    unit_of_work_mock.commit.assert_called()
    event_publisher_mock.publish.assert_called_once_with(action.events)
    action_repository.get_action_view.assert_called_once_with(
        action_id, current_project
    )


async def test_build_action_when_not_exists(
    app_container: ApplicationContainer,
) -> None:
    """Test build repository_action should raise an error when repository_action doesn't exist"""
    action_id = "action_id"
    current_project = "current_project"
    action = RepositoryAction(
        id=action_id,
        project=current_project,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action_in_project.return_value = None
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.build = mock.MagicMock()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action_service: RepositoryActionService = app_container.action_service()
    with pytest.raises(ActionNotFoundException):
        await action_service.build_action(action_id, current_project)
    action_repository.get_action_in_project.assert_called_once_with(
        action_id, current_project
    )
    action.build.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()
    event_publisher_mock.publish.assert_not_called()
    action_repository.get_action_view.assert_not_called()


def test_delete_action(app_container: ApplicationContainer) -> None:
    """Test delete repository_action"""
    action_id = "action_id"
    current_project = "current_project"
    action = RepositoryAction(
        id=action_id,
        project=current_project,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action_in_project.return_value = action
    action.can_be_deleted = mock.MagicMock()
    action.can_be_deleted.return_value = True
    action_service: RepositoryActionService = app_container.action_service()
    action_service.delete_action(action_id, current_project)
    action_repository.get_action_in_project.assert_called_once_with(
        action_id, current_project
    )
    action_repository.delete_action.assert_called_once_with(action)


def test_delete_action_when_not_exists(app_container: ApplicationContainer) -> None:
    """Test delete repository_action"""
    action_id = "action_id"
    current_project = "current_project"
    action = RepositoryAction(
        id=action_id,
        project=current_project,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action_in_project.return_value = None
    action.can_be_deleted = mock.MagicMock()
    action_service: RepositoryActionService = app_container.action_service()
    with pytest.raises(ActionNotFoundException):
        action_service.delete_action(action_id, current_project)
    action_repository.get_action_in_project.assert_called_once_with(
        action_id, current_project
    )
    action.can_be_deleted.assert_not_called()
    action_repository.delete_action.assert_not_called()


def test_delete_action_when_not_deletable(app_container: ApplicationContainer) -> None:
    """Test delete repository_action"""
    action_id = "action_id"
    current_project = "current_project"
    action = RepositoryAction(
        id=action_id,
        project=current_project,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action_in_project.return_value = action
    action.can_be_deleted = mock.MagicMock()
    action.can_be_deleted.return_value = False
    action_service: RepositoryActionService = app_container.action_service()
    with pytest.raises(ActionUnauthorizedDelete):
        action_service.delete_action(action_id, current_project)
    action_repository.get_action_in_project.assert_called_once_with(
        action_id, current_project
    )
    action.can_be_deleted.assert_called_once()
    action_repository.delete_action.assert_not_called()


def test_action_unbuild(app_container: ApplicationContainer) -> None:
    """Test unbuild a repository_action from repository_action deleted event"""
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionDeletedEvent(action_id=action_id)
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action.return_value = action
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.unbuild = mock.MagicMock()
    action_service: RepositoryActionService = app_container.action_service()
    action_service.action_unbuild(event)
    action.unbuild.assert_called_once()
    unit_of_work_mock.commit.assert_called()


def test_action_unbuild_when_action_not_exists(
    app_container: ApplicationContainer,
) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionDeletedEvent(action_id=action_id)
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action.side_effect = ActionNotFoundException
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.unbuild = mock.MagicMock()
    action_service: RepositoryActionService = app_container.action_service()
    with pytest.raises(ActionNotFoundException):
        action_service.action_unbuild(event)
    action_repository.get_action.assert_called_once_with(action_id)
    action.unbuild.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()


def test_action_build_start(app_container: ApplicationContainer) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionBuildStartEvent(action_id=action_id)
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.build_start = mock.MagicMock()
    action_repository.get_action.return_value = action
    action_service: RepositoryActionService = app_container.action_service()
    action_service.action_build_start(event)
    action_repository.get_action.assert_called_once_with(event.action_id)
    action.build_start.assert_called_once()
    unit_of_work_mock.commit.assert_called_once()


def test_action_build_start_when_not_exists(
    app_container: ApplicationContainer,
) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionBuildStartEvent(action_id=action_id)
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.build_start = mock.MagicMock()
    action_repository.get_action.return_value = None
    action_service: RepositoryActionService = app_container.action_service()
    action_service.logger = mock.MagicMock()
    action_service.action_build_start(event)
    action_repository.get_action.assert_called_once_with(event.action_id)
    action.build_start.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()
    action_service.logger.warning.assert_called_once()


async def test_action_build_success(app_container: ApplicationContainer) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionBuildSuccessEvent(action_id=action_id)
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action.build_success = mock.MagicMock()
    action_repository.get_action.return_value = action
    action_service: RepositoryActionService = app_container.action_service()
    await action_service.action_build_success(event)
    action_repository.get_action.assert_called_once_with(event.action_id)
    action.build_success.assert_called_once()
    event_publisher_mock.publish.assert_called_once_with(action.events)
    unit_of_work_mock.commit.assert_called_once()


async def test_action_build_success_when_not_exists(
    app_container: ApplicationContainer,
) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionBuildSuccessEvent(action_id=action_id)
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action.build_success = mock.MagicMock()
    action_repository.get_action.return_value = None
    action_service: RepositoryActionService = app_container.action_service()
    action_service.logger = mock.MagicMock()
    await action_service.action_build_success(event)
    action_repository.get_action.assert_called_once_with(event.action_id)
    action.build_success.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()
    event_publisher_mock.publish.assert_not_called()
    action_service.logger.warning.assert_called_once()


def test_action_build_error(app_container: ApplicationContainer) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionBuildErrorEvent(
        action_id=action_id, code=BuildErrorCode.ImageNameNotDefined
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.build_error = mock.MagicMock()
    action_repository.get_action.return_value = action
    action_service: RepositoryActionService = app_container.action_service()
    action_service.action_build_error(event)
    action_repository.get_action.assert_called_once_with(event.action_id)
    action.build_error.assert_called_once()
    unit_of_work_mock.commit.assert_called_once()


def test_action_build_error_when_not_exists(
    app_container: ApplicationContainer,
) -> None:
    action_id = "action_id"
    action = RepositoryAction(
        project="project",
        id=action_id,
        technical_name="tname",
        name="name",
        version="version",
        description="descr",
        type="type",
        kind="kind",
        archive=bytes(123),
        dynamic_outputs=False,
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        source_id="git_repo",
        sha="sha",
        owner_id="owner",
    )
    event = ActionBuildErrorEvent(
        action_id=action_id, code=BuildErrorCode.ErrorWhileRunningProcess
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action.build_error = mock.MagicMock()
    action_repository.get_action.return_value = None
    action_service: RepositoryActionService = app_container.action_service()
    action_service.logger = mock.MagicMock()
    action_service.action_build_error(event)
    action_repository.get_action.assert_called_once_with(event.action_id)
    action.build_error.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()
    action_service.logger.warning.assert_called_once()
