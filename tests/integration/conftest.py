import os
import shutil
from pathlib import Path
from unittest import mock

import pytest
import sqlalchemy
from aiohttp import test_utils, web
from sqlalchemy.orm import clear_mappers
from sqlalchemy.orm.session import _SessionClassMethods

from ryax.repository.application.authentication_service import AuthenticationService
from ryax.repository.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.auth.auth_token import AuthToken
from ryax.repository.infrastructure.api.setup import setup
from ryax.repository.infrastructure.database.engine import DatabaseEngine
from ryax.repository.infrastructure.database.metadata import metadata


@pytest.fixture()
async def app_container() -> ApplicationContainer:
    container = ApplicationContainer()
    container.config.jwt_secret_key.from_env("RYAX_JWT_SECRET_KEY", required=True)
    container.config.datastore_url.from_env("RYAX_DATASTORE", required=True)
    tmp_data_dir = Path("/tmp/ryax_test_data")
    os.environ["RYAX_TMP_DIR"] = str(tmp_data_dir)
    if not tmp_data_dir.is_dir():
        tmp_data_dir.absolute().mkdir()
    container.config.tmp_directory_path.from_env("RYAX_TMP_DIR", required=True)
    container.config.broker_url.from_env("RYAX_BROKER", required=True)
    container.config.log_level.from_env("RYAX_LOG_LEVEL", "INFO")
    container.config.authorization_api_base_url.from_env(
        "RYAX_AUTHORIZATION_API_BASE_URL", required=True
    )
    container.config.password_encryption_key.from_env(
        "RYAX_PASSWORD_ENCRYPTION_KEY", required=True
    )
    container.config.password_encryption_key.override(
        container.config.password_encryption_key().encode()
    )
    yield container
    if tmp_data_dir.is_dir():
        shutil.rmtree(tmp_data_dir)
    container.unwire()


@pytest.fixture()
def api_client(
    loop, app_container: ApplicationContainer, aiohttp_client
) -> test_utils.TestClient:
    app = web.Application()
    setup(app, app_container)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> DatabaseEngine:
    database_url = os.environ["RYAX_DATASTORE"]
    engine = DatabaseEngine(database_url)
    engine.connect()
    app_container.database_engine.override(engine)
    yield engine
    clear_mappers()
    metadata.drop_all(engine.connection)
    engine.disconnect()


@pytest.fixture()
def database_session(database_engine: DatabaseEngine) -> _SessionClassMethods:
    session = database_engine.get_session()
    yield session
    sqlalchemy.orm.close_all_sessions()


@pytest.fixture()
def authentication_service_mock(app_container: ApplicationContainer):
    authentication_service_mock = mock.MagicMock(AuthenticationService)
    authentication_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user")
    )
    app_container.authentication_service.override(authentication_service_mock)
    return authentication_service_mock


@pytest.fixture()
def project_authorization_service_mock(app_container: ApplicationContainer):
    project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
    project_authorization_service_mock.get_current_project = mock.AsyncMock(
        return_value="project_id"
    )
    app_container.project_authorization_service.override(
        project_authorization_service_mock
    )
    return project_authorization_service_mock
