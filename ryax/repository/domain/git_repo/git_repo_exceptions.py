# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass


@dataclass
class GitRepoNotFoundException(Exception):
    message: str = "Git repo not found"


@dataclass
class ErrorDuringGitRepoFetchException(Exception):
    message: str = "Unable to fetch Git repository, check branch and credentials"


@dataclass
class GitRepoHasNoScanResultException(Exception):
    message: str = "Git repo has no scan result and thus no actions to build"
