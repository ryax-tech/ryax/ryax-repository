#!/usr/bin/env bash

set -e
# For debug
# set -x

while getopts "fd:" opt; do
  case "$opt" in
    f)
      DO_CHECK_ONLY="false"
      ;;
    d)
      CHECK_DIR="$OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f] [-d directory]" >&2
      exit 1
      ;;
  esac
done

TO_CHECK_DIR=${CHECK_DIR:-"."}
CHECK_ONLY=${DO_CHECK_ONLY:-"true"}
RUFF_EXTRA_OPTS="--fix"

if [[ $CHECK_ONLY == "true" ]]
then
    BLACK_EXTRA_OPTS="--check --diff"
    RUFF_EXTRA_OPTS="--diff"
fi

echo "-- Checking for architecture errors"

if grep -qFrin "from ryax.repository.application." ryax/repository/infrastructure/
then
    echo -e "\e[31mWARNING\e[0m: Some infrastructure modules import application code:"
    grep -Frin --color "from ryax.repository.application." ryax/repository/infrastructure/
fi

if grep -qFrin "from ryax.repository.application." ryax/repository/domain
then
    echo -e "\e[31mWARNING\e[0m: Some domain modules import application code:"
    grep -Frin --color "from ryax.repository.application." ryax/repository/domain
fi

if grep -qFrin "from ryax/repository.infrastructure." ryax/repository/domain
then
    echo -e "\e[31mWARNING\e[0m: Some domain modules import infrastructure code:"
    grep -Frin --color "from ryax.repository.infrastructure." ryax/repository/domain
fi

# Detect cross-infrastructure includes, except towards utils.
for dir in $(ls -d  ryax/repository/infrastructure/*/)
do
    dir_name=$(basename $dir)
    if grep -Frin "from ryax.repository.infrastructure." $dir | grep -Fv "ryax.repository.infrastructure.utils" | grep -Fvq "ryax.repository.infrastructure.$dir_name"
    then
        echo -e "\e[31mWARNING\e[0m: There are cross-infrastructure includes in $dir_name:"
        grep -Frin "from ryax.repository.infrastructure." $dir | grep -Fv "ryax.repository.infrastructure.utils" | grep -Fv --color ryax.repository.infrastructure.$dir_name
    fi
done

echo "-- Checking python formating"
black $TO_CHECK_DIR --exclude "docs|ci|migrations|.*pb2.*.py|.poetry" $BLACK_EXTRA_OPTS

echo "-- Running python static checking"
ruff $TO_CHECK_DIR $RUFF_EXTRA_OPTS

echo "-- Checking type annotations"
mypy ./ryax --exclude /*pb2.py
