# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import and_, false

from ryax.repository.domain.git_repo.git_repo import GitRepo
from ryax.repository.domain.git_repo.git_repo_exceptions import GitRepoNotFoundException
from ryax.repository.domain.git_repo.git_repo_repository import IGitRepoRepository


class DatabaseGitRepoRepository(IGitRepoRepository):
    def __init__(self, session: Session):
        self.session = session

    def list_git_repos(self, current_project: str) -> List[GitRepo]:
        sources = (
            self.session.query(GitRepo)
            .filter(
                and_(
                    GitRepo.archived == false(),
                    GitRepo.project == current_project,
                )
            )
            .order_by(GitRepo.name)
            .all()
        )
        return sources

    def get_git_repo(self, git_repo_id: str, current_project: str) -> GitRepo:
        try:
            source = (
                self.session.query(GitRepo)
                .filter(
                    and_(
                        GitRepo.id == git_repo_id,
                        GitRepo.project == current_project,
                    )
                )
                .one()
            )
        except NoResultFound:
            raise GitRepoNotFoundException()
        if not source:
            raise GitRepoNotFoundException()
        else:
            return source

    def refresh_git_repo(self, git_repo: GitRepo) -> None:
        self.session.refresh(git_repo)

    def add_git_repo(self, git_repo: GitRepo) -> None:
        self.session.add(git_repo)
