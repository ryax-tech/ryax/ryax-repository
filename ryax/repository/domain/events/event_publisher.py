# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Union

from ryax.repository.domain.events.base_event import BaseEvent
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
    ActionCancelBuildEvent,
)


class IEventPublisher(abc.ABC):
    @abc.abstractmethod
    async def publish(
        self,
        events: List[
            Union[
                BaseEvent,
                ActionDeletedEvent,
                ActionBuildEvent,
                ActionBuildStartEvent,
                ActionBuildSuccessEvent,
                ActionBuildErrorEvent,
                ActionCancelBuildEvent,
            ]
        ],
    ) -> None:
        """Method used to publish domain event"""
        raise NotImplementedError
