# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import uuid
from dataclasses import dataclass, field
from datetime import datetime, timezone
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

from ryax.repository.domain.events.base_event import BaseEvent
from ryax.repository.domain.logo.logo import Logo
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
    ActionCancelBuildEvent,
)
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    RepositoryActionResourceRequestInvalidException,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionIOKind,
    RepositoryActionIOType,
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
    RepositoryActionWrapperType,
)

logger = logging.getLogger(__name__)


@dataclass
class RepositoryActionResources:
    id: str
    cpu: Optional[float] = None
    memory: Optional[int] = None
    time: Optional[float] = None
    gpu: Optional[int] = None

    @staticmethod
    def from_metadata(
        cpu: Optional[float] = None,
        memory: Optional[Union[int, str]] = None,
        time: Optional[Union[float, str]] = None,
        gpu: Optional[int] = None,
        minimum_memory_allocation: int = 32 * 1024**2,  # 32MB
    ) -> "RepositoryActionResources":
        times_to_seconds = {"s": 1, "m": 60, "h": 3600, "d": 86400}
        time_units = ",".join(times_to_seconds.keys())
        float_time: Optional[float] = None
        if isinstance(time, float) or isinstance(time, int):
            float_time = float(time)
        elif time is not None and float_time is None:
            try:
                if time[-1] not in times_to_seconds:
                    raise RepositoryActionResourceRequestInvalidException(
                        f"Time value must end with one of these units: {time_units}"
                    )
            except (TypeError, IndexError):
                raise RepositoryActionResourceRequestInvalidException(
                    f"Time value must end with one of these units: {time_units}"
                )
            try:
                float_time = float(time[:-1]) * times_to_seconds[time[-1]]
            except ValueError:
                raise RepositoryActionResourceRequestInvalidException(
                    f"Time value must be a float that end with one of these units: {time_units}"
                )

        if float_time is not None and float_time <= 0:
            raise RepositoryActionResourceRequestInvalidException(
                "Time value must not be zero or negative"
            )

        memory_in_bytes = {"K": 1024, "M": 1024**2, "G": 1024**3}
        memory_units = ",".join(memory_in_bytes.keys())
        int_memory: Optional[int] = None
        if isinstance(memory, int):
            int_memory = memory
        elif memory is not None and int_memory is None:
            try:
                if memory[-1] not in memory_units:
                    raise RepositoryActionResourceRequestInvalidException(
                        f"Memory value must end with one of these units: {memory_units}"
                    )
            except (TypeError, IndexError):
                raise RepositoryActionResourceRequestInvalidException(
                    f"Memory value must end with one of these units: {memory_units}"
                )
            try:
                int_memory = int(memory[:-1]) * memory_in_bytes[memory[-1]]
            except ValueError:
                raise RepositoryActionResourceRequestInvalidException(
                    f"Memory value must be a float that end with one of these units: {memory_units}"
                )

        if int_memory is not None and int_memory <= minimum_memory_allocation:
            raise RepositoryActionResourceRequestInvalidException(
                f"Memory value must not be less then {minimum_memory_allocation} bytes to be able to start the action runtime"
            )

        return RepositoryActionResources(
            id=str(uuid.uuid4()),
            cpu=float(cpu) if cpu is not None else None,
            memory=int_memory,
            time=float_time,
            gpu=int(gpu) if gpu is not None else None,
        )


@dataclass
class RepositoryActionIO:
    id: str
    technical_name: str
    display_name: str
    type: RepositoryActionIOType
    kind: RepositoryActionIOKind
    help: str
    enum_values: List[str] = field(init=True, default_factory=list)
    optional: bool = False
    default_value: Optional[str] = None

    @staticmethod
    def from_metadata(
        display_name: str,
        technical_name: str,
        help: str,
        type: RepositoryActionIOType,
        kind: RepositoryActionIOKind,
        enum_values: list = [],
        default_value: Optional[str] = None,
        optional: bool = False,
    ) -> "RepositoryActionIO":
        return RepositoryActionIO(
            id=str(uuid.uuid4()),
            technical_name=technical_name,
            display_name=display_name,
            type=type,
            kind=kind,
            help=help,
            enum_values=enum_values,
            default_value=default_value,
            optional=optional,
        )


@dataclass
class RepositoryAction:
    """
    The representation of Action in the repository microservice.
    This class has many default values that will all be treated when scanning a git repo.
    For a RepositoryAction to be buildable, almost all of the default values will be overwritten.
    In the case of errors in the scan, default values may still be present, and action will be in an errored state
    and thus is not buildable.
    """

    id: str
    project: str
    metadata_path: str
    creation_date: datetime
    owner_id: str
    technical_name: str = ""
    name: str = ""
    version: str = ""
    sha: str = ""
    description: str = ""
    type: RepositoryActionType = RepositoryActionType.UNDEFINED
    kind: RepositoryActionKind = RepositoryActionKind.UNDEFINED
    archive: bytes = b""
    # Blob of the lockfile to be displayed and downloaded
    # from the frontend.
    lockfile: bytes = b""
    dynamic_outputs: bool = False
    status: RepositoryActionStatus = RepositoryActionStatus.SCANNING
    source_id: Optional[str] = None
    build_date: Optional[datetime] = None
    submit_date: Optional[datetime] = None
    inputs: List[RepositoryActionIO] = field(default_factory=list)
    outputs: List[RepositoryActionIO] = field(default_factory=list)
    events: List[
        Union[
            BaseEvent,
            ActionDeletedEvent,
            ActionBuildEvent,
            ActionBuildStartEvent,
            ActionBuildSuccessEvent,
            ActionBuildErrorEvent,
            ActionCancelBuildEvent,
        ]
    ] = field(init=False)
    logo: Optional[Logo] = None
    resources: Optional[RepositoryActionResources] = None
    build_error: Optional[str] = None
    categories: List[str] = field(default_factory=list)
    scan_errors: List[str] = field(init=True, default_factory=list)
    duplicated: bool = False
    addons: Dict[str, dict] = field(init=True, default_factory=dict)

    def __new__(cls, **kwargs: Any) -> "RepositoryAction":
        instance = super(RepositoryAction, cls).__new__(cls)
        instance.events = []
        return instance

    @staticmethod
    def from_project_path_user(
        project_id: str,
        scan_path: str,
        action_path: Path,
        user_id: str,
        git_repo_id: str,
    ) -> "RepositoryAction":
        return RepositoryAction(
            id=str(uuid.uuid4()),
            project=project_id,
            creation_date=datetime.now(),
            metadata_path=str(Path(action_path).relative_to(scan_path)),
            owner_id=user_id,
            source_id=git_repo_id,
        )

    def set_new_action_version(self, existing_versions: List[str]) -> None:
        integer_versions = [v.split(".")[0] for v in existing_versions]
        self.version = str(
            (float(max([0] + [int(v) for v in integer_versions if v.isdigit()])) + 1.0)
        )

    def build_pending(self) -> None:
        allowed_statuses = [
            RepositoryActionStatus.SCANNED,
            RepositoryActionStatus.BUILD_ERROR,
            RepositoryActionStatus.BUILT,
            RepositoryActionStatus.CANCELLED,
        ]
        if self.status in allowed_statuses:
            self.status = RepositoryActionStatus.BUILD_PENDING
            self.submit_date = datetime.now()
        else:
            logger.warning(
                f"Action cannot be passed on pending build status {self.status}"
            )

    def build(self) -> None:
        allowed_statuses = [
            RepositoryActionStatus.BUILD_PENDING,
        ]
        if self.status in allowed_statuses:
            self.status = RepositoryActionStatus.STARTING
            wrapper_type_list: list[RepositoryActionWrapperType] = []

            if self.kind == RepositoryActionKind.SOURCE:
                wrapper_type_list.append(
                    RepositoryActionWrapperType.PYTHON3_GRPC_V1_TRIGGER
                )
            else:
                wrapper_type_list.append(RepositoryActionWrapperType.PYTHON3_GRPC_V1)
                wrapper_type_list.append(RepositoryActionWrapperType.PYTHON3_FILE_V1)
            request_event = ActionBuildEvent(
                action_id=self.id,
                action_type=self.type,
                action_kind=self.kind,
                action_version=self.version,
                action_archive=self.archive,
                action_owner_id=self.owner_id,
                action_project=self.project,
                action_technical_name=self.technical_name,
                action_wrapper_type_list=wrapper_type_list,
            )
            self.events.append(request_event)
        else:
            logger.warning("Action cannot be passed on build status")

    def unbuild(self) -> None:
        self.status = RepositoryActionStatus.SCANNED

    def build_start(self) -> None:
        if self.status == RepositoryActionStatus.STARTING:
            self.status = RepositoryActionStatus.BUILDING
        else:
            logger.warning(
                f"Action cannot enter BUILDING status because it is in status: {self.status}. Required status: {RepositoryActionStatus.BUILD_PENDING}"
            )

    def build_success(self) -> None:
        # Should we allow status=CANCELLING, or should we do a dedicated elif ?
        if (
            self.status == RepositoryActionStatus.BUILDING
            or self.status == RepositoryActionStatus.CANCELLING
        ):
            self.status = RepositoryActionStatus.BUILT
            self.build_date = datetime.now(timezone.utc)
        else:
            logger.warning(
                f"Action cannot move to BUILT status because it is in status {self.status}. Required status: {RepositoryActionStatus.BUILDING}"
            )

    def set_build_error(self, event: ActionBuildErrorEvent) -> None:
        if self.status == RepositoryActionStatus.BUILDING:
            self.status = RepositoryActionStatus.BUILD_ERROR
            self.build_error = f"Build error code: {event.code.name}\n{event.logs}"
        else:
            logger.warning(
                f"Action cannot move to BUILD ERROR status because it is in status {self.status}. Required status: {RepositoryActionStatus.BUILDING}"
            )

    def set_build_cancelled(self) -> None:
        if self.status == RepositoryActionStatus.CANCELLING:
            self.status = RepositoryActionStatus.CANCELLED
        else:
            logger.warning(
                f"Action cannot move to BUILD CANCELLED status because it is in status {self.status}. Required status: {RepositoryActionStatus.CANCELLING}"
            )

    def cancel_build(self) -> None:
        if self.status in [
            RepositoryActionStatus.BUILDING,
            RepositoryActionStatus.STARTING,
        ]:
            self.status = RepositoryActionStatus.CANCELLING
        elif self.status == RepositoryActionStatus.BUILD_PENDING:
            self.status = RepositoryActionStatus.CANCELLED

    def can_be_deleted(self) -> bool:
        return self.status in [
            RepositoryActionStatus.BUILT,
            RepositoryActionStatus.BUILD_ERROR,
            RepositoryActionStatus.SCANNED,
            RepositoryActionStatus.SCAN_ERROR,
        ]

    def is_in_building_process(self) -> bool:
        return self.status in [
            RepositoryActionStatus.BUILD_PENDING,
            RepositoryActionStatus.BUILDING,
            RepositoryActionStatus.BUILD_ERROR,
        ]

    def is_building(self) -> bool:
        return self.status in [
            RepositoryActionStatus.BUILD_PENDING,
            RepositoryActionStatus.BUILDING,
        ]

    def is_built(self) -> bool:
        return self.status is RepositoryActionStatus.BUILT

    def is_scanned(self) -> bool:
        return self.status is RepositoryActionStatus.SCANNED

    def is_error(self) -> bool:
        return self.status in [
            RepositoryActionStatus.BUILD_ERROR,
            RepositoryActionStatus.SCAN_ERROR,
        ]

    def add_scan_error(self, error_message: str) -> None:
        self.scan_errors.append(error_message)

    @property
    def has_scan_error(self) -> bool:
        return bool(self.scan_errors)

    def add_action_input(self, action_input: RepositoryActionIO) -> None:
        self.inputs.append(action_input)

    def add_action_output(self, action_output: RepositoryActionIO) -> None:
        self.outputs.append(action_output)

    def set_logo(self, logo: Logo) -> None:
        self.logo = logo

    def set_lockfile(self, lockfile: bytes) -> None:
        self.lockfile = lockfile
