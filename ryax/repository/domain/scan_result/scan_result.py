# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import uuid
from dataclasses import dataclass, field
from datetime import datetime, timezone
from operator import attrgetter
from typing import List, Optional

from ryax.repository.domain.repository_action.repository_action import RepositoryAction


@dataclass
class ScanResultStatus:
    """
    foo
    """

    modules_success: int
    modules_in_progress: int
    modules_ready: int
    modules_error: int


@dataclass
class ScanResult:
    id: str
    scan_date: datetime
    project: str
    sha: str
    status: Optional[ScanResultStatus] = None
    actions: List[RepositoryAction] = field(init=True, default_factory=list)

    @staticmethod
    def from_project_and_sha(project_id: str, sha: str) -> "ScanResult":
        return ScanResult(
            id=str(uuid.uuid4()),
            scan_date=datetime.now(timezone.utc),
            project=project_id,
            sha=sha,
        )

    def set_project(self, project: str) -> None:
        self.project = project

    def set_sha(self, sha: str) -> None:
        self.sha = sha

    def add_action(self, action: RepositoryAction) -> None:
        self.actions.append(action)

    def get_built_actions(self) -> List[RepositoryAction]:
        return [action for action in self.actions if action.is_built()]

    def get_building_actions(self) -> List[RepositoryAction]:
        return [action for action in self.actions if action.is_building()]

    def get_scanned_actions(self) -> List[RepositoryAction]:
        return [action for action in self.actions if action.is_scanned()]

    def get_action_error(self) -> List[RepositoryAction]:
        return [action for action in self.actions if action.is_error()]

    def refresh_status(self) -> None:
        modules_success = len(self.get_built_actions())
        modules_building = len(self.get_building_actions())
        modules_ready = len(self.get_scanned_actions())
        modules_error = len(self.get_action_error())
        self.status = ScanResultStatus(
            modules_success, modules_building, modules_ready, modules_error
        )

    def sort_actions(
        self, sorting: Optional[str] = None, sort_descending: bool = False
    ) -> None:
        if sorting == "name":
            self.actions = sorted(
                self.actions,
                key=lambda mbr: str(attrgetter("name")(mbr)).lower(),
                reverse=sort_descending,
            )
        elif sorting == "status":
            self.actions = sorted(
                self.actions, key=attrgetter("status.value"), reverse=sort_descending
            )
        else:
            pass

    def remove_action_if_exists(self, action: RepositoryAction) -> None:
        action_to_remove = self.get_action(action)
        if not action_to_remove:
            logger = logging.getLogger("ScanResult")
            logger.warning(
                f"A action with that name {action.technical_name} and version {action.version} already exists."
            )
        else:
            self.actions.remove(action)

    def get_action(self, action: RepositoryAction) -> Optional[RepositoryAction]:
        return next((item for item in self.actions if item.id == action.id), None)
