# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

import aiohttp

from ryax.repository.domain.services.authorization_api_service import (
    IAuthorizationApiService,
)

logger = logging.getLogger(__name__)


class AuthorizationApiService(IAuthorizationApiService):
    def __init__(self, authorization_api_base_url: str):
        self.authorization_api_base_url = authorization_api_base_url

    async def get_current_project(self, user_id: str, authorization_token: str) -> str:
        url = (
            f"http://{self.authorization_api_base_url}/projects/users/{user_id}/current"
        )
        logger.info(f"trying te get current project on {url}")
        try:
            async with aiohttp.client.ClientSession() as session:
                async with session.get(
                    url,
                    headers={"authorization": authorization_token},
                ) as response:
                    response_json = await response.json()
                    logger.info(f"response for current project {response_json}")
                    return response_json.get("id")
        except Exception as err:
            logger.exception(
                f"When attempting to get current project id on : {url}, got an error {err}"
            )
            return ""
