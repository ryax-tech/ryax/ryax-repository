from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.infrastructure.database.engine import Session


class TestRepositoryIntegrationV2Endpoints:
    async def setup_class_variables(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ):
        self.database_session = database_session
        self.project_authorization_service_mock = project_authorization_service_mock
        self.authentication_service_mock = authentication_service_mock
        self.api_client = api_client

    async def _ensure_no_git_repos_exist_v2(self) -> None:
        response = await self.api_client.get("v2/sources")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 0

    async def _create_default_actions_git_repo(self) -> dict:
        response = await self.api_client.post(
            "/sources",
            data={
                "name": "Default Actions",
                "url": "https://gitlab.com/ryax-tech/workflows/default-actions",
            },
        )
        assert response.status == 201
        result = await response.json()
        assert result["name"] == "Default Actions"
        assert result["username"] is None
        assert result["url"] == "https://gitlab.com/ryax-tech/workflows/default-actions"
        assert result["password_is_set"] is False
        assert result["scan_result"] is None
        return result

    async def _create_errored_actions_git_repo(self) -> dict:
        response = await self.api_client.post(
            "/sources",
            data={
                "name": "Errored Actions",
                "url": "https://gitlab.com/ryax-tech/ryax/errored-actions",
            },
        )
        assert response.status == 201
        result = await response.json()
        assert result["name"] == "Errored Actions"
        assert result["username"] is None
        assert result["url"] == "https://gitlab.com/ryax-tech/ryax/errored-actions"
        assert result["password_is_set"] is False
        assert result["scan_result"] is None
        return result

    async def _get_git_repo_by_id_v2(self, git_repo_id: str) -> dict:
        response = await self.api_client.get(f"v2/sources/{git_repo_id}")
        assert response.status == 200
        result = await response.json()
        return result

    async def _scan_git_repo_by_id_v2(self, git_repo_id: str) -> dict:
        """Scans default modules and makes sure they all match the following criteria:
        - Status is SCANNED
        - There are no scan errors"""
        response = await self.api_client.post(f"/v2/sources/{git_repo_id}/scan")
        assert response.status == 200
        result = await response.json()
        return result

    async def _delete_git_repo_by_id(self, git_repo_id: str):
        response = await self.api_client.delete(f"/sources/{git_repo_id}")
        assert response.status == 200
        response = await self.api_client.get("/sources")
        result = await response.json()
        assert not any(git_repo["id"] == git_repo_id for git_repo in result)

    async def _delete_all_actions(self):
        response = await self.api_client.get("/modules")
        assert response.status == 200
        all_actions = await response.json()
        for action in all_actions:
            response = await self.api_client.delete(f"/modules/{action['id']}")
            assert response.status == 200
        response = await self.api_client.get("/modules")
        assert response.status == 200
        all_actions = await response.json()
        assert not all_actions

    async def _get_git_repos_v2(self) -> dict:
        response = await self.api_client.get("v2/sources")
        assert response.status == 200
        result = await response.json()
        return result

    async def test_v2_endpoints_with_default_and_errored_actions(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ):
        await self.setup_class_variables(
            database_session,
            project_authorization_service_mock,
            authentication_service_mock,
            api_client,
        )

        await self._ensure_no_git_repos_exist_v2()

        # Use default and errored actions
        default_actions_git_repo = await self._create_default_actions_git_repo()
        errored_actions_git_repo = await self._create_errored_actions_git_repo()

        all_git_repos = await self._get_git_repos_v2()
        assert len(all_git_repos) == 2 and all(
            repo["last_scan_summary"] is None for repo in all_git_repos
        )

        # Scan default actions
        default_actions_repo_scan_result = await self._scan_git_repo_by_id_v2(
            default_actions_git_repo["id"]
        )
        assert default_actions_repo_scan_result["last_scan"] is not None
        assert (
            len(default_actions_repo_scan_result["last_scan"]["not_built_actions"]) > 0
        )
        assert all(
            action["status"] == RepositoryActionStatus.SCANNED.value
            for action in default_actions_repo_scan_result["last_scan"][
                "not_built_actions"
            ]
        )
        assert not default_actions_repo_scan_result["last_scan"]["built_actions"]

        # Scan errored actions
        errored_actions_git_repo_scan_result = await self._scan_git_repo_by_id_v2(
            errored_actions_git_repo["id"]
        )
        assert errored_actions_git_repo_scan_result["last_scan"] is not None
        assert len(
            errored_actions_git_repo_scan_result["last_scan"]["not_built_actions"]
        )

        # Check the v2 GET returns correct data
        all_git_repos = await self._get_git_repos_v2()
        assert len(all_git_repos) == 2
        for git_repo in all_git_repos:
            if git_repo["name"] == "Default Actions":
                assert git_repo["last_scan_summary"] is not None
                assert git_repo["last_scan_summary"]["actions_scanned"] > 0
                assert git_repo["last_scan_summary"]["actions_built"] == 0
                assert git_repo["last_scan_summary"]["actions_error"] == 0
                assert git_repo["last_scan_summary"]["actions_building"] == 0

            else:
                assert git_repo["last_scan_summary"] is not None
                # 2 errored actions scan since they have errors in the build phase
                assert git_repo["last_scan_summary"]["actions_scanned"] > 0
                assert git_repo["last_scan_summary"]["actions_error"] > 0
                assert git_repo["last_scan_summary"]["actions_built"] == 0
                assert git_repo["last_scan_summary"]["actions_building"] == 0

        # Now that there are scan results, get each repo individually and ensure the correct data is there
        default_actions_get_result = await self._get_git_repo_by_id_v2(
            default_actions_git_repo["id"]
        )
        assert default_actions_get_result["last_scan"] is not None
        assert len(default_actions_get_result["last_scan"]["not_built_actions"]) > 0
        assert all(
            action["status"] == RepositoryActionStatus.SCANNED.value
            for action in default_actions_get_result["last_scan"]["not_built_actions"]
        )
        assert not default_actions_get_result["last_scan"]["built_actions"]

        errored_actions_get_result = await self._get_git_repo_by_id_v2(
            errored_actions_git_repo_scan_result["id"]
        )
        assert errored_actions_get_result["last_scan"] is not None
        assert len(errored_actions_get_result["last_scan"]["not_built_actions"])

        # Delete everything and pretend this all never happened
        await self._delete_git_repo_by_id(default_actions_git_repo["id"])
        await self._delete_git_repo_by_id(errored_actions_git_repo["id"])
        await self._delete_all_actions()
