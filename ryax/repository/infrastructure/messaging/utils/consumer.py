# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import Logger, getLogger
from typing import Callable, Coroutine, Dict, Optional, Type

from aio_pika import ExchangeType
from aio_pika.abc import AbstractChannel, AbstractIncomingMessage
from google.protobuf.message import Message

from ryax.repository.infrastructure.messaging.utils.engine import MessagingEngine

MessageDefinition = Type[Message]

MessageHandler = Callable[[AbstractIncomingMessage], Coroutine]

logger: Logger = getLogger(__name__)


class MessagingConsumer:
    """Class to handle global system messages (aka. Domain events)"""

    def __init__(self, engine: MessagingEngine):
        self.engine: MessagingEngine = engine
        self.channel: Optional[AbstractChannel] = None
        self.handlers: Dict[str, MessageHandler] = {}

    def register_handler(
        self,
        message_type: str,
        message_handler: MessageHandler,
    ) -> None:
        """Method to register handler"""
        self.handlers[message_type] = message_handler

    async def handle_message(self, message: AbstractIncomingMessage) -> None:
        """Method to handle received message"""
        async with message.process():
            message_handler = (
                self.handlers[message.type] if message.type in self.handlers else None
            )
            if message_handler:
                logger.debug(f"Message received : {message}")
                logger.debug(f"Using handler {message_handler}")
                await message_handler(message)
            else:
                logger.debug(
                    f"Message controller not registered for type: {message.type}"
                )

    async def start(self) -> None:
        """Method to start message consuming"""
        logger.info("Start message consuming")
        self.channel = await self.engine.get_channel()
        await self.channel.set_qos(prefetch_count=1)
        exchange = await self.channel.declare_exchange(
            "domain_events", ExchangeType.TOPIC, durable=True
        )
        queue = await self.channel.declare_queue("repository_events", durable=True)
        await queue.bind(exchange, "ActionBuilder.*")
        await queue.bind(exchange, "Studio.ActionDeleted")
        await queue.consume(self.handle_message)
