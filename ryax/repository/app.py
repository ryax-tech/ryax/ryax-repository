# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from aiohttp import web

from ryax.repository.container import ApplicationContainer
from ryax.repository.infrastructure.api.setup import setup as api_setup
from ryax.repository.infrastructure.messaging.setup import setup as messaging_setup


def init() -> web.Application:
    """Init application"""
    # Init application container
    container = ApplicationContainer()

    # Setup configuration
    container.config.jwt_secret_key.from_env("RYAX_JWT_SECRET_KEY", required=True)
    container.config.datastore_url.from_env("RYAX_DATASTORE", required=True)
    container.config.tmp_directory_path.from_env("RYAX_TMP_DIR", required=True)
    container.config.broker_url.from_env("RYAX_BROKER", required=True)
    container.config.log_level.from_env("RYAX_LOG_LEVEL", "INFO")
    container.config.authorization_api_base_url.from_env(
        "RYAX_AUTHORIZATION_API_BASE_URL", required=True
    )
    container.config.password_encryption_key.from_env(
        "RYAX_PASSWORD_ENCRYPTION_KEY", required=True
    )
    container.config.password_encryption_key.override(
        container.config.password_encryption_key().encode()
    )

    # Setup logging
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    str_level = container.config.log_level()
    numeric_level = getattr(logging, str_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)
    logging.getLogger().setLevel(numeric_level)
    logger = logging.getLogger(__name__)
    logger.info("Logging level is set to %s" % str_level.upper())

    # Init application
    app: web.Application = web.Application()
    api_setup(app, container)
    app["container"] = container

    # Setup consumer
    consumer = container.messaging_consumer()
    messaging_setup(consumer, container)

    return app


async def on_startup(app: web.Application) -> None:
    """Define hook when application start"""
    container: ApplicationContainer = app["container"]
    container.database_engine().connect()
    await container.messaging_engine().connect()
    await container.messaging_consumer().start()


async def on_cleanup(app: web.Application) -> None:
    """Define hook when application stop"""
    container: ApplicationContainer = app["container"]
    container.database_engine().disconnect()
    await container.messaging_engine().disconnect()


def start() -> None:
    app: web.Application = init()
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)
    web.run_app(app)


def db_migration() -> None:
    import alembic.config

    alembic_args = ["upgrade", "head"]
    alembic.config.main(argv=alembic_args)
