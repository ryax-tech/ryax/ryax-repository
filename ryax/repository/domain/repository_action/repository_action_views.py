# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)


@dataclass
class SourceView:
    id: str
    name: str
    project: str


@dataclass
class ActionView:
    id: str
    name: str
    technical_name: str
    version: str
    description: str
    lockfile: bytes
    status: RepositoryActionStatus
    creation_date: datetime
    source: SourceView
    project: str
    build_error: Optional[str] = None
