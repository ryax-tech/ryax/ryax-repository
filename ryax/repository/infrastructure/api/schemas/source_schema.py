# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.repository.infrastructure.api.schemas.scan_result_schema import (
    ScanResultBarSchema,
    ScanResultRepositoryActionSchemaV1,
)


class GitRepoPreviewSchemaV1(Schema):
    id = fields.String(
        metadata={
            "description": "Identifier of the git_repo",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )
    name = fields.String(
        metadata={
            "description": "Name of the git_repo",
            "example": "Gitlab Repository",
        },
    )


class GitRepoSchemaV1(GitRepoPreviewSchemaV1):
    url = fields.String(
        metadata={"description": "Source url", "example": "github.com/project"},
    )

    scan_result = fields.Nested(
        ScanResultBarSchema,
        metadata={"description": "Result of the last scan of this git_repo"},
    )
    username = fields.String(
        metadata={"description": "Source username", "example": "ryaxGitlabUser"},
    )
    password_is_set = fields.Boolean(
        metadata={
            "description": "Boolean indicating id the password is non-null",
            "example": True,
        },
    )


class GitRepoDetailsSchemaV1(GitRepoPreviewSchemaV1):
    url = fields.String(
        metadata={"description": "Source url", "example": "github.com/project"},
    )

    last_scan = fields.Nested(
        ScanResultRepositoryActionSchemaV1,
        metadata={
            "description": "Result of the last scan of this git_repo",
        },
        attribute="scan_result",
    )
