import os
from datetime import datetime

import pytest
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import clear_mappers, scoped_session, sessionmaker

from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
)
from ryax.repository.infrastructure.database.engine import Session
from ryax.repository.infrastructure.database.mapper import start_mapping
from ryax.repository.infrastructure.database.metadata import metadata


@pytest.fixture()
def app_container(database_session: Session) -> ApplicationContainer:
    container = ApplicationContainer()
    return container


@pytest.fixture(scope="function")
def database_engine() -> Engine:
    database_url = os.environ["RYAX_DATASTORE"]
    engine: Engine = create_engine(database_url)
    metadata.create_all(engine)
    start_mapping()
    yield engine
    clear_mappers()
    metadata.drop_all(engine)
    engine.dispose()


@pytest.fixture(scope="function")
def database_session_factory(database_engine: Engine):
    return scoped_session(sessionmaker(bind=database_engine))


@pytest.fixture(scope="function")
def database_session(database_session_factory: Session):
    session = database_session_factory()
    yield session
    session.close()


@pytest.fixture(scope="function")
def generate_action(database_session: Session):
    def _generate(
        id: str = None,
        name: str = None,
        technical_name: str = None,
        version: str = None,
        description: str = None,
        type: RepositoryActionType = None,
        kind: RepositoryActionKind = None,
        status: RepositoryActionStatus = None,
        archive: bytes = None,
        dynamic_outputs: bool = None,
        creation_date: datetime = None,
        source_id: str = None,
        owner_id: str = None,
        logo_id: str = None,
        lockfile: bytes = b"",
        categories: [str] = [],
        project: str = None,
        sha: str = None,
        metadata_path: str = "",
    ):
        action_raw = {
            "id": id,
            "name": name,
            "technical_name": technical_name,
            "version": version,
            "description": description,
            "type": type,
            "kind": kind,
            "status": status,
            "archive": archive,
            "dynamic_outputs": dynamic_outputs,
            "creation_date": creation_date,
            "source_id": source_id,
            "owner_id": owner_id,
            "logo_id": logo_id,
            "categories": categories,
            "project": project,
            "lockfile": lockfile,
            "sha": sha,
            "metadata_path": metadata_path,
        }
        database_session.execute(
            "INSERT INTO repository_action (id, name, technical_name, version, description, lockfile, type, kind, status, archive, dynamic_outputs, creation_date, source_id, owner_id, logo_id, categories, project, sha, metadata_path)"
            "VALUES (:id, :name, :technical_name, :version, :description, :lockfile, :type, :kind, :status, :archive, :dynamic_outputs, :creation_date, :source_id, :owner_id, :logo_id, :categories, :project, :sha, :metadata_path)",
            action_raw,
        )
        return action_raw

    return _generate


@pytest.fixture(scope="function")
def generate_logo(database_session: Session):
    def _generate(
        id: str = None,
        name: str = None,
        extension: str = None,
        content: bytes = None,
    ):
        logo_raw = {
            "id": id,
            "name": name,
            "extension": extension,
            "content": content,
        }
        database_session.execute(
            "INSERT INTO logo (id, name, extension, content)"
            "VALUES (:id, :name, :extension, :content)",
            logo_raw,
        )
        return logo_raw

    return _generate
