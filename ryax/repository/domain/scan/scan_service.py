# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from pathlib import Path
from typing import Optional

from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.scan.scan import Scan
from ryax.repository.domain.scan.scan_values import ScanGitRepoRequest


class IRepositoryScanService(abc.ABC):
    @abc.abstractmethod
    async def scan_source_code(
        self,
        scan_git_repo_request: ScanGitRepoRequest,
        project: str,
        git_repo_url: str,
        git_repo_username: Optional[str] = None,
        git_repo_password: Optional[str] = None,
    ) -> Scan:
        """Method should load git_repo code"""
        pass

    @abc.abstractmethod
    def set_git_sha(self, scan: Scan) -> None:
        """Method to analyze git_repo code and return parsed actions"""
        pass

    @abc.abstractmethod
    def parse_action_data(
        self, action: RepositoryAction, action_metadata_path: Path
    ) -> None:
        pass

    @abc.abstractmethod
    def clear_source_code(self, scan: Scan) -> None:
        """Method to clean git_repo code"""
        pass
