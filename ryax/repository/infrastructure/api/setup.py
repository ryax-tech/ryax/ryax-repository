# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from ryax.repository.container import ApplicationContainer
from ryax.repository.infrastructure.api.controllers import (
    git_repo_controller,
    repository_action_controller,
    util_controller,
)
from ryax.repository.infrastructure.api.middlewares import (
    authentication_middleware,
    current_project_id_middleware,
)


def setup(app: web.Application, container: ApplicationContainer) -> None:
    """Method to setup api"""
    # Configure application container for wiring
    container.wire(
        modules=[
            authentication_middleware,
            current_project_id_middleware,
            git_repo_controller,
            repository_action_controller,
            util_controller,
        ]
    )

    # Setup server middlewares
    app.middlewares.extend(
        [
            authentication_middleware.check_token(
                public_paths=["/docs", "/static", "/healthz"]
            ),
            current_project_id_middleware.handle(
                public_paths=["/docs", "/static", "/healthz"]
            ),
            validation_middleware,
        ]
    )

    # Setup server handlers

    app.add_routes(
        [
            web.get("/healthz", util_controller.health_check, allow_head=False),
            web.get(
                "/sources", git_repo_controller.list_repositories, allow_head=False
            ),
            web.get(
                "/v2/sources", git_repo_controller.list_git_repos, allow_head=False
            ),
            web.post("/sources", git_repo_controller.add_repository),
            web.get(
                "/sources/{source_id}",
                git_repo_controller.get_repository,
                allow_head=False,
            ),
            web.get(
                "/v2/sources/{source_id}",
                git_repo_controller.get_git_repo,
                allow_head=False,
            ),
            web.put("/sources/{source_id}", git_repo_controller.update_repository),
            web.delete("/sources/{source_id}", git_repo_controller.delete_repository),
            web.post("/v2/sources/{source_id}/scan", git_repo_controller.scan_git_repo),
            web.get(
                "/modules", repository_action_controller.list_actions, allow_head=False
            ),
            web.delete(
                "/modules/{module_id}", repository_action_controller.delete_action
            ),
            web.post(
                "/modules/{module_id}/build", repository_action_controller.build_action
            ),
            web.post(
                "/v2/sources/{source_id}/build", git_repo_controller.build_all_actions
            ),
            web.post(
                "/v2/actions/{action_id}/cancel_build",
                repository_action_controller.cancel_build,
            ),
            web.post(
                "/v2/sources/{source_id}/cancel_all_builds",
                git_repo_controller.cancel_all_builds,
            ),
        ]
    )

    # Setup application request documentation
    setup_aiohttp_apispec(
        app,
        title="Repository Api",
        version="0.0.1",
        url="/docs/swagger.json",
        swagger_path="/docs",
        static_path="/static/swagger",
        securityDefinitions={
            "bearer": {
                "type": "apiKey",
                "in": "header",
                "name": "Authorization",
                "description": "Authorization token",
            }
        },
        security=[{"bearer": []}],
    )
