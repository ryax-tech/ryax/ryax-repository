# Ryax Repository

Ryax repository (code source) management microservice.
This microservice manage sources of code by using git repositories.
And orchestrate module builds and store module definitions for the other part of the plateform.


## Requirements

Assume that you have the following dependencies installed on your system :

* Python 3.9 ([Installation Guide](https://wiki.python.org/moin/BeginnersGuide/))
* Poetry ([Installation Guide](https://python-poetry.org/docs/#installation))
* Docker ([Install Guide](https://docs.docker.com/get-docker/))

## Quick start

1. Open terminal in project and run ``docker-compose up`` to start development services.
2. Open other terminal in project and run ``poetry install`` to install dependencies.
3. Run ``poetry shell`` to activate the virtualenv.
4. Run ``export $(grep -v '^#' default.env | xargs -0)`` to load environment definition from file.
5. Run ``python main.py`` to start application.


## Linting

To run linter on code, use following command :

```bash
poetry run ./lint.sh
```

You can use the `-f` option to apply the autoformat :

```bash
poetry run ./lint.sh -f
```

### Setup git pre commit hook

It is recommended to create a pre-commit hook on git and make it executable:

```bash
cp ./pre-commit.sh ./.git/hooks/pre-commit
chmod +x ./.git/hooks/pre-commit
```

## Database migration (Alembic)

**IMPORTANT :** Commands should be run in a virtual environment shell to use defined environment variables.
 
To open a shell in the virtual environment, by running following commands : 
1. Run ``poetry shell`` to activate the virtualenv.
2. Run ``export $(grep -v '^#' default.env | xargs -0)`` to load environment definition from file.

You need to have a current version of the database deployed and running.

```bash
docker compose up -d
```

Make sure the new database is on the last revision.

```bash
alembic upgrade head
```

Use the following command to generate database migration script:

```bash
alembic revision -m "<script_migration_message>" --autogenerate
```

Upgrade the database again to contain the last changes.

```bash
alembic upgrade head
```

Migration scripts are generated in ``./migrations/version`` directory.
Add and commit the newly generated file to git.

## Message compilation (Protobuff)

Use the following command to compile protobuf message definition (.proto file):

```bash
python -m grpc_tools.protoc  --mypy_out=. -I./ --python_out=. ./ryax/repository/infrastructure/messaging/messages/*.proto
```
