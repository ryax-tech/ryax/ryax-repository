import datetime

import pytest

from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionNotFoundException,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
    RepositoryActionKind,
    RepositoryActionType,
)
from ryax.repository.infrastructure.database.engine import Session
from ryax.repository.infrastructure.database.repositories.action_repository import (
    DatabaseActionRepository,
)


def test_get_action(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action,
) -> None:
    """Get repository_action by id should return repository_action when exists"""
    action_id = "b221ef40-9aa9-4b07-adcb-fe7a94d2b8b4"
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    action = generate_action(
        id=action_id,
        name="Test-repository_action-name1",
        technical_name="valid-test-technical-name",
        status=RepositoryActionStatus.SCANNED.value,
        dynamic_outputs=False,
        creation_date=datetime.datetime.now(),
        lockfile=b"",
        owner_id="user",
        project=project_id,
        version="test",
        type="python3",
        kind=RepositoryActionKind.SOURCE.value,
        sha="ton",
        metadata_path="tis",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    result = action_repository.get_action_in_project(action_id, project_id)

    print("scan errors:", result.scan_errors)

    assert result == RepositoryAction(
        id=action_id,
        name=action["name"],
        technical_name=action["technical_name"],
        status=RepositoryActionStatus(action["status"]),
        dynamic_outputs=action["dynamic_outputs"],
        creation_date=action["creation_date"],
        owner_id=action["owner_id"],
        project=action["project"],
        version=action["version"],
        lockfile=action["lockfile"],
        description=action["description"],
        type=RepositoryActionType(action["type"]),
        kind=RepositoryActionKind(action["kind"]),
        archive=action["archive"],
        source_id=action["source_id"],
        sha=action["sha"],
        metadata_path=action["metadata_path"],
    )


def test_get_action_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
) -> None:
    """Get repository_action by id should return None when not exists"""
    action_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    with pytest.raises(ActionNotFoundException):
        action_repository.get_action_in_project(action_id, project_id)


def test_get_twins(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action,
) -> None:
    action_id = "b221ef40-9aa9-4b07-adcb-fe7a94d2b8b4"
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    action = generate_action(
        id=action_id,
        name="Test-repository_action-name1",
        technical_name="valid-test-technical-name",
        status=RepositoryActionStatus.SCANNED.value,
        dynamic_outputs=False,
        creation_date=datetime.datetime.now(),
        owner_id="user",
        project=project_id,
        version="test",
        type="python3",
        kind=RepositoryActionKind.SOURCE.value,
        sha="ton",
        metadata_path="tis",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_matching_actions(
        "valid-test-technical-name", "test", project_id
    ) == [
        RepositoryAction(
            id=action_id,
            name=action["name"],
            technical_name=action["technical_name"],
            status=RepositoryActionStatus(action["status"]),
            dynamic_outputs=action["dynamic_outputs"],
            creation_date=action["creation_date"],
            owner_id=action["owner_id"],
            project=action["project"],
            version=action["version"],
            description=action["description"],
            type=RepositoryActionType(action["type"]),
            kind=RepositoryActionKind(action["kind"]),
            archive=action["archive"],
            source_id=action["source_id"],
            sha=action["sha"],
            metadata_path=action["metadata_path"],
        )
    ]


def test_get_action_versions_by_technical_name(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action,
) -> None:
    current_project = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    action_technical_names = "technical_name"
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="b221ef40-9aa9-4b07-adcb-fe7a94d2b8b4",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=current_project,
        sha="ton",
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=action_technical_names,
        type="python3",
        version="1.0",
    )
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="86f56a95-395d-4d98-8921-b5bd8e4a959f",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="name1",
        owner_id="user",
        project=current_project,
        sha="ton",
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=action_technical_names,
        type="python3",
        version="2.0",
    )

    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="aa4372ba-f625-4456-a428-b6c6144e9bab",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="name1",
        owner_id="user",
        technical_name=action_technical_names,
        project=current_project,
        sha="ton",
        status=RepositoryActionStatus.SCANNED.value,
        type="python3",
        version="2.5",
    )
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="13e5267d-de69-4549-ad14-abeedea2ecd0",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="name1",
        owner_id="user",
        project=current_project,
        sha="ton",
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=action_technical_names,
        type="python3",
        version="2.9",
    )
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="c8200442-5401-4e6f-8310-2e49cfc6d1da",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=current_project,
        sha="ton",
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=action_technical_names,
        type="python3",
        version="non_numeric_version",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_action_versions_by_technical_name(
        action_technical_names, current_project=current_project
    ) == ["1.0", "2.0", "2.5", "2.9", "non_numeric_version"]


def test_get_new_action_versions_when_no_actions(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action,
) -> None:
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    desired_technical_name = "technical_name"
    action_undesired_technical_name = "undesired"
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="b6611a3c-db86-40fb-a015-ce830ffb9082",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=project_id,
        sha="ton",
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=action_undesired_technical_name,
        type="python3",
        version="1.0",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert (
        action_repository.get_action_versions_by_technical_name(
            desired_technical_name, project_id
        )
        == []
    )


def test_get_latest_version_of_action_with_same_sha(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action,
) -> None:
    action_id = "b221ef40-9aa9-4b07-adcb-fe7a94d2b8b4"
    action_id_2 = "b221ef40-9aa9-4b07-adcb-fe7a94d2b8b5"
    action_id_3 = "b221ef40-9aa9-4b07-adcb-fe7a94d2b8b6"
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    technical_name = "technical_name"
    sha = "901h236"
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id=action_id,
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=project_id,
        sha=sha,
        status=RepositoryActionStatus.SCANNED.value,
        technical_name="another_technical_name",
        type="python3",
        version="1.0",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert (
        action_repository.get_latest_version_of_action_with_same_sha(
            technical_name, sha, project_id
        )
        is None
    )
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id=action_id_2,
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=project_id,
        sha=sha,
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=technical_name,
        type="python3",
        version="1.0",
    )
    assert (
        action_repository.get_latest_version_of_action_with_same_sha(
            technical_name, sha, project_id
        )
        == "1.0"
    )
    generate_action(
        creation_date=datetime.datetime.now() + datetime.timedelta(seconds=1),
        dynamic_outputs=False,
        id=action_id_3,
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=project_id,
        sha=sha,
        status=RepositoryActionStatus.SCANNED.value,
        technical_name=technical_name,
        type="python3",
        version="2.0",
    )
    assert (
        action_repository.get_latest_version_of_action_with_same_sha(
            technical_name, sha, project_id
        )
        == "2.0"
    )


@pytest.mark.parametrize(
    "status, result",
    [
        (RepositoryActionStatus.SCANNING, 0),
        (RepositoryActionStatus.SCAN_ERROR, 0),
        (RepositoryActionStatus.SCANNED, 0),
        (RepositoryActionStatus.BUILD_PENDING, 1),
        (RepositoryActionStatus.BUILDING, 1),
        (RepositoryActionStatus.CANCELLING, 1),
        (RepositoryActionStatus.CANCELLED, 0),
        (RepositoryActionStatus.BUILD_ERROR, 0),
        (RepositoryActionStatus.BUILT, 0),
        (RepositoryActionStatus.STARTING, 1),
    ],
)
def test_list_pending_actions(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action,
    status,
    result,
) -> None:
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    generate_action(
        creation_date=datetime.datetime.now(),
        dynamic_outputs=False,
        id="608f5b39-f383-40f7-8c1b-cac79e7324f0",
        kind=RepositoryActionKind.SOURCE.value,
        metadata_path="tis",
        name="Test-repository_action-name1",
        owner_id="user",
        project=project_id,
        sha="sha",
        status=status.value,
        technical_name="technical_name",
        type="python3",
        version="1.0",
    )

    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )

    pendings = action_repository.list_pending_actions_in_project(project_id, None)
    print(pendings)
