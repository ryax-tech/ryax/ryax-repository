from unittest import mock

import pytest

from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.domain.git_repo.git_repo_repository import IGitRepoRepository
from ryax.repository.domain.repository_action.repository_action_repository import (
    IActionRepository,
)
from ryax.repository.domain.services.encryption_service import IEncryptionService


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()

    source_repository = mock.MagicMock(IGitRepoRepository)
    action_repository = mock.MagicMock(IActionRepository)
    app_container.source_repository.override(source_repository)
    app_container.action_repository.override(action_repository)

    unit_of_work: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    unit_of_work.git_repos = source_repository
    unit_of_work.actions = action_repository
    app_container.unit_of_work.override(unit_of_work)

    encryption_service = mock.MagicMock(IEncryptionService)
    app_container.encryption_service.override(encryption_service)
    return app_container
