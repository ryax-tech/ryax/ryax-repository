from base64 import b64encode
from unittest.mock import AsyncMock, MagicMock

from aio_pika import Channel, Exchange
from google.protobuf import json_format

from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildEvent,
)
from ryax.repository.infrastructure.messaging.utils.publisher import MessagingPublisher


def test_serialize_action_build_event(app_container: ApplicationContainer) -> None:
    engine = app_container.messaging_engine()
    exchange = MagicMock(Exchange)
    exchange.publish = AsyncMock()
    channel = MagicMock(Channel)
    channel.declare_exchange = AsyncMock(return_value="")
    engine.get_channel = AsyncMock(return_value=channel)
    event = ActionBuildEvent(
        action_id="Action ID",
        action_version="Action Version",
        action_kind="Action Kind",
        action_type="Action type",
        action_archive=b"Action archive",
        action_technical_name="modslurmssh",
        action_wrapper_type_list=["a", "b"],
        action_owner_id="Dadou",
        action_project="project",
    )
    event_publisher: MessagingPublisher = app_container.messaging_publisher()
    event_publisher.handle_event(event)
    assert json_format.MessageToDict(
        event_publisher.handle_event(event),
        preserving_proto_field_name=True,
    ) == {
        "module_id": event.action_id,
        "module_version": event.action_version,
        "module_kind": event.action_kind,
        "module_type": event.action_type,
        "module_archive": b64encode(event.action_archive).decode("utf-8"),
        "module_technical_name": event.action_technical_name,
        "module_wrapper_type_list": event.action_wrapper_type_list,
    }
