from typing import Union

import pytest

from ryax.repository.domain.repository_action.repository_action import (
    RepositoryActionResources,
)
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    RepositoryActionResourceRequestInvalidException,
)


@pytest.mark.parametrize(
    "time",
    [-200, -12.0, "200", "-2s", "0s", "zzzzz021d" "12T" "not-valid", "12d12s"],
)
def test_resource_time_errors(time: Union[float, str]) -> None:
    with pytest.raises(RepositoryActionResourceRequestInvalidException):
        RepositoryActionResources.from_metadata(time=time)


@pytest.mark.parametrize(
    "time",
    [200, 200.01, "200s", "2m", "2d", "12h", "0.12s", "12.1234561231564231534563d"],
)
def test_resource_time_valid(time: Union[float, str]) -> None:
    resource = RepositoryActionResources.from_metadata(time=time)
    assert resource.time is not None and resource.time > 0


@pytest.mark.parametrize(
    "memory",
    [12.0, -142, "200", "-2M", "0K", "0.2G", "zzzzz021G" "12T" "not-valid", "12M12K"],
)
def test_resource_memory_errors(memory: Union[int, str]) -> None:
    with pytest.raises(RepositoryActionResourceRequestInvalidException):
        RepositoryActionResources.from_metadata(memory=memory)


@pytest.mark.parametrize(
    "memory",
    [50_000_000, "200000K", "200M", "2G"],
)
def test_resource_memory_valid(memory: Union[int, str]) -> None:
    resource = RepositoryActionResources.from_metadata(memory=memory)
    assert resource.memory is not None and resource.memory > 0
