# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List

from ryax.repository.domain.git_repo.git_repo import GitRepo


class IGitRepoRepository(abc.ABC):
    @abc.abstractmethod
    def list_git_repos(self, current_project: str) -> List[GitRepo]:
        pass

    @abc.abstractmethod
    def get_git_repo(self, git_repo_id: str, current_project: str) -> GitRepo:
        pass

    @abc.abstractmethod
    def refresh_git_repo(self, git_repo: GitRepo) -> None:
        """Method to refresh git_repo from git_repo"""
        pass

    @abc.abstractmethod
    def add_git_repo(self, git_repo: GitRepo) -> None:
        """Create git_repo from informations"""
        pass
