# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import Enum
from typing import Optional

from ryax.repository.domain.repository_action.repository_action_exceptions import (
    RepositoryActionInvalidKindException,
    RepositoryActionInvalidTypeException,
)


class RepositoryActionWrapperType(Enum):
    PYTHON3_GRPC_V1_TRIGGER = "python3-grpcv1-trigger"
    PYTHON3_GRPC_V1 = "python3-grpcv1"
    PYTHON3_FILE_V1 = "python3-filev1"


class RepositoryActionKind(Enum):
    PROCESSOR = "Processor"
    SOURCE = "Source"
    PUBLISHER = "Publisher"
    UNDEFINED = "Undefined"

    @classmethod
    def string_to_enum(cls, value: str) -> "RepositoryActionKind":
        for name in cls:
            if name.value == value:
                return name
        else:
            raise RepositoryActionInvalidKindException

    @classmethod
    def has_value(cls, value: str) -> bool:
        return any(value == var.value for var in cls)


class RepositoryActionType(Enum):
    PYTHON3 = "python3"
    PYTHON3_CUDA = "python3-cuda"
    CSHARP_DOTNET6 = "csharp-dotnet6"
    NODEJS = "nodejs"
    INTERNAL = "internal"
    UNDEFINED = "Undefined"

    @classmethod
    def string_to_enum(cls, value: str) -> "RepositoryActionType":
        for name in cls:
            if name.value == value:
                return name
        else:
            raise RepositoryActionInvalidTypeException


class RepositoryActionStatus(Enum):
    SCANNING = "Scanning"
    SCANNED = "Scanned"
    SCAN_ERROR = "Scan Error"
    BUILD_PENDING = "Build Pending"
    BUILDING = "Building"
    CANCELLING = "Cancelling"
    CANCELLED = "Cancelled"
    BUILD_ERROR = "Build Error"
    BUILT = "Built"
    STARTING = "Starting"


class RepositoryActionIOType(Enum):
    BYTES = "bytes"
    STRING = "string"
    INTEGER = "integer"
    FLOAT = "float"
    BOOLEAN = "boolean"
    FILE = "file"
    DIRECTORY = "directory"
    LONGSTRING = "longstring"
    PASSWORD = "password"
    ENUM = "enum"
    TABLE = "table"

    @classmethod
    def has_value(cls, value: Optional[str]) -> bool:
        return any(value == var.value for var in cls)


class RepositoryActionIOKind(Enum):
    INPUT = "input"
    OUTPUT = "output"


class BuildErrorCode(Enum):
    # This codes comes from repository_action builder
    ImageNameNotDefined = 101
    ActionTypeNotSupported = 102
    InternalRegistryConnection = 103
    ErrorWhileRunningProcess = 104
    RegisteringFunction = 105
    AnotherBuildIsInProgress = 106
