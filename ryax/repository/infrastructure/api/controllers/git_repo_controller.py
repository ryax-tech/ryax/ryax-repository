# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from typing import List

from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema
from dependency_injector.wiring import Provide

from ryax.repository.application.git_repo_service import RepositoryService
from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.git_repo.git_repo_exceptions import (
    ErrorDuringGitRepoFetchException,
    GitRepoHasNoScanResultException,
    GitRepoNotFoundException,
)
from ryax.repository.domain.git_repo.git_repo_values import GitRepoInfo
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionBuildNotAllowedException,
    ActionNotFoundException,
    ActionUnauthorizedDelete,
)
from ryax.repository.domain.repository_action.repository_action_views import ActionView
from ryax.repository.domain.scan.scan_values import ScanGitRepoRequest
from ryax.repository.infrastructure.api.schemas.action_build_schema import (
    ActionBuildSchema,
)
from ryax.repository.infrastructure.api.schemas.add_git_repo_schema import (
    AddGitRepoSchema,
)
from ryax.repository.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.repository.infrastructure.api.schemas.git_repo_scan_request_schema import (
    GitRepoScanRequestSchema,
)
from ryax.repository.infrastructure.api.schemas.git_repo_schema import (
    GitRepoPreviewSchema,
    GitRepoSchema,
)
from ryax.repository.infrastructure.api.schemas.source_scan_schema import (
    RepositoryScanErrorSchema,
)
from ryax.repository.infrastructure.api.schemas.source_schema import (
    GitRepoDetailsSchemaV1,
    GitRepoSchemaV1,
)
from ryax.repository.infrastructure.api.schemas.update_git_repo_schema import (
    UpdateGitRepoSchema,
)

logger = logging.getLogger(__name__)


@docs(
    tags=["Repositories"],
    summary="List all repositories v1",
    description="List all code repositories available",
    responses={
        200: {
            "description": "Repositories fetched successfully",
            "schema": GitRepoSchemaV1(many=True),
        }
    },
)
async def list_repositories(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    current_project = request.get("current_project_id", None)
    git_repos = service.list_with_v1_result_bar(current_project)
    result = GitRepoSchemaV1().dump(git_repos, many=True)
    return json_response(result, status=200)


@docs(
    tags=["Repositories"],
    summary="List all git repos",
    description="List all code repositories available",
    responses={
        200: {
            "description": "Repositories fetched successfully",
            "schema": GitRepoPreviewSchema(many=True),
        }
    },
)
async def list_git_repos(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    current_project = request.get("current_project_id", None)
    git_repos = service.list(current_project)
    result = GitRepoPreviewSchema().dump(git_repos, many=True)
    return json_response(result, status=200)


@docs(
    tags=["Repositories"],
    summary="Build all actions from last scan in this git repo",
    description="Build all actions in this git repo",
    responses={
        200: {
            "description": "Actions built successfully",
            "schema": ActionBuildSchema(many=True),
        },
        404: {"description": "Entity not found", "schema": ErrorSchema},
        400: {"description": "unauthorised action build", "schema": ErrorSchema},
    },
)
async def build_all_actions(
    request: Request,
    git_repo_service: RepositoryService = Provide[
        ApplicationContainer.repository_service
    ],
    repository_action_service: RepositoryActionService = Provide[
        ApplicationContainer.action_service
    ],
) -> Response:
    current_project = request.get("current_project_id", None)
    source_id = request.match_info["source_id"]
    try:
        git_repo = git_repo_service.get(
            source_id,
            current_project,
        )
        actions_built: List[ActionView] = []
        if git_repo.scan_result is not None:
            for action in git_repo.scan_result.actions:
                action_build = await repository_action_service.build_action(
                    action_id=action.id, current_project=current_project
                )
                actions_built.append(action_build)
        else:
            raise GitRepoHasNoScanResultException

        result = ActionBuildSchema().dump(actions_built, many=True)
        return json_response(result, status=200)
    except GitRepoHasNoScanResultException as err:
        result = ErrorSchema().dump({"name": err.message})
        return json_response(result, status=404)
    except GitRepoNotFoundException as err:
        result = ErrorSchema().dump({"name": err.message})
        return json_response(result, status=404)
    except ActionNotFoundException:
        result = ErrorSchema().dump({"name": "Action not found"})
        return json_response(result, status=404)
    except ActionBuildNotAllowedException:
        result = ErrorSchema().dump({"name": "This action can't be built"})
        return json_response(result, status=400)


@docs(
    tags=["Repositories"],
    summary="Add git_repo",
    description="Add new user git_repo",
    responses={
        201: {
            "description": "Repository created successfully",
            "schema": GitRepoSchemaV1,
        }
    },
)
@request_schema(AddGitRepoSchema())
async def add_repository(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    current_project = request.get("current_project_id", None)
    source_infos = GitRepoInfo(**request["data"])
    new_source = service.add(source_infos, current_project)
    result = GitRepoSchemaV1().dump(new_source)
    return json_response(result, status=201)


@docs(
    tags=["Repositories"],
    summary="Get one git_repo (v1)",
    description="Get the requested git_repo by ID",
    parameters=[
        {
            "in": "query",
            "name": "module_sorting",
            "type": "string",
        },
        {
            "in": "query",
            "name": "module_sorting_type",
            "type": "string",
        },
    ],
    responses={
        200: {
            "description": "Repository fetched successfully",
            "schema": GitRepoDetailsSchemaV1,
        },
        404: {"description": "Repository not found", "schema": ErrorSchema},
    },
)
async def get_repository(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    try:
        current_project = request.get("current_project_id", None)
        module_sorting = request.rel_url.query.get("module_sorting", None)
        module_sorting_type = request.rel_url.query.get("module_sorting_type", None)
        source_id = request.match_info["source_id"]
        source = service.get(
            source_id, current_project, module_sorting, module_sorting_type
        )
        result = GitRepoDetailsSchemaV1().dump(source)
        return json_response(result, status=200)
    except GitRepoNotFoundException as err:
        result = ErrorSchema().dump({"name": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Repositories"],
    summary="Get one git_repo",
    description="Get the requested git_repo by ID",
    parameters=[
        {
            "in": "query",
            "name": "action_sorting",
            "type": "string",
        },
        {
            "in": "query",
            "name": "action_sorting_type",
            "type": "string",
        },
    ],
    responses={
        200: {
            "description": "Repository fetched successfully",
            "schema": GitRepoSchema,
        },
        404: {"description": "Repository not found", "schema": ErrorSchema},
    },
)
async def get_git_repo(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    try:
        current_project = request.get("current_project_id", None)
        action_sorting = request.rel_url.query.get("action_sorting", None)
        action_sorting_type = request.rel_url.query.get("action_sorting_type", None)
        git_repo_id = request.match_info["source_id"]
        git_repo = service.get(
            git_repo_id, current_project, action_sorting, action_sorting_type
        )
        result = GitRepoSchema().dump(git_repo)
        return json_response(result, status=200)
    except GitRepoNotFoundException as err:
        result = ErrorSchema().dump({"name": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Repositories"],
    summary="Update git_repo",
    description="Update the data of one git_repo",
    responses={
        200: {
            "description": "Repository updated successfully",
            "schema": GitRepoSchemaV1,
        },
        404: {"description": "Repository not found", "schema": ErrorSchema},
    },
)
@request_schema(UpdateGitRepoSchema())
async def update_repository(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    try:
        current_project = request.get("current_project_id", None)
        source_id = request.match_info["source_id"]
        source_infos = GitRepoInfo(**request["data"])
        updated_source = service.update(source_id, source_infos, current_project)
        result = GitRepoSchemaV1().dump(updated_source)
        return json_response(result, status=200)
    except GitRepoNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Repositories"],
    summary="Delete git_repo",
    description="Delete a git_repo",
    responses={
        200: {"description": "Repository deleted successfully"},
        404: {"description": "Repository not found", "schema": ErrorSchema},
    },
)
async def delete_repository(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    try:
        current_project = request.get("current_project_id", None)
        source_id = request.match_info["source_id"]
        service.archive(source_id, current_project)
        return json_response(None, status=200)
    except GitRepoNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Repositories"],
    summary="Scan git repo",
    description="Launch git_repo analysis (scan method) and return the same data as GET /v2/sources/{source_id}",
    responses={
        200: {
            "description": "Repository scanned successfully",
            "schema": GitRepoSchema,
        },
        404: {"description": "Repository not found", "schema": ErrorSchema},
        400: {
            "description": "Repository access is not valid, unable to fetch",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(GitRepoScanRequestSchema())
async def scan_git_repo(
    request: Request,
    service: RepositoryService = Provide[ApplicationContainer.repository_service],
) -> Response:
    user_id = request["user_id"]
    repository_id = request.match_info["source_id"]
    repository_scan_request = ScanGitRepoRequest(**request["data"])
    current_project = request.get("current_project_id", None)
    try:
        repository_scan = await service.scan(
            user_id, repository_id, current_project, repository_scan_request
        )
        repository_scan_schema = GitRepoSchema()
        result = repository_scan_schema.dump(repository_scan)
        return json_response(result, status=200)
    except ErrorDuringGitRepoFetchException as err:
        result = RepositoryScanErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except GitRepoNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Repository Actions"],
    summary="Cancel all action builds",
    description="Cancel al builds",
    responses={
        200: {"description": "Action deleted successfully"},
        400: {"description": "Action not deletable", "schema": ErrorSchema},
        404: {"description": "Action not found", "schema": ErrorSchema},
    },
)
async def cancel_all_builds(
    request: Request,
    repository_action_service: RepositoryActionService = Provide[
        ApplicationContainer.action_service
    ],
) -> Response:
    current_project = request.get("current_project_id", None)
    source_id = request.match_info["source_id"]
    try:
        logger.info(f"Cancel all builds for project {current_project}")
        # Since one action can only have one build at a time
        await repository_action_service.cancel_all_builds(current_project, source_id)

        return json_response({}, status=200)
    except ActionUnauthorizedDelete:
        result = ErrorSchema().dump(
            {"error": "Action can't be cancelled in this status"}
        )
        return json_response(result)
