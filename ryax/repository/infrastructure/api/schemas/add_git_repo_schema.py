# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class AddGitRepoSchema(Schema):
    name = fields.String(
        metadata={
            "description": "Name of the git_repo",
            "example": "Gitlab Repository",
        },
        required=True,
        error_messages={"required": "Source name required", "code": "400"},
    )

    url = fields.String(
        metadata={
            "description": "Source url",
            "example": "http://blbl.fr",
        },
        required=True,
        error_messages={"required": "Source url required", "code": "400"},
    )
    username = fields.String(
        metadata={
            "description": "Source username",
            "example": "User1",
        },
        required=False,
    )
    password = fields.String(
        metadata={
            "description": "Source password",
            "example": "4_r3411y_g00d_p455w0rd!",
        },
        required=False,
    )
