# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from marshmallow import Schema, fields

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.domain.scan_result.scan_result import ScanResult
from ryax.repository.infrastructure.api.schemas.action_schema import ActionSchema


class GitRepoScanSchema(Schema):
    id = fields.String(
        metadata={
            "description": "Scan result id",
            "example": "6db54a2b96da72a0ffe8e0247e39815117dc2e92",
        }
    )
    sha = fields.String(
        metadata={
            "description": "Git Repo last scan sha",
            "example": "6db54a2b96da72a0ffe8e0247e39815117dc2e92",
        }
    )
    scan_date = fields.DateTime(
        metadata={
            "description": "Git Repo last scan date",
            "example": "2020-10-28T16:05:19.031339",
        }
    )

    built_actions = fields.Method("get_built_actions")

    not_built_actions = fields.Method("get_not_built_actions")

    @staticmethod
    def get_built_actions(scan_result: ScanResult) -> List[ActionSchema]:
        return [
            ActionSchema().dump(action)
            for action in scan_result.actions
            if action.status == RepositoryActionStatus.BUILT
        ]

    @staticmethod
    def get_not_built_actions(scan_result: ScanResult) -> List[ActionSchema]:
        return [
            ActionSchema().dump(action)
            for action in scan_result.actions
            if action.status != RepositoryActionStatus.BUILT
        ]


class GitRepoSchema(Schema):
    id = fields.String(
        metadata={
            "description": "Identifier of the git_repo",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )
    name = fields.String(
        metadata={
            "description": "Name of the git_repo",
            "example": "Gitlab Repository",
        },
    )
    url = fields.String(
        metadata={
            "description": "URL of this git repo",
            "example": "https://my_git_repo.com",
        }
    )
    last_scan = fields.Nested(
        GitRepoScanSchema,
    )


class LastScanSummarySchema(Schema):
    scan_date = fields.DateTime(
        metadata={
            "description": "Git Repo last scan date",
            "example": "2020-10-28T16:05:19.031339",
        }
    )
    actions_error = fields.Integer(
        metadata={
            "description": "Number of actions with errored scan or build",
            "example": "5",
        }
    )

    actions_scanned = fields.Integer(
        metadata={"description": "Number of scanned actions", "example": "5"}
    )

    actions_built = fields.Integer(
        metadata={"description": "Number of built actions", "example": "5"}
    )

    # Includes pending
    actions_building = fields.Integer(
        metadata={"description": "Number of actions currently building", "example": "5"}
    )


class GitRepoPreviewSchema(Schema):
    id = fields.String(
        metadata={
            "description": "Identifier of the git_repo",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )
    name = fields.String(
        metadata={
            "description": "Name of the git_repo",
            "example": "Gitlab Repository",
        },
    )
    url = fields.String(
        metadata={"description": "Git Repo url", "example": "github.com/project"},
    )

    last_scan_summary = fields.Nested(
        LastScanSummarySchema,
        metadata={"description": "Result of the last scan of this git_repo"},
    )
    username = fields.String(
        metadata={"description": "Git Repo username", "example": "ryaxGitlabUser"},
    )
    password_is_set = fields.Boolean(
        metadata={
            "description": "Boolean indicating id the password is non-null",
            "example": True,
        },
    )
