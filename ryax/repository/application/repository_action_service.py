# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from typing import List

from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.domain.events.event_publisher import IEventPublisher
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
    ActionBuildCanceledEvent,
    ActionCancelBuildEvent,
)
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionNotFoundException,
    ActionUnauthorizedDelete,
)

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
    BuildErrorCode,
)

from ryax.repository.domain.repository_action.repository_action_views import ActionView
from ryax.repository.domain.repository_action.repository_action import RepositoryAction

logger = logging.getLogger(__name__)


class RepositoryActionService:
    def __init__(
        self,
        uow: IUnitOfWork,
        event_publisher: IEventPublisher,
    ):
        self.uow = uow
        self.event_publisher = event_publisher

    async def _process_next(self) -> None:
        with self.uow:
            running_tasks = self.uow.actions.get_running_tasks()
            if running_tasks:
                running_task = running_tasks[0]
                logger.debug(
                    f"currently processing: {running_task.id} in state {running_task.status}"
                )
            else:
                logger.debug("No task running. Looking for next task")
                next_task = self.uow.actions.get_next()
                if next_task:
                    logger.debug("Starting task: {next_task.id}")
                    await self._start_build(next_task)
                else:
                    logger.debug("Nothing to do")

    async def _start_build(self, task: RepositoryAction) -> None:
        """Start a build. Publish an event that should be received by the builder to start a build."""
        with self.uow:
            if task.id:
                logger.debug(f"Starting action: {task.id}")
                action = self.uow.actions.get_action(task.id)
                action.build()

                await self.event_publisher.publish(action.events)

                self.uow.commit()

    def list_actions(self, current_project: str) -> List[ActionView]:
        with self.uow:
            return self.uow.actions.list_actions_views(current_project)

    def get_action_in_project(
        self, action_id: str, current_project: str
    ) -> RepositoryAction:
        with self.uow:
            return self.uow.actions.get_action_in_project(action_id, current_project)

    async def build_action(self, action_id: str, current_project: str) -> ActionView:
        with self.uow:
            logger.warning(f"Building action: {action_id}")
            action = self.uow.actions.get_action_in_project(action_id, current_project)
            action.build_pending()

            # await self.event_publisher.publish(action.events)
            # logger.warning(f"Action should be pending: {action}")

            self.uow.commit()

            res = self.uow.actions.get_action_view(action.id, current_project)

            await self._process_next()
            # logger.warning(f"Return for front: {res}")
            return res

    async def cancel_all_builds(
        self, current_project: str, source_id: str | None = None
    ) -> ActionView | None:
        with self.uow:
            pending_builds = self.uow.actions.list_pending_actions_in_project(
                current_project, source_id
            )

            logger.info(f"Currently building action: {pending_builds}")
            for pending_build in pending_builds:
                pending_build.cancel_build()
                if pending_build.status == RepositoryActionStatus.CANCELLING:
                    event = ActionCancelBuildEvent(pending_build.id)
                    await self.event_publisher.publish([event])

                logger.info(
                    f"Cancelled task and message sent, commiting {pending_build}"
                )
                self.uow.commit()
            else:
                logger.warning(f"Cannot cancel task for project {current_project}.")
                return None

    async def cancel_build(
        self, action_id: str, current_project: str
    ) -> ActionView | None:
        with self.uow:
            pending_builds = self.uow.actions.list_pending_actions_in_project(
                current_project, None
            )
            logger.info(f"Currently building action: {pending_builds}")
            if action_id in [pending_build.id for pending_build in pending_builds]:
                pending_build = next(
                    pending_build
                    for pending_build in pending_builds
                    if pending_build.id == action_id
                )

                pending_build.cancel_build()
                if pending_build.status == RepositoryActionStatus.CANCELLING:
                    event = ActionCancelBuildEvent(pending_build.id)
                    await self.event_publisher.publish([event])

                logger.info(f"Canceld task and message sent, commiting {pending_build}")
                self.uow.commit()
                return self.uow.actions.get_action_view(action_id, current_project)
            else:
                logger.warning(f"Cannot cancel task for action {action_id}.")
                return None

    def delete_action(self, action_id: str, current_project: str) -> None:
        """Delete registered repository_action"""
        with self.uow:
            action = self.uow.actions.get_action_in_project(action_id, current_project)
            if not action:
                raise ActionNotFoundException
            if not action.can_be_deleted():
                raise ActionUnauthorizedDelete
            self.uow.actions.delete_action(action)
            self.uow.commit()

    def action_unbuild(self, event: ActionDeletedEvent) -> None:
        """Set repository_action to ready status"""
        with self.uow:
            action = self.uow.actions.get_action(event.action_id)
            action.unbuild()
            self.uow.commit()

    def action_build_start(self, event: ActionBuildStartEvent) -> None:
        """Handle build success event"""
        with self.uow:
            action = self.uow.actions.get_action(event.action_id)
            if action:
                action.build_start()
                self.uow.commit()
            else:
                logger.warning(f"Action {event.action_id} is not found")

    async def action_build_canceled(self, event: ActionBuildCanceledEvent) -> None:
        """Handle build success event"""
        with self.uow:
            action = self.uow.actions.get_action(event.action_id)
            if action:
                action.set_build_cancelled()
                self.uow.commit()
                await self.event_publisher.publish(action.events)
            else:
                logger.warning(f"Action {event.action_id} is not found")

        await self._process_next()

    async def action_build_success(self, event: ActionBuildSuccessEvent) -> None:
        """Handle build success event"""
        with self.uow:
            action = self.uow.actions.get_action(event.action_id)
            if action:
                action.build_success()
                self.uow.commit()
                await self.event_publisher.publish(action.events)
            else:
                logger.warning(f"Action {event.action_id} is not found")

        await self._process_next()

    async def action_build_error(self, event: ActionBuildErrorEvent) -> None:
        """Handle build error event"""
        with self.uow:
            action = self.uow.actions.get_action(event.action_id)
            action.set_build_error(event)
            self.uow.commit()

        await self._process_next()

    async def no_build(self) -> None:
        """Handle builder restart by adding an error the building action"""
        with self.uow:
            action = self.uow.actions.get_building_action()
            if action:
                event = ActionBuildErrorEvent(
                    action_id=action.id,
                    code=BuildErrorCode.ErrorWhileRunningProcess,
                    logs=(
                        "Internal error while building the action: \n"
                        "The Action builder has restarted. "
                        "This might be due to a lack of resources. \n"
                        "Try to increase disk space or memory resource for the Action Builder."
                    ),
                )
                await self.action_build_error(event)
