# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy import and_
from sqlalchemy.orm import mapper, relationship, deferred

from ryax.repository.domain.git_repo.git_repo import GitRepo
from ryax.repository.domain.logo.logo import Logo
from ryax.repository.domain.repository_action.repository_action import (
    RepositoryAction,
    RepositoryActionIO,
    RepositoryActionResources,
)
from ryax.repository.domain.scan_result.scan_result import ScanResult
from ryax.repository.infrastructure.database.metadata import (
    git_repo_table,
    logo_table,
    repository_action_io_table,
    repository_action_resources_table,
    repository_action_table,
    scan_result_action_association,
    scan_result_table,
)


def start_mapping() -> None:
    """Method to start mapping between database and domain models"""

    mapper(
        GitRepo,
        git_repo_table,
        properties={
            "scan_result": relationship(
                ScanResult,
                lazy=False,
                cascade="all, delete, delete-orphan",
                back_populates="git_repo",
                uselist=False,
            ),
        },
    )

    mapper(
        RepositoryAction,
        repository_action_table,
        properties={
            # Avoid loading archive for each query
            "archive": deferred(repository_action_table.columns.archive),
            "inputs": relationship(
                RepositoryActionIO,
                lazy=False,
                primaryjoin=and_(
                    repository_action_table.columns.id
                    == repository_action_io_table.columns.action_id,
                    repository_action_io_table.columns.kind == "input",
                ),
            ),
            "outputs": relationship(
                RepositoryActionIO,
                lazy=False,
                primaryjoin=and_(
                    repository_action_table.columns.id
                    == repository_action_io_table.columns.action_id,
                    repository_action_io_table.columns.kind == "output",
                ),
                overlaps="inputs",
            ),
            "resources": relationship(RepositoryActionResources),
            "logo": relationship(Logo, lazy=False),
        },
    )

    mapper(
        Logo,
        logo_table,
    )

    mapper(
        RepositoryActionResources,
        repository_action_resources_table,
    )

    mapper(
        RepositoryActionIO,
        repository_action_io_table,
    )

    mapper(
        ScanResult,
        scan_result_table,
        properties={
            "git_repo": relationship(GitRepo, back_populates="scan_result"),
            "actions": relationship(
                RepositoryAction,
                secondary=scan_result_action_association,
                lazy=False,
                order_by=RepositoryAction.name,
            ),
        },
    )
