"""add-resources

Revision ID: ff229b322438
Revises: f32139dd76cc
Create Date: 2023-04-27 14:55:46.499178

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ff229b322438'
down_revision = '35acbebed9d9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('repository_action_resources',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('cpu', sa.Float(), nullable=True),
    sa.Column('memory', sa.BigInteger(), nullable=True),
    sa.Column('time', sa.Float(), nullable=True),
    sa.Column('gpu', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('repository_action', sa.Column('repository_action_resources_id', sa.String(), nullable=True))
    op.create_foreign_key(None, 'repository_action', 'repository_action_resources', ['repository_action_resources_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'repository_action', type_='foreignkey')
    op.drop_column('repository_action', 'repository_action_resources_id')
    op.drop_table('repository_action_resources')
    # ### end Alembic commands ###
