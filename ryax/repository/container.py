# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer

from ryax.repository.application.authentication_service import AuthenticationService
from ryax.repository.application.git_repo_service import RepositoryService
from ryax.repository.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.application.util_service import UtilService
from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.domain.git_repo.git_repo_repository import IGitRepoRepository
from ryax.repository.domain.repository_action.repository_action_repository import (
    IActionRepository,
)
from ryax.repository.infrastructure.authorization.authorization_api_service import (
    AuthorizationApiService,
)
from ryax.repository.infrastructure.database.engine import DatabaseEngine
from ryax.repository.infrastructure.database.repositories.action_repository import (
    DatabaseActionRepository,
)
from ryax.repository.infrastructure.database.repositories.git_repo_repository import (
    DatabaseGitRepoRepository,
)
from ryax.repository.infrastructure.database.unit_of_work import DatabaseUnitOfWork
from ryax.repository.infrastructure.encryption.encryption_service import (
    EncryptionService,
)
from ryax.repository.infrastructure.git.git_service import GitService
from ryax.repository.infrastructure.jwt.jwt_service import JwtService
from ryax.repository.infrastructure.messaging.utils.consumer import MessagingConsumer
from ryax.repository.infrastructure.messaging.utils.engine import MessagingEngine
from ryax.repository.infrastructure.messaging.utils.publisher import MessagingPublisher


class ApplicationContainer(DeclarativeContainer):
    """Application container for dependency injection"""

    # Define configuration providers
    config = providers.Configuration()

    # Database utils && repositories
    database_engine = providers.Singleton(
        DatabaseEngine, connection_url=config.datastore_url
    )

    # Database repositories factories
    git_repo_repository: providers.Factory[IGitRepoRepository] = providers.Factory(
        DatabaseGitRepoRepository
    )
    action_repository: providers.Factory[IActionRepository] = providers.Factory(
        DatabaseActionRepository
    )

    # Database unit of work
    unit_of_work: providers.Factory[IUnitOfWork] = providers.Factory(
        DatabaseUnitOfWork,
        engine=database_engine,
        git_repo_factory=git_repo_repository.provider,
        action_repository_factory=action_repository.provider,
    )

    # Broker services
    messaging_engine = providers.Singleton(
        MessagingEngine, connection_url=config.broker_url
    )
    messaging_consumer = providers.Singleton(MessagingConsumer, engine=messaging_engine)
    messaging_publisher = providers.Singleton(
        MessagingPublisher, engine=messaging_engine
    )

    # Git service
    git_service = providers.Singleton(
        GitService, tmp_directory_path=config.tmp_directory_path
    )

    # Jwt service
    jwt_service = providers.Singleton(JwtService, secret_key=config.jwt_secret_key)

    # Application services
    util_service = providers.Singleton(UtilService)

    authentication_service = providers.Singleton(
        AuthenticationService, security_service=jwt_service
    )

    encryption_service = providers.Singleton(
        EncryptionService, encryption_key=config.password_encryption_key
    )

    repository_service: providers.Factory[RepositoryService] = providers.Factory(
        RepositoryService,
        uow=unit_of_work,
        repository_scan_service=git_service,
        encryption_service=encryption_service,
    )

    authorization_api_service = providers.Singleton(
        AuthorizationApiService, config.authorization_api_base_url
    )

    project_authorization_service = providers.Singleton(
        ProjectAuthorizationService, authorization_api_service=authorization_api_service
    )

    action_service: providers.Factory[RepositoryActionService] = providers.Factory(
        RepositoryActionService,
        uow=unit_of_work,
        event_publisher=messaging_publisher,
    )
