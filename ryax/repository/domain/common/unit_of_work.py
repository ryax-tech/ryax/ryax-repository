# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from types import TracebackType
from typing import Type

from ryax.repository.domain.git_repo.git_repo_repository import IGitRepoRepository
from ryax.repository.domain.repository_action.repository_action_repository import (
    IActionRepository,
)


class IUnitOfWork(abc.ABC):
    git_repos: IGitRepoRepository
    actions: IActionRepository

    @abc.abstractmethod
    def __enter__(self) -> None:
        pass

    @abc.abstractmethod
    def __exit__(
        self,
        exc_type: Type[Exception] | None = None,
        exc_val: Exception | None = None,
        exc_tb: TracebackType | None = None,
    ) -> None:
        pass

    @abc.abstractmethod
    def commit(self) -> None:
        pass

    @abc.abstractmethod
    def rollback(self) -> None:
        pass
