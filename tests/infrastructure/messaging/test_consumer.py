from unittest.mock import AsyncMock, MagicMock, call

from aio_pika import Channel, Exchange, ExchangeType, IncomingMessage, Queue

from ryax.repository.container import ApplicationContainer
from ryax.repository.infrastructure.messaging.utils.consumer import MessagingConsumer


def test_register_handler(app_container: ApplicationContainer):
    message_type = "message_type"
    message_handler = AsyncMock()
    messaging_consumer: MessagingConsumer = app_container.messaging_consumer()
    messaging_consumer.register_handler(message_type, message_handler)
    assert messaging_consumer.handlers[message_type] == message_handler


async def test_handle_registered_message_type_with_success(
    app_container: ApplicationContainer,
):
    """Should process message with handler"""
    message = MagicMock(IncomingMessage)
    message.type = "message_type"
    message_handler = AsyncMock(return_value=None)
    messaging_consumer: MessagingConsumer = app_container.messaging_consumer()
    messaging_consumer.message_definitions = {
        message.type: MagicMock(return_value="result")
    }
    messaging_consumer.handlers = {message.type: message_handler}
    await messaging_consumer.handle_message(message)
    message_handler.assert_called_with(message)


async def test_start(app_container: ApplicationContainer):
    """Should received message on studio event queue when publishing on ..."""
    domain_event_exchange = MagicMock(Exchange)
    repository_event_queue = MagicMock(Queue)
    channel = MagicMock(Channel)
    channel.declare_exchange = AsyncMock(return_value=domain_event_exchange)
    channel.declare_queue = AsyncMock(return_value=repository_event_queue)
    messaging_engine = app_container.messaging_engine()
    messaging_engine.get_channel = AsyncMock(return_value=channel)
    messaging_consumer: MessagingConsumer = app_container.messaging_consumer()
    messaging_consumer.handlers = {"event_type": AsyncMock()}
    await messaging_consumer.start()
    channel.declare_exchange.assert_called_with(
        "domain_events", ExchangeType.TOPIC, durable=True
    )
    channel.declare_queue.assert_called_with("repository_events", durable=True)
    repository_event_queue.bind.assert_has_calls(
        [
            call(domain_event_exchange, "ActionBuilder.*"),
            call(domain_event_exchange, "Studio.ActionDeleted"),
        ]
    )
    repository_event_queue.consume.assert_called_with(messaging_consumer.handle_message)
