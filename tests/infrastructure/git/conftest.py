import tempfile

import pytest

from ryax.repository.container import ApplicationContainer


@pytest.fixture()
def app_container():
    container = ApplicationContainer()
    with tempfile.TemporaryDirectory() as tmp_dir:
        container.config.set("tmp_directory_path", tmp_dir)
        yield container
