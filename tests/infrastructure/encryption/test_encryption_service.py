from unittest import mock

from ryax.repository.infrastructure.encryption.encryption_service import (
    EncryptionService,
)


class TestEncryptionService:
    def test_encrypt(self):
        mock_encryption_key = b"bK4ksZHU1jFPQL6MIpJ_d7TxChuqjg5J0VMRc9gQsCQ="
        mock_plaintext = "plaintext"
        encoded_plaintext = mock_plaintext.encode()
        encryption_service = EncryptionService(mock_encryption_key)
        encryption_service.symmetric_encryption.encrypt = mock.MagicMock(
            side_effect=lambda x: x
        )
        ciphertext = encryption_service.encrypt(mock_plaintext)
        assert ciphertext == encoded_plaintext
        encryption_service.symmetric_encryption.encrypt.assert_called_once_with(
            encoded_plaintext
        )

    def test_decrypt(self):
        mock_encryption_key = b"bK4ksZHU1jFPQL6MIpJ_d7TxChuqjg5J0VMRc9gQsCQ="
        mock_ciphertext = b"plaintext"
        mock_plaintext = mock_ciphertext.decode()

        encryption_service = EncryptionService(mock_encryption_key)
        encryption_service.symmetric_encryption.decrypt = mock.MagicMock(
            side_effect=lambda x: x
        )
        plaintext = encryption_service.decrypt(mock_ciphertext)
        assert plaintext == mock_plaintext
        encryption_service.symmetric_encryption.decrypt.assert_called_once_with(
            mock_plaintext.encode()
        )
