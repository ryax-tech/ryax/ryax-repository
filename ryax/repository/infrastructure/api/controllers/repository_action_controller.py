# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, response_schema
from dependency_injector.wiring import Provide

from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionBuildNotAllowedException,
    ActionNotFoundException,
    ActionUnauthorizedDelete,
)
from ryax.repository.infrastructure.api.schemas.action_build_schema import (
    ActionBuildSchema,
)
from ryax.repository.infrastructure.api.schemas.error_schema import ErrorSchema

logger = logging.getLogger(__name__)


@docs(
    tags=["Repository Actions"],
    summary="List all actions",
    description="List all code actions available",
)
@response_schema(
    ActionBuildSchema(many=True), code=200, description="Action fetched successfully"
)
async def list_actions(
    request: Request,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    current_project = request.get("current_project_id", None)
    actions = service.list_actions(current_project)
    result = ActionBuildSchema().dump(actions, many=True)
    return json_response(result, status=200)


@docs(
    tags=["Repository Actions"],
    summary="Build repository action",
    description="Launch repository action building",
)
@response_schema(ActionBuildSchema, 200, description="Module build successfully")
@response_schema(
    ErrorSchema, code=400, description="Unauthorized repository_action build"
)
@response_schema(ErrorSchema, code=401, description="Unauthorized")
@response_schema(ErrorSchema, code=404, description="Module not found")
async def build_action(
    request: Request,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    action_id = request.match_info["module_id"]
    try:
        current_project = request.get("current_project_id", None)
        build = await service.build_action(action_id, current_project)
        logger.info(f"Received build action: {action_id}")
        result = ActionBuildSchema().dump(build)
        logger.info(f"Result: {result}")
        return json_response(result, status=200)
    except ActionNotFoundException:
        result = ErrorSchema().dump({"name": f"Module {action_id} not found"})
        return json_response(result, status=404)
    except ActionBuildNotAllowedException:
        result = ErrorSchema().dump({"name": "This repository_action can't be built"})
        return json_response(result, status=400)


@docs(
    tags=["Repository Actions"],
    summary="Delete repository action",
    description="Delete repository action",
    responses={
        200: {"description": "Action deleted successfully"},
        400: {"description": "Action not deletable", "schema": ErrorSchema},
        404: {"description": "Action not found", "schema": ErrorSchema},
    },
)
async def delete_action(
    request: Request,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    try:
        current_project = request.get("current_project_id", None)
        action_id: str = request.match_info["module_id"]
        service.delete_action(action_id, current_project)
        return json_response(None, status=200)
    except ActionNotFoundException:
        result = ErrorSchema().dump({"error": f"Action {action_id} not found"})
        return json_response(result, status=404)
    except ActionUnauthorizedDelete:
        result = ErrorSchema().dump({"error": "Action can't be deleted in this status"})
        return json_response(result, status=400)


@docs(
    tags=["Repository Actions"],
    summary="Cancel an action build",
    description="Cancel a build",
    responses={
        200: {"description": "Build cancelled successfully"},
        404: {"description": "Build not found", "schema": ErrorSchema},
    },
)
async def cancel_build(
    request: Request,
    repository_action_service: RepositoryActionService = Provide[
        ApplicationContainer.action_service
    ],
) -> Response:
    current_project = request.get("current_project_id", None)
    action_id = request.match_info["action_id"]
    action = repository_action_service.get_action_in_project(action_id, current_project)
    action_view = None
    try:
        if action:
            logger.info(f"Cancel build for action: {action_id}")
            # Since one action can only have one build at a time
            action_view = await repository_action_service.cancel_build(
                action.id, current_project
            )
            result = ActionBuildSchema().dump(action_view)
        else:
            raise ActionNotFoundException

        return json_response(result, status=200)
    except ActionNotFoundException:
        result = ErrorSchema().dump({"error": f"Action {action_id} not found"})
        return json_response(result, status=404)
