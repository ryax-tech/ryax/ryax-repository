import copy
from datetime import datetime

import pytest

from ryax.repository.domain.logo.logo import Logo
from ryax.repository.domain.repository_action.repository_action import (
    RepositoryAction,
    RepositoryActionIO,
)
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildEvent,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionIOKind,
    RepositoryActionIOType,
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
    RepositoryActionWrapperType,
)


@pytest.mark.skip()
def test_build_when_ready() -> None:
    """Build should update status and add build request event"""
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        id="Action id",
        technical_name="nameslurmssh",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.PROCESSOR.value,
        status=RepositoryActionStatus.READY,
        owner_id="repository_action owner",
        archive=bytes("repository_action archive", "utf-8"),
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action.build()
    assert action.status == RepositoryActionStatus.BUILD_PENDING
    assert action.events == [
        ActionBuildEvent(
            action_id=action.id,
            action_type=action.type.value,
            action_version=action.version,
            action_archive=action.archive,
            action_kind=action.kind.value,
            action_technical_name=action.technical_name,
            action_wrapper_type_list=[
                RepositoryActionWrapperType.PYTHON3_GRPC_V1.value,
                RepositoryActionWrapperType.PYTHON3_FILE_V1.value,
            ],
        )
    ]


@pytest.mark.skip()
def test_build_success() -> None:
    """Build repository_action should update repository_action status and generate event"""
    action = RepositoryAction(
        project="project",
        metadata_path="path",
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        status=RepositoryActionStatus.BUILDING,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
        logo=Logo(
            id="action_id",
            name="logo",
            extension="png",
            path="/foo",
            content=b"logo_content",
        ),
        categories=["cat_1", "cat_2"],
    )
    action.build_success()
    assert action.status == RepositoryActionStatus.SUCCESS


@pytest.mark.skip()
def test_build_success_when_no_logo() -> None:
    action = RepositoryAction(
        project="project",
        metadata_path="path",
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        status=RepositoryActionStatus.BUILDING,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action.build_success()
    assert action.status == RepositoryActionStatus.SUCCESS


def test_build_success_when_not_building() -> None:
    """Build repository_action should do nothing"""
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=RepositoryActionStatus.BUILD_PENDING,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_copy = copy.deepcopy(action)
    action.build_success()
    assert action == action_copy


@pytest.mark.skip()
def test_unbuild_action() -> None:
    """Unbuild repository_action should change it's status to Ready"""
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=RepositoryActionStatus.SUCCESS,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action.unbuild()
    assert action.status == RepositoryActionStatus.READY


@pytest.mark.skip()
@pytest.mark.parametrize(
    "action_status, expected",
    [
        (RepositoryActionStatus.SCANNED, False),
        (RepositoryActionStatus.BUILD_PENDING, True),
        (RepositoryActionStatus.BUILDING, True),
        (RepositoryActionStatus.BUILT, True),
        (RepositoryActionStatus.SCAN_ERROR, False),
    ],
)
def test_is_in_build_process(action_status, expected) -> None:
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=action_status,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    assert action.is_in_building_process() == expected


@pytest.mark.skip()
@pytest.mark.parametrize(
    "action_status, expected",
    [
        (RepositoryActionStatus.SCANNED, True),
        (RepositoryActionStatus.BUILD_PENDING, True),
        (RepositoryActionStatus.BUILDING, False),
        (RepositoryActionStatus.BUILT, False),
        (RepositoryActionStatus.SCAN_ERROR, True),
    ],
)
def test_is_delete_enabled(action_status, expected) -> None:
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=action_status,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    assert action.can_be_deleted() == expected


@pytest.mark.skip()
def test_build_error() -> None:
    event = ActionBuildErrorEvent(logs="logs text", code=1, action_id="id")
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=RepositoryActionStatus.BUILDING,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action.build_error(event)
    assert action.status == RepositoryActionStatus.ERROR
    assert action.build_error_logs == event.logs
    assert action.build_error_code == 101


@pytest.mark.parametrize(
    "action_status, expected",
    [
        (RepositoryActionStatus.SCANNED, False),
        (RepositoryActionStatus.BUILD_PENDING, True),
        (RepositoryActionStatus.BUILDING, True),
        (RepositoryActionStatus.BUILT, False),
        (RepositoryActionStatus.SCAN_ERROR, False),
    ],
)
def test_is_building(action_status, expected) -> None:
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=action_status,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    assert action.is_building() == expected


@pytest.mark.parametrize(
    "action_status, expected",
    [
        (RepositoryActionStatus.SCANNED, False),
        (RepositoryActionStatus.BUILD_PENDING, False),
        (RepositoryActionStatus.BUILDING, False),
        (RepositoryActionStatus.BUILT, True),
        (RepositoryActionStatus.SCAN_ERROR, False),
    ],
)
def test_is_success(action_status, expected) -> None:
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=action_status,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    assert action.is_built() == expected


@pytest.mark.parametrize(
    "action_status, expected",
    [
        (RepositoryActionStatus.SCANNED, True),
        (RepositoryActionStatus.BUILD_PENDING, False),
        (RepositoryActionStatus.BUILDING, False),
        (RepositoryActionStatus.BUILT, False),
        (RepositoryActionStatus.SCAN_ERROR, False),
    ],
)
def test_is_ready(action_status, expected) -> None:
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=action_status,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    assert action.is_scanned() == expected


@pytest.mark.parametrize(
    "action_status, expected",
    [
        (RepositoryActionStatus.SCANNED, False),
        (RepositoryActionStatus.BUILD_PENDING, False),
        (RepositoryActionStatus.BUILDING, False),
        (RepositoryActionStatus.BUILT, False),
        (RepositoryActionStatus.SCAN_ERROR, True),
    ],
)
def test_is_error(action_status, expected) -> None:
    action = RepositoryAction(
        metadata_path="path",
        project="project",
        status=action_status,
        technical_name="tname",
        creation_date=datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        id="Action id",
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    assert action.is_error() == expected
