# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import fields

from ryax.repository.infrastructure.api.schemas.action_schema import ActionSchema
from ryax.repository.infrastructure.api.schemas.source_schema import (
    GitRepoPreviewSchemaV1,
)


class ActionBuildSchema(ActionSchema):
    source = fields.Nested(
        GitRepoPreviewSchemaV1,
        metadata={"description": "Source of the repository_action"},
    )
