# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields
from marshmallow_enum import EnumField

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
)


class BuildErrorSchema(Schema):
    logs = fields.String(
        metadata={
            "description": "Error logs",
            "example": "Build fail, library not found",
        }
    )

    code = fields.Integer(
        metadata={
            "description": "Error code",
            "example": 101,
        }
    )


class ActionSchema(Schema):
    id = fields.String(
        metadata={
            "description": "ID of the repository_action",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )

    name = fields.String(
        metadata={
            "description": "Name of the repository_action",
            "example": "MQTT Gateway",
        },
    )

    version = fields.String(
        metadata={"description": "Version of the repository_action", "example": "1.0"},
    )

    technical_name = fields.String(
        metadata={
            "description": "Technical name of the repository_action",
            "example": "mqttgw",
        },
    )

    lockfile = fields.String(
        metadata={
            "description": "Lockfile of the action if any. This a json file.",
            "example": "{}",
        },
    )

    description = fields.String(
        metadata={
            "description": "Description of the repository_action",
            "example": "This repository_action creates a MQTT Gateway",
        },
    )

    kind = EnumField(RepositoryActionKind, by_value=True)

    type = EnumField(RepositoryActionType, by_value=True)

    dynamic_outputs = fields.Boolean(
        metadata={
            "description": "Whether repository_action has dynamic outputs",
            "example": True,
        },
    )

    status = fields.Str(
        metadata={
            "description": "Build status of the repository_action",
            "example": "Pending",
        },
        enum=list(item.value for item in RepositoryActionStatus),
        attribute="status.value",
    )

    creation_date = fields.DateTime(
        metadata={
            "description": "Creation date of the repository_action",
            "example": "2020-10-28T16:05:19.031339",
        },
    )

    build_error = fields.String(
        metadata={
            "description": "Buold error code and logs",
            "example": "Build error code: 201\nError logs",
        }
    )

    metadata_path = fields.String(
        metadata={
            "description": "Path of ryax metadata file found relative to the root git_repo",
            "example": "/module1",
        },
    )

    scan_errors = fields.List(
        fields.String(
            metadata={
                "description": "In cas of problem while analysing the repository_action",
                "example": ["Error while parsing repository_action files"],
            },
        )
    )

    sha = fields.String(
        metadata={
            "description": "The commit sha of the repository_action",
            "example": "6db54a2b96da72a0ffe8e0247e39815117dc2e92",
        },
    )
