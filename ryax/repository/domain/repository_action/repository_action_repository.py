# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Optional

from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_views import ActionView


class IActionRepository(abc.ABC):
    @abc.abstractmethod
    def list_actions_views(self, current_project: str) -> List[ActionView]:
        pass

    @abc.abstractmethod
    def get_action_view(self, action_id: str, current_project: str) -> ActionView:
        pass

    @abc.abstractmethod
    def list_actions_by_ids(self, action_ids: List[str]) -> List[RepositoryAction]:
        pass

    @abc.abstractmethod
    def get_action_in_project(
        self, action_id: str, current_project: str
    ) -> RepositoryAction:
        pass

    @abc.abstractmethod
    def get_action(self, action_id: str) -> RepositoryAction:
        """This method is used in event to get a repository_action without project filter"""
        pass

    @abc.abstractmethod
    def get_building_action(self) -> RepositoryAction:
        """Get the currently building action"""
        pass

    @abc.abstractmethod
    def refresh_action(self, action: RepositoryAction) -> None:
        pass

    @abc.abstractmethod
    def add_action(self, action: RepositoryAction) -> None:
        pass

    @abc.abstractmethod
    def delete_action(self, action: RepositoryAction) -> None:
        pass

    @abc.abstractmethod
    def get_matching_actions(
        self, technical_name: str, version: str, current_project: str
    ) -> List[RepositoryAction]:
        pass

    @abc.abstractmethod
    def get_action_versions_by_technical_name(
        self, technical_name: str, current_project: str
    ) -> List[str]:
        pass

    @abc.abstractmethod
    def get_latest_version_of_action_with_same_sha(
        self, technical_name: str, sha: str, current_project: str
    ) -> Optional[str]:
        pass

    @abc.abstractmethod
    def list_pending_actions_in_project(
        self, current_project: str, source_id: Optional[str]
    ) -> List[RepositoryAction]:
        pass

    @abc.abstractmethod
    def get_running_tasks(self) -> List[RepositoryAction]:
        pass

    @abc.abstractmethod
    def get_next(self) -> RepositoryAction:
        pass
