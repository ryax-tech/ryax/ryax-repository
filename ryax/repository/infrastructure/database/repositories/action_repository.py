# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List, Optional

from sqlalchemy import and_, desc
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session

from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_exceptions import (
    ActionNotFoundException,
)

from ryax.repository.domain.repository_action.repository_action_repository import (
    IActionRepository,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.domain.repository_action.repository_action_views import ActionView


class DatabaseActionRepository(IActionRepository):
    def __init__(self, session: Session):
        self.session = session

    def list_actions_views(self, current_project: str) -> List[ActionView]:
        return (
            self.session.query(RepositoryAction)
            .filter(RepositoryAction.project == current_project)
            .all()
        )

    def list_actions_by_ids(self, action_ids: List[str]) -> List[RepositoryAction]:
        """
        Returns all the actions with ids from the input list action_ids. Type ignore is used
        since mypy doesn't recognise the sqlalchemy _in() clause
        """
        return (
            self.session.query(RepositoryAction)
            .filter(RepositoryAction.id.in_(action_ids))  # type: ignore
            .all()
        )

    def get_action_view(self, action_id: str, current_project: str) -> ActionView:
        try:
            action_view = (
                self.session.query(RepositoryAction)
                .filter(
                    and_(
                        RepositoryAction.id == action_id,
                        RepositoryAction.project == current_project,
                    )
                )
                .one()
            )
        except NoResultFound:
            raise ActionNotFoundException()
        if not action_view:
            raise ActionNotFoundException()
        return action_view

    def get_action_in_project(
        self, action_id: str, current_project: str
    ) -> RepositoryAction:
        try:
            action = (
                self.session.query(RepositoryAction)
                .filter(
                    and_(
                        RepositoryAction.id == action_id,
                        RepositoryAction.project == current_project,
                    )
                )
                .one()
            )
        except NoResultFound:
            raise ActionNotFoundException()
        if not action:
            raise ActionNotFoundException()
        return action

    def get_action(self, action_id: str) -> RepositoryAction:
        try:
            action = (
                self.session.query(RepositoryAction)
                .filter(RepositoryAction.id == action_id)
                .one()
            )
        except NoResultFound:
            raise ActionNotFoundException()
        if not action:
            raise ActionNotFoundException()
        return action

    def get_building_action(self) -> RepositoryAction:
        return (
            self.session.query(RepositoryAction)
            .filter(RepositoryAction.status == RepositoryActionStatus.BUILDING)
            .scalar()
        )

    def refresh_action(self, action: RepositoryAction) -> None:
        self.session.refresh(action)

    def get_matching_actions(
        self, technical_name: str, version: str, current_project: str
    ) -> List[RepositoryAction]:
        return (
            self.session.query(RepositoryAction)
            .filter(
                and_(
                    RepositoryAction.technical_name == technical_name,
                    RepositoryAction.version == version,
                    RepositoryAction.project == current_project,
                )
            )
            .all()
        )

    def get_action_versions_by_technical_name(
        self, technical_name: str, current_project: str
    ) -> List[str]:
        versions_tuples = (
            self.session.query(RepositoryAction.version)
            .filter(
                RepositoryAction.technical_name == technical_name,
                RepositoryAction.project == current_project,
            )
            .all()
        )
        return [v[0] for v in versions_tuples]

    def add_action(self, action: RepositoryAction) -> None:
        self.session.add(action)

    def delete_action(self, action: RepositoryAction) -> None:
        self.session.delete(action)

    def get_latest_version_of_action_with_same_sha(
        self, technical_name: str, sha: str, current_project: str
    ) -> Optional[str]:
        latest_version = (
            self.session.query(RepositoryAction.version)
            .filter(
                RepositoryAction.technical_name == technical_name,
                RepositoryAction.sha == sha,
                RepositoryAction.project == current_project,
            )
            .order_by(desc("creation_date"))
            .limit(1)
        ).first()
        return latest_version["version"] if latest_version else None

    def list_pending_actions_in_project(
        self, current_project: str, source_id: Optional[str]
    ) -> List[RepositoryAction]:
        stmt = select(RepositoryAction).where(
            RepositoryAction.status.in_(  # type: ignore
                [
                    RepositoryActionStatus.STARTING,
                    RepositoryActionStatus.BUILDING,
                    RepositoryActionStatus.CANCELLING,
                    RepositoryActionStatus.BUILD_PENDING,
                ]
            ),
            RepositoryAction.project == current_project,
        )

        if source_id:
            stmt = stmt.where(RepositoryAction.source_id == source_id)

        stmt = stmt.order_by(RepositoryAction.submit_date.desc())  # type: ignore

        # We must call unique here because of relationships
        # https://docs.sqlalchemy.org/en/20/orm/queryguide/relationships.html#joined-eager-loading
        pending = self.session.execute(stmt).unique().scalars().all()

        return pending

    def get_running_tasks(self) -> List[RepositoryAction]:
        """Get running tasks, not that it should only have one"""
        pending = (
            self.session.query(RepositoryAction)
            .filter(
                RepositoryAction.status.in_(  # type: ignore
                    [  # type: ignore
                        RepositoryActionStatus.BUILDING,
                        RepositoryActionStatus.CANCELLING,
                        RepositoryActionStatus.STARTING,
                    ]
                )
            )
            .order_by(RepositoryAction.submit_date.desc())  # type: ignore
            .all()
        )
        return pending

    def get_next(self) -> RepositoryAction:
        """Get running tasks, not that it should only have one"""
        return (
            self.session.query(RepositoryAction)  # type: ignore
            .order_by(RepositoryAction.submit_date)
            .filter(RepositoryAction.status == RepositoryActionStatus.BUILD_PENDING)
            .first()
        )
