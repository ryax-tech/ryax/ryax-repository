import datetime

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.domain.repository_action.repository_action_views import (
    ActionView,
    SourceView,
)


def test_error() -> None:
    action_view = ActionView(
        build_error_logs="test logs",
        build_error_code=102,
        id="id",
        name="name",
        technical_name="technical_name",
        version="version",
        description="description",
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        project="project",
        source=SourceView(id="id2", name="name", project="project"),
    )
    assert action_view.build_error == {
        "logs": action_view.build_error_logs,
        "code": action_view.build_error_code,
    }


def test_error_when_null() -> None:
    action_view = ActionView(
        id="id",
        name="name",
        technical_name="technical_name",
        version="version",
        description="description",
        status=RepositoryActionStatus.READY,
        creation_date=datetime.datetime.now(),
        project="project",
        source=SourceView(id="id2", name="name", project="project"),
    )
    assert action_view.build_error is None
