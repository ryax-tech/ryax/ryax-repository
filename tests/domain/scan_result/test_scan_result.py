import datetime
from unittest import mock

from ryax.repository.domain.repository_action.repository_action import (
    RepositoryAction,
    RepositoryActionIO,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionIOKind,
    RepositoryActionIOType,
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
)
from ryax.repository.domain.scan_result.scan_result import ScanResult, ScanResultStatus


def test_set_project() -> None:
    project_id = "project_id"
    scan_result = ScanResult(
        id="id", scan_date=datetime.datetime.now(), project=project_id, sha="sha"
    )
    scan_result.set_project(project_id)
    assert scan_result.project == project_id


def test_add_action() -> None:
    project_id = "project_id"
    action = RepositoryAction(
        id="action_id",
        project=project_id,
        status=RepositoryActionStatus.READY,
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        id="id", scan_date=datetime.datetime.now(), project=project_id, sha="sha"
    )
    scan_result.add_action(action)
    assert scan_result.actions == [action]


def test_get_actions_success() -> None:
    action_1 = RepositoryAction(
        id="action_id_1",
        status=RepositoryActionStatus.READY,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_2 = RepositoryAction(
        id="action_id_2",
        status=RepositoryActionStatus.SUCCESS,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_3 = RepositoryAction(
        id="action_id_3",
        status=RepositoryActionStatus.BUILDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_4 = RepositoryAction(
        id="action_id_4",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_5 = RepositoryAction(
        id="action_id_5",
        status=RepositoryActionStatus.ERROR,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[action_1, action_2, action_3, action_4, action_5],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    assert scan_result.get_built_actions() == [action_2]


def test_get_action_building() -> None:
    action_1 = RepositoryAction(
        id="action_id_1",
        status=RepositoryActionStatus.READY,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_2 = RepositoryAction(
        id="action_id_2",
        status=RepositoryActionStatus.SUCCESS,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_3 = RepositoryAction(
        id="action_id_3",
        status=RepositoryActionStatus.BUILDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_4 = RepositoryAction(
        id="action_id_4",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_5 = RepositoryAction(
        id="action_id_5",
        status=RepositoryActionStatus.ERROR,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[action_1, action_2, action_3, action_4, action_5],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    assert scan_result.get_building_actions() == [action_3, action_4]


def test_get_action_ready() -> None:
    action_1 = RepositoryAction(
        id="action_id_1",
        status=RepositoryActionStatus.READY,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_2 = RepositoryAction(
        id="action_id_2",
        status=RepositoryActionStatus.SUCCESS,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_3 = RepositoryAction(
        id="action_id_3",
        status=RepositoryActionStatus.BUILDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_4 = RepositoryAction(
        id="action_id_4",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_5 = RepositoryAction(
        id="action_id_5",
        status=RepositoryActionStatus.ERROR,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[action_1, action_2, action_3, action_4, action_5],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    assert scan_result.get_scanned_actions() == [action_1]


def test_action_error() -> None:
    action_1 = RepositoryAction(
        id="action_id_1",
        status=RepositoryActionStatus.READY,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_2 = RepositoryAction(
        id="action_id_2",
        status=RepositoryActionStatus.SUCCESS,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_3 = RepositoryAction(
        id="action_id_3",
        status=RepositoryActionStatus.BUILDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_4 = RepositoryAction(
        id="action_id_4",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_5 = RepositoryAction(
        id="action_id_5",
        status=RepositoryActionStatus.ERROR,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        name="repository_action name",
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[action_1, action_2, action_3, action_4, action_5],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    assert scan_result.get_action_error() == [action_5]


def test_refresh_bar() -> None:
    scan_result = ScanResult(
        id="id", scan_date=datetime.datetime.now(), project="project", sha="sha"
    )
    scan_result.get_built_actions = mock.Mock(
        return_value=[
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
        ]
    )
    scan_result.get_building_actions = mock.Mock(
        return_value=[
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            )
        ]
    )
    scan_result.get_scanned_actions = mock.Mock(
        return_value=[
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
        ]
    )
    scan_result.get_action_error = mock.Mock(
        return_value=[
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
            RepositoryAction(
                id="action_1",
                name="eaction",
                status=RepositoryActionStatus.READY,
                project="project",
                technical_name="tname",
                creation_date=datetime.datetime.now(),
                source_id="id",
                sha="123",
                archive=bytes(123),
                description="repository_action description",
                version="repository_action version",
                type=RepositoryActionType.PYTHON3.value,
                kind=RepositoryActionKind.SOURCE.value,
                owner_id="repository_action owner",
                dynamic_outputs=True,
                inputs=[
                    RepositoryActionIO(
                        id="repository_action input id",
                        display_name="Input display name",
                        technical_name="Input technical name",
                        type=RepositoryActionIOType.ENUM,
                        kind=RepositoryActionIOKind.INPUT,
                        enum_values=["value1", "value2", "value3"],
                        help="help",
                    )
                ],
                outputs=[
                    RepositoryActionIO(
                        id="repository_action output id",
                        display_name="Output display name",
                        technical_name="Output technical name",
                        type=RepositoryActionIOType.INTEGER,
                        kind=RepositoryActionIOKind.OUTPUT,
                        enum_values=[],
                        help="help",
                    )
                ],
            ),
        ]
    )
    scan_result.refresh_status()
    assert scan_result.status == ScanResultStatus(2, 1, 3, 4)


def test_sort_actions() -> None:
    action_1 = RepositoryAction(
        id="action_1",
        name="eaction",
        status=RepositoryActionStatus.READY,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_2 = RepositoryAction(
        id="action_2",
        name="aaction",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_3 = RepositoryAction(
        id="action_3",
        name="caction",
        status=RepositoryActionStatus.BUILDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_4 = RepositoryAction(
        id="action_4",
        name="daction",
        status=RepositoryActionStatus.SUCCESS,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_5 = RepositoryAction(
        id="action_5",
        name="baction",
        status=RepositoryActionStatus.ERROR,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    action_6 = RepositoryAction(
        id="action_6",
        name="Baction2",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[action_1, action_2, action_3, action_4, action_5, action_6],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    scan_result.sort_actions("name", False)
    assert scan_result.actions == [
        action_2,
        action_5,
        action_6,
        action_3,
        action_4,
        action_1,
    ]
    scan_result.sort_actions("name", True)
    assert scan_result.actions == [
        action_1,
        action_4,
        action_3,
        action_6,
        action_5,
        action_2,
    ]
    scan_result.sort_actions("status", False)
    assert scan_result.actions == [
        action_3,
        action_5,
        action_6,
        action_2,
        action_1,
        action_4,
    ]
    scan_result.sort_actions("status", True)
    assert scan_result.actions == [
        action_4,
        action_1,
        action_6,
        action_2,
        action_5,
        action_3,
    ]


def test_remove_action_if_exists() -> None:
    action_1 = RepositoryAction(
        id="123",
        name="Baction2",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[action_1],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    scan_result.remove_action_if_exists(action_1)
    assert scan_result.actions == []


def test_remove_action_if_not_exists() -> None:
    action_1 = RepositoryAction(
        id="123",
        name="Baction2",
        status=RepositoryActionStatus.BUILD_PENDING,
        project="project",
        technical_name="tname",
        creation_date=datetime.datetime.now(),
        source_id="id",
        sha="123",
        archive=bytes(123),
        description="repository_action description",
        version="repository_action version",
        type=RepositoryActionType.PYTHON3.value,
        kind=RepositoryActionKind.SOURCE.value,
        owner_id="repository_action owner",
        dynamic_outputs=True,
        inputs=[
            RepositoryActionIO(
                id="repository_action input id",
                display_name="Input display name",
                technical_name="Input technical name",
                type=RepositoryActionIOType.ENUM,
                kind=RepositoryActionIOKind.INPUT,
                enum_values=["value1", "value2", "value3"],
                help="help",
            )
        ],
        outputs=[
            RepositoryActionIO(
                id="repository_action output id",
                display_name="Output display name",
                technical_name="Output technical name",
                type=RepositoryActionIOType.INTEGER,
                kind=RepositoryActionIOKind.OUTPUT,
                enum_values=[],
                help="help",
            )
        ],
    )
    scan_result = ScanResult(
        actions=[],
        id="id",
        scan_date=datetime.datetime.now(),
        project="project",
        sha="sha",
    )
    scan_result.remove_action_if_exists(action_1)
    assert scan_result.actions == []
