# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from pathlib import Path
from typing import List, Optional

from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.domain.git_repo.git_repo import GitRepo
from ryax.repository.domain.git_repo.git_repo_values import GitRepoInfo
from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.domain.scan.scan import Scan
from ryax.repository.domain.scan.scan_service import IRepositoryScanService
from ryax.repository.domain.scan.scan_values import ScanGitRepoRequest
from ryax.repository.domain.scan_result.scan_result import ScanResult
from ryax.repository.domain.services.encryption_service import IEncryptionService

logger = logging.getLogger(__name__)


class RepositoryService:
    def __init__(
        self,
        uow: IUnitOfWork,
        repository_scan_service: IRepositoryScanService,
        encryption_service: IEncryptionService,
    ):
        self.uow = uow
        self.repository_scan_service = repository_scan_service
        self.encryption_service = encryption_service

    def list_with_v1_result_bar(self, current_project: str) -> List[GitRepo]:
        with self.uow:
            git_repos = self.uow.git_repos.list_git_repos(current_project)
            for git_repo in git_repos:
                if git_repo.scan_result:
                    git_repo.scan_result.refresh_status()
            return git_repos

    def list(self, current_project: str) -> List[GitRepo]:
        with self.uow:
            git_repos = self.uow.git_repos.list_git_repos(current_project)
            for git_repo in git_repos:
                if git_repo.scan_result:
                    git_repo.set_last_scan_summary()
            return git_repos

    def add(self, data: GitRepoInfo, current_project: str) -> GitRepo:
        with self.uow:
            encrypted_password = None
            if data.password is not None:
                encrypted_password = (
                    self.encryption_service.encrypt(data.password)
                    if len(data.password) > 0
                    else None
                )
            new_git_repo = GitRepo.create_from_info(
                project=current_project,
                data=data,
                encrypted_password=encrypted_password,
            )
            self.uow.git_repos.add_git_repo(new_git_repo)
            self.uow.commit()
            self.uow.git_repos.refresh_git_repo(new_git_repo)
            return new_git_repo

    def get(
        self,
        source_id: str,
        current_project: str,
        action_sorting: Optional[str] = None,
        action_sorting_type: Optional[str] = None,
    ) -> GitRepo:
        with self.uow:
            git_repo = self.uow.git_repos.get_git_repo(source_id, current_project)
            if git_repo and git_repo.scan_result:
                git_repo.last_scan = git_repo.scan_result
                git_repo.scan_result.sort_actions(
                    action_sorting, sort_descending=action_sorting_type == "desc"
                )
                git_repo.scan_result.refresh_status()
            return git_repo

    def update(
        self, source_id: str, data: GitRepoInfo, current_project: str
    ) -> Optional[GitRepo]:
        with self.uow:
            source = self.uow.git_repos.get_git_repo(source_id, current_project)
            encrypted_password = None
            if data.password is not None:
                encrypted_password = (
                    self.encryption_service.encrypt(data.password)
                    if len(data.password) > 0
                    else b""
                )
            source.update_information(
                source_data=data, encrypted_password=encrypted_password
            )
            self.uow.commit()
            self.uow.git_repos.refresh_git_repo(source)
            return source

    def archive(self, source_id: str, current_project: str) -> None:
        with self.uow:
            source = self.uow.git_repos.get_git_repo(source_id, current_project)
            source.archive()
            self.uow.commit()

    async def scan(
        self,
        user_id: str,
        git_repo_id: str,
        current_project: str,
        repository_scan_request: ScanGitRepoRequest,
    ) -> GitRepo:
        """Responsible for retrieving the code from a GitRepo and parsing all the repository_action data, reporting all possible errors."""
        with self.uow:
            # Initialise the scan
            repository = self.uow.git_repos.get_git_repo(git_repo_id, current_project)
            decrypted_password: Optional[str] = (
                self.encryption_service.decrypt(repository.password)
                if repository.password is not None
                else None
            )
            scan: Scan = await self.repository_scan_service.scan_source_code(
                repository_scan_request,
                current_project,
                repository.url,
                repository.username,
                decrypted_password,
            )

            self.repository_scan_service.set_git_sha(scan)

            scanned_actions: List[RepositoryAction] = []
            # Analyse each repository action individually
            for path in Path(scan.path).rglob("ryax_metadata.yaml"):
                action_being_scanned = RepositoryAction.from_project_path_user(
                    project_id=current_project,
                    scan_path=scan.path,
                    action_path=path,
                    user_id=user_id,
                    git_repo_id=repository.id,
                )
                scanned_actions.append(action_being_scanned)
                self.repository_scan_service.parse_action_data(
                    action_being_scanned, action_metadata_path=path
                )

            # Internal versioning if no version was given
            for action in scanned_actions:
                if not action.version:
                    action_version = (
                        self.uow.actions.get_latest_version_of_action_with_same_sha(
                            action.technical_name,
                            action.sha,
                            current_project,
                        )
                    )
                    if action_version:
                        action.version = action_version
                    else:
                        existing_versions = (
                            self.uow.actions.get_action_versions_by_technical_name(
                                action.technical_name, current_project
                            )
                        )
                        action.set_new_action_version(existing_versions)

            # Initialize the ScanResult to return
            scan_result = ScanResult.from_project_and_sha(current_project, scan.sha)

            # Populate the scan result
            repository.scan_result = scan_result

            # Internal duplicate checking and repository_action creation.
            for action in scanned_actions:
                matching_actions = self.uow.actions.get_matching_actions(
                    action.technical_name, action.version, current_project
                )

                if any([item.is_in_building_process() for item in matching_actions]):
                    action.duplicated = True
                    action_to_add = matching_actions.pop(0)
                    scan_result.add_action(action_to_add)

                else:
                    for item in matching_actions:
                        self.uow.actions.delete_action(item)
                    action.status = (
                        RepositoryActionStatus.SCANNED
                        if not action.has_scan_error
                        else RepositoryActionStatus.SCAN_ERROR
                    )
                    self.uow.actions.add_action(action)
                    scan_result.add_action(action)
            # Remove scanned code
            self.repository_scan_service.clear_source_code(scan)

            self.uow.commit()
            # FIXME: Don't know why this is needed...
            actions_from_db = self.uow.actions.list_actions_by_ids(
                [action.id for action in scanned_actions]
            )
            repository.scan_result = scan_result
            repository.scan_result.actions = actions_from_db
            repository.scan_result.sort_actions(sorting="name")

            # FIXME: Use a view instead...
            repository.last_scan = scan_result

        return repository
