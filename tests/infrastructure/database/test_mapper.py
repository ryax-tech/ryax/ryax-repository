from datetime import datetime

from ryax.repository.domain.logo.logo import Logo
from ryax.repository.domain.repository_action.repository_action import RepositoryAction
from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.infrastructure.database.engine import Session


def test_action_mapper(
    generate_logo, generate_action, database_session: Session
) -> None:
    """Action mapper should return repository_action"""
    action_id = "b221ef40-9aa9-4b07-adcb-fe7a94d2b8b4"
    logo_id = "38a8c032-19c6-4491-b5f6-3f7421a0a2c5"
    project_id = "3fca406f-0ce3-44f7-a46b-d8dbb6324d7b"
    logo = generate_logo(
        id=logo_id,
        name="logo",
        extension="png",
        content=b"logo",
    )
    action = generate_action(
        id=action_id,
        name="ActionTestName",
        technical_name="valid-technical-name-test",
        status=RepositoryActionStatus.READY.value,
        dynamic_outputs=False,
        creation_date=datetime.now(),
        owner_id="user",
        logo_id=logo["id"],
        categories=["cat_1", "cat_2"],
        project=project_id,
    )
    assert database_session.query(RepositoryAction).get(action_id) == RepositoryAction(
        id=action["id"],
        name=action["name"],
        technical_name=action["technical_name"],
        status=RepositoryActionStatus(action["status"]),
        dynamic_outputs=action["dynamic_outputs"],
        creation_date=action["creation_date"],
        owner_id=action["owner_id"],
        logo=Logo(
            id=logo_id,
            name=logo["name"],
            extension=logo["extension"],
            content=logo["content"],
        ),
        categories=["cat_1", "cat_2"],
        project=action["project"],
        version=action["version"],
        description=action["description"],
        type=action["type"],
        kind=action["kind"],
        archive=action["archive"],
        source_id=action["source_id"],
        sha=action["sha"],
    )
