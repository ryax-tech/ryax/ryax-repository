# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os

from marshmallow import Schema, fields
from marshmallow_enum import EnumField

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
)


class ScannedRepositoryActionSchema(Schema):
    id = fields.String(
        metadata={
            "description": "ID of the repository_action",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )

    technical_name = fields.String(
        metadata={
            "description": "Technical name of the repository_action",
            "example": "mqttgw",
        },
    )

    name = fields.String(
        metadata={
            "description": "Name of the repository_action",
            "example": "MQTT Gateway",
        },
    )

    version = fields.String(
        metadata={"description": "Version of the repository_action", "example": "1.0"},
    )

    description = fields.String(
        metadata={
            "description": "Description of the repository_action",
            "example": "This repository_action creates a MQTT Gateway",
        },
    )

    kind = EnumField(RepositoryActionKind, by_value=True)

    type = EnumField(RepositoryActionType, by_value=True)

    dynamic_outputs = fields.Boolean(
        metadata={
            "description": "Whether repository_action has dynamic outputs",
            "example": "True",
        },
    )

    status = EnumField(RepositoryActionStatus, by_value=True)

    scan_errors = fields.List(
        fields.String(),
        metadata={
            "description": "In cas of problem while analysing the repository_action",
            "example": ["Error while parsing repository_action files"],
        },
    )

    metadata_path = fields.Function(
        lambda obj, context: os.path.relpath(obj.metadata_path, context["source_path"]),
        metadata={
            "description": "Path of ryax metadata file found relative to the root git_repo",
        },
    )

    duplicated = fields.Boolean(
        metadata={
            "description": "Whether the repository_action already exists on the system",
            "example": True,
        },
    )

    sha = fields.String(
        metadata={
            "description": "Commit sha of the repository_action",
            "example": "99d50838417b8e310d229ccb252b5eaa5d142861",
        },
    )
