# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Callable, List, Union

from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from dependency_injector.wiring import Provide

from ryax.repository.application.authentication_service import AuthenticationService
from ryax.repository.container import ApplicationContainer
from ryax.repository.infrastructure.api.schemas.error_schema import ErrorSchema


def check_token(
    public_paths: List[str],
    service: AuthenticationService = Provide[
        ApplicationContainer.authentication_service
    ],
) -> Callable:
    @web.middleware
    async def middleware_handler(
        request: Request, handler: Callable
    ) -> Union[Callable, Response]:
        is_public = any(request.path.startswith(item) for item in public_paths)
        if not is_public:
            token = request.headers.get("Authorization", None)
            token_payload = service.check_access(token)
            if not token_payload:
                result = ErrorSchema().dump({"error": "Access denied"})
                return json_response(result, status=401)
            else:
                request["user_id"] = token_payload.user_id
                return await handler(request)
        else:
            return await handler(request)

    return middleware_handler
