# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from marshmallow import Schema, fields

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.domain.scan_result.scan_result import ScanResult
from ryax.repository.infrastructure.api.schemas.action_schema import ActionSchema


class ScanResultBarDetailsSchema(Schema):
    modules_success = fields.Integer(
        metadata={
            "description": "Number of modules built successfully",
            "example": "5",
        },
    )

    modules_in_progress = fields.Integer(
        metadata={"description": "Number of modules building", "example": "5"},
    )

    modules_ready = fields.Integer(
        metadata={"description": "Number of modules not built", "example": "5"},
    )

    modules_error = fields.Integer(
        metadata={"description": "Number of modules in error", "example": "5"},
    )


class ScanResultBarSchema(Schema):
    bar = fields.Nested(
        ScanResultBarDetailsSchema,
        metadata={
            "description": "Numbers of last scan modules arranged by status",
        },
        attribute="status",
    )
    scan_date = fields.DateTime(
        metadata={
            "description": "Source last scan date",
            "example": "2020-10-28T16:05:19.031339",
        }
    )


class ScanResultRepositoryActionSchemaV1(Schema):
    sha = fields.String(
        metadata={
            "description": "Source last scan sha",
            "example": "6db54a2b96da72a0ffe8e0247e39815117dc2e92",
        }
    )
    scan_date = fields.DateTime(
        metadata={
            "description": "Source last scan date",
            "example": "2020-10-28T16:05:19.031339",
        }
    )

    built_modules = fields.Method("get_built_modules")

    none_built_modules = fields.Method("get_non_built_modules")

    scan_date = fields.DateTime(
        metadata={
            "description": "Source last scan date",
            "example": "2020-10-28T16:05:19.031339",
        }
    )

    @staticmethod
    def get_built_modules(scan_result: ScanResult) -> List[ActionSchema]:
        return [
            ActionSchema().dump(action)
            for action in scan_result.actions
            if action.status == RepositoryActionStatus.BUILT
        ]

    @staticmethod
    def get_non_built_modules(scan_result: ScanResult) -> List[ActionSchema]:
        return [
            ActionSchema().dump(action)
            for action in scan_result.actions
            if action.status != RepositoryActionStatus.BUILT
        ]
