from unittest.mock import MagicMock

import pytest
from aio_pika import IncomingMessage

from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionDeletedEvent,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    BuildErrorCode,
)
from ryax.repository.infrastructure.messaging.handlers import repository_action_handler
from ryax.repository.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionBuildError,
)
from ryax.repository.infrastructure.messaging.messages.studio_messages_pb2 import (
    ActionDeleted,
)


@pytest.fixture(scope="function")
def action_service_mock(app_container: ApplicationContainer):
    action_service_mock = MagicMock(RepositoryActionService)
    app_container.action_service.override(action_service_mock)
    return action_service_mock


async def test_on_action_deleted_event(
    action_service_mock: RepositoryActionService, app_container: ApplicationContainer
) -> None:
    app_container.wire(modules=[repository_action_handler])

    action_deleted = ActionDeleted()
    action_deleted.action_id = "action_id"

    message = MagicMock(IncomingMessage)
    message.body = action_deleted.SerializeToString()
    await repository_action_handler.on_action_deleted(message)
    action_service_mock.action_unbuild.assert_called_with(
        ActionDeletedEvent(action_id=action_deleted.action_id)
    )


async def test_on_action_build_error(
    action_service_mock: RepositoryActionService, app_container: ApplicationContainer
) -> None:
    app_container.wire(modules=[repository_action_handler])

    action_build_error = ActionBuildError()
    action_build_error.action_id = "action_id"
    action_build_error.logs = "build error logs"
    action_build_error.code = 1

    message = MagicMock(IncomingMessage)
    message.body = action_build_error.SerializeToString()
    await repository_action_handler.on_action_build_error(message)
    action_service_mock.action_build_error.assert_called_with(
        ActionBuildErrorEvent(
            action_id=action_build_error.action_id,
            logs=action_build_error.logs,
            code=BuildErrorCode.ImageNameNotDefined,
        )
    )
