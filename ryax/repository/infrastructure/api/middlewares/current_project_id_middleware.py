# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Callable, List, Union

from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from dependency_injector.wiring import Provide

from ryax.repository.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.common.common_exceptions import NoCurrentProjectException
from ryax.repository.infrastructure.api.schemas.error_schema import ErrorSchema


def handle(
    public_paths: List[str],
    service: ProjectAuthorizationService = Provide[
        ApplicationContainer.project_authorization_service
    ],
) -> Callable:
    """Get current project id from authorization with a http request then Inject it in the request"""

    @web.middleware
    async def middleware_handler(
        request: Request, handler: Callable
    ) -> Union[Callable, Response]:
        is_public = any(request.path.startswith(item) for item in public_paths)
        if not is_public:
            authorization_token = request.headers.get("Authorization", "")
            user_project = await service.get_current_project(
                request["user_id"], authorization_token
            )
            if not user_project:
                error = NoCurrentProjectException.message
                result = ErrorSchema().dump({"error": error})
                return json_response(result, status=401)
            else:
                request["current_project_id"] = user_project
                return await handler(request)
        else:
            return await handler(request)

    return middleware_handler
