# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import Logger, getLogger
from typing import List, Union

from aio_pika import ExchangeType, Message

from ryax.repository.domain.events.base_event import BaseEvent
from ryax.repository.domain.events.event_publisher import IEventPublisher
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
    ActionCancelBuildEvent,
)
from ryax.repository.infrastructure.messaging.messages.repository_messages_pb2 import (
    ActionBuild,
    ActionCancelBuild,
)
from ryax.repository.infrastructure.messaging.utils.engine import MessagingEngine

MessageContent = Union[ActionBuild, ActionCancelBuild]

logger: Logger = getLogger(__name__)


class MessagingPublisher(IEventPublisher):
    def __init__(self, engine: MessagingEngine):
        self.engine: MessagingEngine = engine

    def _handle_action_cancel_build(
        self, event: ActionCancelBuildEvent
    ) -> ActionCancelBuild:
        action_cancel_build = ActionCancelBuild()
        action_cancel_build.action_id = event.action_id
        return action_cancel_build

    def _handle_action_build(self, event: ActionBuildEvent) -> ActionBuild:
        action_build = ActionBuild()
        action_build.action_id = event.action_id
        action_build.action_type = event.action_type.value
        action_build.action_kind = event.action_kind.value
        action_build.action_version = event.action_version
        action_build.action_archive = event.action_archive
        action_build.action_technical_name = event.action_technical_name
        action_build.owner_id = event.action_owner_id
        action_build.project = event.action_project
        for item in event.action_wrapper_type_list:
            action_build.action_wrapper_type_list.append(item.value)
        return action_build

    def handle_event(
        self,
        event: Union[
            BaseEvent,
            ActionDeletedEvent,
            ActionBuildEvent,
            ActionBuildStartEvent,
            ActionBuildSuccessEvent,
            ActionBuildErrorEvent,
            ActionCancelBuildEvent,
        ],
    ) -> MessageContent:
        if isinstance(event, ActionBuildEvent):
            return self._handle_action_build(event)
        elif isinstance(event, ActionCancelBuildEvent):
            return self._handle_action_cancel_build(event)
        else:
            raise Exception("Event is serializable into message")

    async def publish(
        self,
        events: List[
            Union[
                BaseEvent,
                ActionDeletedEvent,
                ActionBuildEvent,
                ActionBuildStartEvent,
                ActionBuildSuccessEvent,
                ActionBuildErrorEvent,
                ActionCancelBuildEvent,
            ]
        ],
    ) -> None:
        channel = await self.engine.get_channel()
        exchange = await channel.declare_exchange(
            "domain_events", ExchangeType.TOPIC, durable=True
        )
        for item in events:
            logger.info(f"Publishing event: {item}")
            message_content = self.handle_event(item)
            message = Message(
                type=item.event_type, body=message_content.SerializeToString()
            )
            await exchange.publish(message, routing_key=f"Repository.{item.event_type}")
        await channel.close()
