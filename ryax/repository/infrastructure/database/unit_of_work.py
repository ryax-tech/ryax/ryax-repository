# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import traceback
from types import TracebackType
from typing import Callable, Type

from ryax.repository.domain.common.unit_of_work import IUnitOfWork
from ryax.repository.infrastructure.database.engine import DatabaseEngine
from ryax.repository.infrastructure.database.repositories.action_repository import (
    DatabaseActionRepository,
)
from ryax.repository.infrastructure.database.repositories.git_repo_repository import (
    DatabaseGitRepoRepository,
)

logger = logging.getLogger(__name__)


class DatabaseUnitOfWork(IUnitOfWork):
    def __init__(
        self,
        engine: DatabaseEngine,
        git_repo_factory: Callable[..., DatabaseGitRepoRepository],
        action_repository_factory: Callable[..., DatabaseActionRepository],
    ):
        self.engine = engine
        self.git_repo_factory = git_repo_factory
        self.module_repository_factory = action_repository_factory

    def __enter__(self) -> None:
        self.session = self.engine.get_session()
        self.git_repos: DatabaseGitRepoRepository = self.git_repo_factory(
            session=self.session
        )
        self.actions: DatabaseActionRepository = self.module_repository_factory(
            session=self.session
        )

    def __exit__(
        self,
        exc_type: Type[Exception] | None = None,
        exc_val: Exception | None = None,
        exc_tb: TracebackType | None = None,
    ) -> None:
        if exc_type is not None:
            logger.error(
                "Error during the Unit of Work context: %s",
                "".join(traceback.format_exception(exc_type, value=exc_val, tb=exc_tb)),
            )
        self.session.close()

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
