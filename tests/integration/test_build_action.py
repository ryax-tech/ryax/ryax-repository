import datetime
import pytest
from unittest import mock
from aiohttp.test_utils import TestClient
from ryax.repository.domain.repository_action.repository_action import (
    RepositoryActionIO,
)
from ryax.repository.infrastructure.database.engine import Session
from ryax.repository.domain.events.event_publisher import IEventPublisher
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildEvent,
)
from ryax.repository.domain.scan_result.scan_result import ScanResult
from ryax.repository.container import ApplicationContainer


from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionKind,
    RepositoryActionStatus,
    RepositoryActionType,
    RepositoryActionWrapperType,
)

from ryax.repository.domain.git_repo.git_repo import GitRepo

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionIOKind,
    RepositoryActionIOType,
)

from ryax.repository.domain.repository_action.repository_action import RepositoryAction


@pytest.fixture(scope="function")
def action_factory():
    """
    Since BuildTask has a foreign key against action_repository
    we need to populate the db with in order to create BuildTask
    """

    def gen_action(
        action_id: str, source_id: str = "source_id", project: str = "project_id"
    ):
        action = RepositoryAction(
            project=project,
            technical_name="tname",
            metadata_path="path",
            creation_date=datetime.datetime.now(),
            source_id=source_id,
            sha="123",
            id=f"Action {action_id}",
            name="repository_action name",
            description="repository_action description",
            version="repository_action version",
            type=RepositoryActionType.PYTHON3,
            kind=RepositoryActionKind.SOURCE,
            status=RepositoryActionStatus.SCANNED,
            owner_id="repository_action owner",
            archive=bytes("repository_action archive", "utf-8"),
            dynamic_outputs=True,
            inputs=[
                RepositoryActionIO(
                    id="repository_action input id",
                    display_name="Input display name",
                    technical_name="Input technical name",
                    type=RepositoryActionIOType.ENUM,
                    kind=RepositoryActionIOKind.INPUT,
                    enum_values=["value1", "value2", "value3"],
                    help="help",
                )
            ],
            outputs=[
                RepositoryActionIO(
                    id="repository_action output id",
                    display_name="Output display name",
                    technical_name="Output technical name",
                    type=RepositoryActionIOType.INTEGER,
                    kind=RepositoryActionIOKind.OUTPUT,
                    enum_values=[],
                    help="help",
                )
            ],
        )

        return action

    yield gen_action

    # Looks like that the provided scoped session automatically rollbacks
    # everything after each test function.


async def test_build_source(
    action_factory,
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    app_container: ApplicationContainer,
    api_client: TestClient,
) -> None:
    """ """

    messaging_publisher: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()

    messaging_publisher.publish = mock.AsyncMock()

    project = "project_id"
    source_id = "sourcetestid"
    action = action_factory(1, source_id=source_id)

    source = GitRepo(
        id=source_id,
        project=project,
        username="user",
        password=b"password1",
        name="name",
        url="url",
        scan_result=ScanResult(
            id="1",
            scan_date=datetime.datetime.now(),
            project=project,
            sha="",
            actions=[action],
        ),
    )

    database_session.add(source)
    database_session.commit()

    # repository_service_mock.get.return_value = source
    # action_service_mock.build_action.return_value = action

    url = f"/v2/sources/{source_id}/build"
    print(f"url: {url}")

    response = await api_client.post(url)

    data = await response.json()

    assert response.status == 200

    messaging_publisher.publish.assert_has_calls(
        [
            mock.call(
                [
                    ActionBuildEvent(
                        action_id="Action 1",
                        action_version="repository_action version",
                        action_archive=b"repository_action archive",  # Use bytes instead of empty string
                        action_project="project_id",  # Match the actual project value
                        action_owner_id="repository_action owner",
                        action_technical_name="tname",
                        action_type=RepositoryActionType.PYTHON3,  # Use the enum
                        action_kind=RepositoryActionKind.SOURCE,  # Use the enum
                        event_type="ActionBuild",
                        action_wrapper_type_list=[
                            RepositoryActionWrapperType.PYTHON3_GRPC_V1_TRIGGER
                        ],  # Use the enum list
                    )
                ]
            )
        ]
    )

    action_result = {
        "id": "Action 1",
        "name": "repository_action name",
        "version": "repository_action version",
        "technical_name": "tname",
        "lockfile": "",
        "description": "repository_action description",
        "kind": "Source",
        "type": "python3",
        "dynamic_outputs": True,
        "status": "Build Pending",
        "build_error": None,
        "metadata_path": "path",
        "scan_errors": [],
        "sha": "123",
    }

    del data[0]["creation_date"]

    assert data == [action_result]
