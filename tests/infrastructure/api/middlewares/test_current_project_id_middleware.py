from typing import Callable
from unittest import mock

from aiohttp.web_request import Request

from ryax.repository.infrastructure.api.middlewares import current_project_id_middleware


async def test_current_project_id_middleware_handler() -> None:
    user_project = "4241a730-1ada-4741-a1ef-ba46c6b6a3ac"
    service_mock = mock.AsyncMock()
    service_mock.get_current_project.return_value = user_project
    middleware = current_project_id_middleware
    middleware_handler = middleware.handle(public_paths=[], service=service_mock)
    req = mock.MagicMock(Request)
    handler = mock.AsyncMock(Callable)
    await middleware_handler(req, handler)
    req.assert_has_calls(
        [
            mock.call.headers.get("Authorization", ""),
            mock.call.__getitem__("user_id"),
            mock.call.__setitem__("current_project_id", user_project),
        ]
    )
    # assert req.get("current_project_id") == user_project
    handler.assert_called_once_with(req)
