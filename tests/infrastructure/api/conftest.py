from unittest import mock

import pytest
from aiohttp import web

from ryax.repository.application.authentication_service import AuthenticationService
from ryax.repository.application.git_repo_service import RepositoryService
from ryax.repository.infrastructure.encryption.encryption_service import (
    EncryptionService,
)

from ryax.repository.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.auth.auth_token import AuthToken
from ryax.repository.infrastructure.api.setup import setup as setup_api


@pytest.fixture()
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
async def api_client(app_container: ApplicationContainer, aiohttp_client):
    """Instantiate client to test api part"""
    app = web.Application()
    setup_api(app, app_container)
    return await aiohttp_client(app)


@pytest.fixture()
def project_authorization_service_mock(app_container: ApplicationContainer):
    project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
    project_authorization_service_mock.get_current_project = mock.AsyncMock(
        return_value="project_id"
    )
    app_container.project_authorization_service.override(
        project_authorization_service_mock
    )
    return project_authorization_service_mock


@pytest.fixture()
def repository_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    action_service_mock = mock.MagicMock(RepositoryService)
    app_container.repository_service.override(action_service_mock)
    return action_service_mock


@pytest.fixture()
def encryption_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    encryption_service_mock = mock.MagicMock(EncryptionService)
    app_container.encryption_service.override(encryption_service_mock)
    return encryption_service_mock


@pytest.fixture()
def authentication_service_mock(app_container: ApplicationContainer):
    authentication_service_mock = mock.MagicMock(AuthenticationService)
    authentication_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user")
    )
    app_container.authentication_service.override(authentication_service_mock)
    return authentication_service_mock


@pytest.fixture()
def action_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    action_service_mock = mock.MagicMock(RepositoryActionService)
    app_container.action_service.override(action_service_mock)
    return action_service_mock
