"""add lockfile blob

Revision ID: be84ace25339
Revises: 9b5e9df24cdf
Create Date: 2025-01-30 10:42:38.245939

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'be84ace25339'
down_revision = '9b5e9df24cdf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('repository_action', sa.Column('lockfile', sa.LargeBinary(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('repository_action', 'lockfile')
    # ### end Alembic commands ###
