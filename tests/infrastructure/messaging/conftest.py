import os

import pytest
from aio_pika import Channel, RobustConnection, connect

from ryax.repository.container import ApplicationContainer


@pytest.fixture(scope="function")
async def broker_connection():
    broker_url = os.getenv("RYAX_BROKER")
    broker_connection: RobustConnection = await connect(broker_url)
    yield broker_connection
    await broker_connection.close()


@pytest.fixture(scope="function")
async def broker_channel(broker_connection: RobustConnection):
    broker_channel: Channel = await broker_connection.channel()
    yield broker_channel
    await broker_channel.close()


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()
