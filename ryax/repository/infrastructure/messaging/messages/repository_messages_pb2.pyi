"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import builtins
import collections.abc
import google.protobuf.descriptor
import google.protobuf.internal.containers
import google.protobuf.message
import sys

if sys.version_info >= (3, 8):
    import typing as typing_extensions
else:
    import typing_extensions

DESCRIPTOR: google.protobuf.descriptor.FileDescriptor

@typing_extensions.final
class ActionBuild(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    ACTION_ID_FIELD_NUMBER: builtins.int
    ACTION_TYPE_FIELD_NUMBER: builtins.int
    ACTION_KIND_FIELD_NUMBER: builtins.int
    ACTION_VERSION_FIELD_NUMBER: builtins.int
    ACTION_ARCHIVE_FIELD_NUMBER: builtins.int
    ACTION_TECHNICAL_NAME_FIELD_NUMBER: builtins.int
    ACTION_WRAPPER_TYPE_LIST_FIELD_NUMBER: builtins.int
    ACTION_REVISION_FIELD_NUMBER: builtins.int
    OWNER_ID_FIELD_NUMBER: builtins.int
    PROJECT_FIELD_NUMBER: builtins.int
    action_id: builtins.str
    action_type: builtins.str
    action_kind: builtins.str
    action_version: builtins.str
    action_archive: builtins.bytes
    action_technical_name: builtins.str
    @property
    def action_wrapper_type_list(self) -> google.protobuf.internal.containers.RepeatedScalarFieldContainer[builtins.str]: ...
    action_revision: builtins.str
    owner_id: builtins.str
    project: builtins.str
    def __init__(
        self,
        *,
        action_id: builtins.str = ...,
        action_type: builtins.str = ...,
        action_kind: builtins.str = ...,
        action_version: builtins.str = ...,
        action_archive: builtins.bytes = ...,
        action_technical_name: builtins.str = ...,
        action_wrapper_type_list: collections.abc.Iterable[builtins.str] | None = ...,
        action_revision: builtins.str = ...,
        owner_id: builtins.str = ...,
        project: builtins.str = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing_extensions.Literal["action_archive", b"action_archive", "action_id", b"action_id", "action_kind", b"action_kind", "action_revision", b"action_revision", "action_technical_name", b"action_technical_name", "action_type", b"action_type", "action_version", b"action_version", "action_wrapper_type_list", b"action_wrapper_type_list", "owner_id", b"owner_id", "project", b"project"]) -> None: ...

global___ActionBuild = ActionBuild

@typing_extensions.final
class ActionCancelBuild(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    ACTION_ID_FIELD_NUMBER: builtins.int
    action_id: builtins.str
    def __init__(
        self,
        *,
        action_id: builtins.str = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing_extensions.Literal["action_id", b"action_id"]) -> None: ...

global___ActionCancelBuild = ActionCancelBuild
