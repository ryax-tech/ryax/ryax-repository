from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.repository.domain.repository_action.repository_action_values import (
    RepositoryActionStatus,
)
from ryax.repository.infrastructure.database.engine import Session


class TestRepositoryIntegrationLifecycle:
    async def setup_class_variables(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ):
        self.database_session = database_session
        self.project_authorization_service_mock = project_authorization_service_mock
        self.authentication_service_mock = authentication_service_mock
        self.api_client = api_client

    async def _ensure_no_git_repos_exist(self) -> None:
        response = await self.api_client.get("/sources")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 0

    async def _create_default_actions_git_repo(self) -> dict:
        response = await self.api_client.post(
            "/sources",
            data={
                "name": "Default Actions",
                "url": "https://gitlab.com/ryax-tech/workflows/default-actions",
            },
        )
        assert response.status == 201
        result = await response.json()
        assert result["name"] == "Default Actions"
        assert result["username"] is None
        assert result["url"] == "https://gitlab.com/ryax-tech/workflows/default-actions"
        assert result["password_is_set"] is False
        assert result["scan_result"] is None
        return result

    async def _get_git_repo_by_id(self, git_repo_id: str) -> dict:
        response = await self.api_client.get(f"/sources/{git_repo_id}")
        assert response.status == 200
        result = await response.json()
        return result

    async def _scan_git_repo_by_id(self, git_repo_id: str) -> dict:
        """Scans default modules and makes sure they all match the following criteria:
        - Status is SCANNED
        - There are no scan errors"""
        response = await self.api_client.post(f"v2/sources/{git_repo_id}/scan")
        assert response.status == 200
        result = await response.json()
        return result

    async def _delete_git_repo_by_id(self, git_repo_id: str):
        response = await self.api_client.delete(f"/sources/{git_repo_id}")
        assert response.status == 200
        response = await self.api_client.get("/sources")
        result = await response.json()
        assert not any(git_repo["id"] == git_repo_id for git_repo in result)

    async def _get_git_repos(self) -> dict:
        response = await self.api_client.get("/sources")
        assert response.status == 200
        result = await response.json()
        return result

    async def _delete_all_actions(self):
        response = await self.api_client.get("/modules")
        assert response.status == 200
        all_actions = await response.json()
        for action in all_actions:
            response = await self.api_client.delete(f"/modules/{action['id']}")
            assert response.status == 200
        response = await self.api_client.get("/modules")
        assert response.status == 200
        all_actions = await response.json()
        assert not all_actions

    async def test_scan_update_rescan_delete(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ):
        await self.setup_class_variables(
            database_session,
            project_authorization_service_mock,
            authentication_service_mock,
            api_client,
        )

        await self._ensure_no_git_repos_exist()

        default_actions_git_repo = await self._create_default_actions_git_repo()

        default_actions_git_repo_get_check = await self._get_git_repo_by_id(
            default_actions_git_repo["id"]
        )
        assert not default_actions_git_repo_get_check["last_scan"]

        git_repo_scan_result = await self._scan_git_repo_by_id(
            default_actions_git_repo["id"]
        )
        assert (
            all(
                action["status"] == RepositoryActionStatus.SCANNED.value
                and not action["scan_errors"]
                for action in git_repo_scan_result["last_scan"]["built_actions"]
                + git_repo_scan_result["last_scan"]["not_built_actions"]
            )
            and len(git_repo_scan_result["last_scan"]["not_built_actions"]) > 0
        )

        # Make sure the status is there
        get_git_repos_result = await self._get_git_repos()
        assert len(get_git_repos_result) == 1
        assert get_git_repos_result[0]["scan_result"] is not None
        assert get_git_repos_result[0]["scan_result"]["bar"]["modules_ready"] > 0
        assert get_git_repos_result[0]["scan_result"]["bar"]["modules_error"] == 0

        # Check all actions are there after the scan.
        response = await self.api_client.get("/modules")
        assert response.status == 200
        result = await response.json()

        assert set(action["id"] for action in result) == set(
            action["id"]
            for action in git_repo_scan_result["last_scan"]["not_built_actions"]
            + git_repo_scan_result["last_scan"]["built_actions"]
        )

        # Check the last scan is coherent (contains the actions from git_repo_scan_result)
        default_actions_git_repo_get_check_after_scan = await self._get_git_repo_by_id(
            default_actions_git_repo["id"]
        )
        assert default_actions_git_repo_get_check_after_scan["last_scan"] is not None

        # Now do errored actions
        response = await self.api_client.put(
            f"/sources/{default_actions_git_repo['id']}",
            data={
                "url": "https://gitlab.com/ryax-tech/ryax/errored-actions",
                "name": "Errored Actions",
            },
        )
        assert response.status == 200
        errored_actions_git_repo_id = default_actions_git_repo["id"]

        errored_actions_scan_result = await self._scan_git_repo_by_id(
            errored_actions_git_repo_id
        )
        assert len(errored_actions_scan_result["last_scan"]["not_built_actions"]) > 0

        # All errored actions but 2 (shown below) have scan errors.
        # The other 2 will have build errors.
        for errored_action in errored_actions_scan_result["last_scan"][
            "not_built_actions"
        ]:
            if errored_action["name"] in [
                "TEST: The dependency of this module does not exist",
                "Create a video with wrong library",
            ]:
                assert errored_action["status"] == RepositoryActionStatus.SCANNED.value
            else:
                assert (
                    errored_action["status"] == RepositoryActionStatus.SCAN_ERROR.value
                )

        await self._delete_git_repo_by_id(errored_actions_git_repo_id)
        await self._delete_all_actions()
