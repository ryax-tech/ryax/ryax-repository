# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
class ActionNotFoundException(Exception):
    pass


class ActionBuildNotAllowedException(Exception):
    pass


class ActionUnauthorizedDelete(Exception):
    pass


class ActionInvalidSchemaException(Exception):
    message: str = "Invalid ryax_metadata.yaml file."


class ActionRunnerFileNotFoundException(Exception):
    pass


class RepositoryActionResourceRequestInvalidException(Exception):
    pass


class ActionHandleFunctionInvalidArgumentCountException(Exception):
    pass


class ActionHandleFunctionNotFoundException(Exception):
    pass


class ActionMetadataIsV1Exception(Exception):
    pass


class ActionDynamicOutputsNotPermittedException(Exception):
    pass


class ActionIOInvalidTypeException(Exception):
    pass


class UnableToOpenLogoFileException(Exception):
    pass


class ActionSurpassesDataLimitException(Exception):
    pass


class UnableToLoadActionFilesException(Exception):
    pass


class RepositoryActionInvalidKindException(Exception):
    pass


class RepositoryActionInvalidTypeException(Exception):
    pass


class RepositoryActionTechnicalNameInvalidException(Exception):
    pass


class LogoFileDoesNotExistException(Exception):
    pass


class LogoFileInvalidFileTypeException(Exception):
    pass


class LogoFileTooLargeException(Exception):
    pass


class RepositoryActionDuplicatedInputTechnicalNameException(Exception):
    pass


class RepositoryActionDuplicatedOutputTechnicalNameException(Exception):
    pass
