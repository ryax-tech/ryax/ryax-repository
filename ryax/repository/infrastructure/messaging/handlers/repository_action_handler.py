# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide

from ryax.repository.application.repository_action_service import (
    RepositoryActionService,
)
from ryax.repository.container import ApplicationContainer
from ryax.repository.domain.repository_action.repository_action_events import (
    ActionBuildErrorEvent,
    ActionBuildStartEvent,
    ActionBuildSuccessEvent,
    ActionDeletedEvent,
    ActionBuildCanceledEvent,
)
from ryax.repository.domain.repository_action.repository_action_values import (
    BuildErrorCode,
)
from ryax.repository.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionBuildError,
    ActionBuildStart,
    ActionBuildSuccess,
    ActionBuildCanceled,
)
from ryax.repository.infrastructure.messaging.messages.studio_messages_pb2 import (
    ActionDeleted,
)


async def on_action_build_start(
    message: AbstractIncomingMessage,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> None:
    message_content = ActionBuildStart()
    message_content.ParseFromString(message.body)
    event = ActionBuildStartEvent(action_id=message_content.action_id)
    service.action_build_start(event)


async def on_action_build_success(
    message: AbstractIncomingMessage,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> None:
    message_content = ActionBuildSuccess()
    message_content.ParseFromString(message.body)
    event = ActionBuildSuccessEvent(action_id=message_content.action_id)
    await service.action_build_success(event)


async def on_action_build_error(
    message: AbstractIncomingMessage,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> None:
    message_content = ActionBuildError()
    message_content.ParseFromString(message.body)
    event = ActionBuildErrorEvent(
        action_id=message_content.action_id,
        logs=message_content.logs,
        code=BuildErrorCode(message_content.code + 100),
    )

    await service.action_build_error(event)


async def on_action_build_canceled(
    message: AbstractIncomingMessage,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> None:
    message_content = ActionBuildCanceled()
    message_content.ParseFromString(message.body)
    event = ActionBuildCanceledEvent(
        action_id=message_content.action_id,
    )
    await service.action_build_canceled(event)


async def on_action_deleted(
    message: AbstractIncomingMessage,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> None:
    message_content = ActionDeleted()
    message_content.ParseFromString(message.body)
    event = ActionDeletedEvent(action_id=message_content.action_id)
    service.action_unbuild(event)


async def on_no_action_build(
    message: AbstractIncomingMessage,
    service: RepositoryActionService = Provide[ApplicationContainer.action_service],
) -> None:
    await service.no_build()
